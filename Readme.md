# Crawler project

This project was created in purpose to study .net usage in web. 
[This](https://www.udemy.com/course/complete-guide-to-building-an-app-with-net-core-and-react/) course helped me very much, especially in front-end woes.

## What it does and why

App webscrapes motorcycle advertisements and provides ability to rate and place comments to them. When I tried to sell my own motorcycle, I noticed many overpriced adverts. So this app attempts to help people who want to buy motorcycle to find best rated one with good price. On the other side, experts can leave their comments on each motorcycle, depicting flaws and discussing price. Comment are post-moderated, users can be banned for flexible periods of time.


## Project structure

Solution consists of several projects, here is a little information about each of them (ordered as presented in repo):

- API
  
   It is .net core webapi project for backend purposes. Used to provide information for front-end. 

- Application

   It is .net core class library. I tried to structure app using [Clean Architecture design pattern](https://github.com/jasontaylordev/CleanArchitecture). Here is latest [video](https://youtu.be/dK4Yb6-LxAk) on it. I used [MediatR](https://github.com/jbogard/MediatR) and [CQRS](https://martinfowler.com/bliki/CQRS.html) pattern in this app; so implementation of MediatR calls is placed in this lib and called from API.

- Crawler

  It is .net core console app, used to scrape data from advertising web-site about one motorcycle model (Yamaha FZ-6). Data is stored in database using EF Core provider.

- CrawlerWorkerService  

  It is .net core class lib, contains BackgroundService class to launch web-scraping once a day.

- DataBase

  It is .net core class lib, which contains datacontext, migrations and other db-related things.

- Domain

  It is another .net core class lib from Clean Architecture pattern. It's purpose is to have most common used classes. It must not have any dependencies on other project libs.

- Infrastructure
  
  This is last piece from Clean Architecture pattern, it contains interfaces for Application to inherit from. Also many useful classes are here, with intention for them to be somewhat "pluggable".

- Testing

  This class library contains several unit and integration tests using [XUnit](https://xunit.net/). Also I had to use dind gitlab service to provide integration docker testing. More info about it is written in comments inside ScraperApp/Dockerfile and corresponding unit-test.

- frontend 
  
  This folder contains front-end solution. I used React with mobx and typescript language. I do not intend to pursue any career in front-end, so code here is not good and overall UI design is crappy.

- tortest2

  This badly named folder contains wrapper for web-spider, so scheduler launches this class library. Also .sln file is here.

## Some notes

I used gitlab to store repo and for CI/CD purposes. App is hosted on small VPS server. CI routine consists of 4 stages:
- build 
- test
- release
- deploy
  
Release and deploy stages triggered only on push in master branch. *Release* stage produces published version of application and *deploy* pushes it on VPS.

I used Docker containers to host app, which is divided into 3 parts:
- database (mysql)
- crawler (for web-spider)
- API (backend and frontend)


After deploy stage CD routine builds new containers and starts them using docker-compose. In docker-compose there are 2 additional containers for nginx (reverse-proxy) and certbot (automatic ssl certification).

Recently I read about microservices configuration on official msdn page and found out that using docker-compose in production is not a great idea, especially for databases. App(s) might be placed in containers, but db and other things should not. Therefore, please do not take this app structure seriously.

To launch app in debug mode one should have default mysql docker container up, then launch webapi in debug configuration. It applies migration and serves default images from web. After that, write "npm run start" in frontend folder. Vk auth should be working because I created separate app just for debug.


I owned FZ-6 motorcycle for ~5 years and it was great :)


### Disclaimer


Since it is **illegal** to [webscrape](https://yandex.ru/legal/autoru_terms_of_service/) the site that I scrape; this project is placed into private repository. I do not intend to make any money from it. Webspider works very slow, with artifical delays up to 2 minutes, so my project is a small burden to original service. On each ad page there is link towards original advert page on original web-site. I did not try to scrape seller's phone number or provide webscraping service to anyone, this is not the purpose of this project.


