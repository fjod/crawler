﻿namespace DataBase
{
    public class HostingEnv : IHostingDbEnv
    {
        public bool IsDebug = false;
        
        public bool IsDebugEnvironment()
        {
            return IsDebug;
        }
    }
}