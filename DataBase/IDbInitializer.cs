﻿using System.Threading.Tasks;

namespace DataBase
{
    public interface IDbInitializer
    {
        /// <summary>
        /// Applies any pending migrations for the context to the database.
        /// Will create the database if it does not already exist.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Adds some default values to the Db (superAdmin user)
        /// </summary>
        Task SeedData();

        /// <summary>
        /// sample data for tests
        /// </summary>
        Task AddSampleData();

        /// <summary>
        /// remove sample data in prod
        /// </summary>
        Task RemoveSampleData();
    }
}