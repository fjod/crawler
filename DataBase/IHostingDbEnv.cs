﻿namespace DataBase
{
    public interface IHostingDbEnv
    {
        bool IsDebugEnvironment();
    }
}