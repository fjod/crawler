﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable All

namespace DataBase.Migrations
{
    public partial class AdditionalUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UrlHash",
                table: "AdPics");

            migrationBuilder.AddColumn<int>(
                name: "AdID",
                table: "AdPics",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "AdPics",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AdPicUrls",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Url = table.Column<string>(nullable: true),
                    AdPicID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdPicUrls", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AdPicUrls_AdPics_AdPicID",
                        column: x => x.AdPicID,
                        principalTable: "AdPics",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdPics_AdID",
                table: "AdPics",
                column: "AdID");

            migrationBuilder.CreateIndex(
                name: "IX_AdPicUrls_AdPicID",
                table: "AdPicUrls",
                column: "AdPicID");

            migrationBuilder.CreateIndex(
                name: "IX_AdPicUrls_ID",
                table: "AdPicUrls",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_AdPics_ScrapedAds_AdID",
                table: "AdPics",
                column: "AdID",
                principalTable: "ScrapedAds",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdPics_ScrapedAds_AdID",
                table: "AdPics");

            migrationBuilder.DropTable(
                name: "AdPicUrls");

            migrationBuilder.DropIndex(
                name: "IX_AdPics_AdID",
                table: "AdPics");

            migrationBuilder.DropColumn(
                name: "AdID",
                table: "AdPics");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "AdPics");

            migrationBuilder.AddColumn<string>(
                name: "UrlHash",
                table: "AdPics",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
