﻿using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable All

namespace DataBase.Migrations
{
    public partial class urlhash : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UrlHash",
                table: "AdPics",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UrlHash",
                table: "AdPics");
        }
    }
}
