﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataBase.Migrations
{
    public partial class Rating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdRating",
                columns: table => new
                {
                    ID = table.Column<int>(maxLength: 255, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ScrapedAdId = table.Column<int>(nullable: false),
                    VkUserId = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdRating", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AdRating_ScrapedAds_ScrapedAdId",
                        column: x => x.ScrapedAdId,
                        principalTable: "ScrapedAds",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdRating_VkUsers_VkUserId",
                        column: x => x.VkUserId,
                        principalTable: "VkUsers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdRating_ScrapedAdId",
                table: "AdRating",
                column: "ScrapedAdId");

            migrationBuilder.CreateIndex(
                name: "IX_AdRating_VkUserId",
                table: "AdRating",
                column: "VkUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdRating");
        }
    }
}
