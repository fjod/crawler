﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataBase.Migrations
{
    public partial class RatingInContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdRating_ScrapedAds_ScrapedAdId",
                table: "AdRating");

            migrationBuilder.DropForeignKey(
                name: "FK_AdRating_VkUsers_VkUserId",
                table: "AdRating");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdRating",
                table: "AdRating");

            migrationBuilder.RenameTable(
                name: "AdRating",
                newName: "Ratings");

            migrationBuilder.RenameIndex(
                name: "IX_AdRating_VkUserId",
                table: "Ratings",
                newName: "IX_Ratings_VkUserId");

            migrationBuilder.RenameIndex(
                name: "IX_AdRating_ScrapedAdId",
                table: "Ratings",
                newName: "IX_Ratings_ScrapedAdId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ratings",
                table: "Ratings",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_ScrapedAds_ScrapedAdId",
                table: "Ratings",
                column: "ScrapedAdId",
                principalTable: "ScrapedAds",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_VkUsers_VkUserId",
                table: "Ratings",
                column: "VkUserId",
                principalTable: "VkUsers",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_ScrapedAds_ScrapedAdId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_VkUsers_VkUserId",
                table: "Ratings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ratings",
                table: "Ratings");

            migrationBuilder.RenameTable(
                name: "Ratings",
                newName: "AdRating");

            migrationBuilder.RenameIndex(
                name: "IX_Ratings_VkUserId",
                table: "AdRating",
                newName: "IX_AdRating_VkUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Ratings_ScrapedAdId",
                table: "AdRating",
                newName: "IX_AdRating_ScrapedAdId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdRating",
                table: "AdRating",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_AdRating_ScrapedAds_ScrapedAdId",
                table: "AdRating",
                column: "ScrapedAdId",
                principalTable: "ScrapedAds",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdRating_VkUsers_VkUserId",
                table: "AdRating",
                column: "VkUserId",
                principalTable: "VkUsers",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
