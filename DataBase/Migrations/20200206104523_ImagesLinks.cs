﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable All

namespace DataBase.Migrations
{
    public partial class ImagesLinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdPicsLinks",
                columns: table => new
                {
                    ID = table.Column<int>(maxLength: 255, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BigImageID = table.Column<int>(nullable: true),
                    SmallImageID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdPicsLinks", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AdPicsLinks_AdPics_BigImageID",
                        column: x => x.BigImageID,
                        principalTable: "AdPics",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdPicsLinks_AdPics_SmallImageID",
                        column: x => x.SmallImageID,
                        principalTable: "AdPics",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdPicsLinks_BigImageID",
                table: "AdPicsLinks",
                column: "BigImageID");

            migrationBuilder.CreateIndex(
                name: "IX_AdPicsLinks_ID",
                table: "AdPicsLinks",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_AdPicsLinks_SmallImageID",
                table: "AdPicsLinks",
                column: "SmallImageID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdPicsLinks");
        }
    }
}
