﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable All
namespace DataBase.Migrations
{
    public partial class vk_auth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Ava1",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Uid",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VkUsers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Uid = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Ava1 = table.Column<string>(nullable: true),
                    Ava2 = table.Column<string>(nullable: true),
                    Guid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VkUsers", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VkUsers_ID",
                table: "VkUsers",
                column: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VkUsers");

            migrationBuilder.DropColumn(
                name: "Ava1",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Uid",
                table: "AspNetUsers");
        }
    }
}
