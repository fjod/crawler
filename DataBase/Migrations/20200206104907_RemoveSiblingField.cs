﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataBase.Migrations
{
    public partial class RemoveSiblingField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdPics_AdPics_SiblingID",
                table: "AdPics");

            migrationBuilder.DropIndex(
                name: "IX_AdPics_SiblingID",
                table: "AdPics");

            migrationBuilder.DropColumn(
                name: "SiblingID",
                table: "AdPics");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SiblingID",
                table: "AdPics",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdPics_SiblingID",
                table: "AdPics",
                column: "SiblingID");

            migrationBuilder.AddForeignKey(
                name: "FK_AdPics_AdPics_SiblingID",
                table: "AdPics",
                column: "SiblingID",
                principalTable: "AdPics",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
