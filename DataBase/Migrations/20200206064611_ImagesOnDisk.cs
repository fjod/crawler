﻿using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable All

namespace DataBase.Migrations
{
    public partial class ImagesOnDisk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "AdPics");

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "AdPics",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SiblingID",
                table: "AdPics",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdPics_SiblingID",
                table: "AdPics",
                column: "SiblingID");

            migrationBuilder.AddForeignKey(
                name: "FK_AdPics_AdPics_SiblingID",
                table: "AdPics",
                column: "SiblingID",
                principalTable: "AdPics",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdPics_AdPics_SiblingID",
                table: "AdPics");

            migrationBuilder.DropIndex(
                name: "IX_AdPics_SiblingID",
                table: "AdPics");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "AdPics");

            migrationBuilder.DropColumn(
                name: "SiblingID",
                table: "AdPics");

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "AdPics",
                type: "longblob",
                nullable: true);
        }
    }
}
