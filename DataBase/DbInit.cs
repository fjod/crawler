﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataBase
{
    public class DbInit : IDbInitializer
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public DbInit(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public void Initialize()
        {
            var serviceScope = _scopeFactory.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<DataContext>();
            context.Database.Migrate();
        }

        public async Task SeedData()
        {
            var serviceScope = _scopeFactory.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<DataContext>();

            var meAsSuperAdmin = await context.Users.FirstOrDefaultAsync(u => u.Uid == SuperAdminUid);
            //fix SuperAdmin user
            if (meAsSuperAdmin != null)
            {
                var myClaims = await context.UserClaims.Where(claim => claim.UserId == meAsSuperAdmin.Id).ToListAsync();
                var adminClaim = myClaims.FirstOrDefault(c => c.ClaimType == ClaimTypes.Role);
                if (adminClaim?.ClaimValue == "SuperAdmin") return;
                if (adminClaim != null)
                {
                    adminClaim.ClaimValue = "SuperAdmin";
                }
                else
                {
                    var claimRole1 = AppUser.CreateSampleClaim(meAsSuperAdmin, ClaimTypes.Role,
                        "SuperAdmin");
                    context.UserClaims.Add(claimRole1);
                }

                var nameClaim = myClaims.FirstOrDefault(c => c.ClaimType == ClaimTypes.Role);
                if (nameClaim == null)
                {
                    var claimName1 =
                        AppUser.CreateSampleClaim(meAsSuperAdmin, ClaimTypes.Name, meAsSuperAdmin.UserName);
                    context.UserClaims.Add(claimName1);
                }

                await context.SaveChangesAsync();
                return;
            }

            //add SuperAdmin user
            VkUser vk = new VkUser
            {
                ID = 1,
                Ava1 = "https://sun9-23.userapi.com/c639117/v639117582/3dfed/mVfPFrCo5w8.jpg?ava=1",
                Ava2 = "https://sun9-23.userapi.com/c639117/v639117582/3dfed/mVfPFrCo5w8.jpg?ava=1",
                Guid = "898208af-7aca-4756-826f-a452c3501405",
                Name = "Михаил",
                Surname = "Снытко",
                Uid = SuperAdminUid
            };
            context.VkUsers.Add(vk);

            AppUser appUser = new AppUser
            {
                Id = "aa077d71-637d-48dd-852f-0550f5ec57ee",
                UserName = "dcd1571b-c440-426d-9f0e-69753d384806",
                NormalizedUserName = "DCD1571B-C440-426D-9F0E-69753D384806",
                SecurityStamp = "A5UDZ52LDVHQOUU2QPC4FTRHE6J5YDRL",
                ConcurrencyStamp = "bad5b05c-2942-4457-b616-b3f8ded14d26",
                Ava1 = "https://sun9-23.userapi.com/c639117/v639117582/3dfed/mVfPFrCo5w8.jpg?ava=1",
                Uid = SuperAdminUid
            };
            context.Users.Add(appUser);

            var claimName = AppUser.CreateSampleClaim(appUser, ClaimTypes.Name, appUser.UserName);
            var claimRole = AppUser.CreateSampleClaim(appUser, ClaimTypes.Role,
                "SuperAdmin");
            context.UserClaims.Add(claimName);
            context.UserClaims.Add(claimRole);

            await context.SaveChangesAsync();
        }

        public readonly int TotalAdsCount = 10;
        public readonly int TotalUsersCount = 5;
        public async Task AddSampleData()
        {
            var serviceScope = _scopeFactory.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<DataContext>();
            var gotSampleUsers = await context.VkUsers.AnyAsync(c => c.Name.Contains(VkUser.SampleAdder));
            var gotSampleAds = await context.ScrapedAds.Include(ad => ad.AdPics).ToListAsync();

            if (gotSampleUsers && gotSampleAds.Count > 0) return;

            Random random = new Random();
            var vkUserId = random.Next(10000, 50000);

            List<ScrapedAd> sampleAds = new List<ScrapedAd>();
            for (int i = 0; i < TotalAdsCount; i++)
            {
                var sampleAd = ScrapedAd.CreateMockAd(DateTime.Now-TimeSpan.FromDays(35*i));
                sampleAd.ID = vkUserId + i;
                sampleAd.UniqueId = sampleAd.ID;
                context.ScrapedAds.Add(sampleAd);
                sampleAds.Add(sampleAd);
                
                for (int j = 0; j < 10; j++)
                {
                    var bigPic = AdPic.CreateMock(true);
                    var smallPic = AdPic.CreateMock(false);
                    if (sampleAd.AdPics == null)
                        sampleAd.AdPics= new List<AdPic>();
                    sampleAd.AdPics.Add(bigPic);
                    sampleAd.AdPics.Add(smallPic);
                    AdsAndImages apc1 = new AdsAndImages {Ad = sampleAd, Image = bigPic};
                    AdsAndImages apc2 = new AdsAndImages {Ad = sampleAd, Image = smallPic};
                    context.AdImageLink.Add(apc1);
                    context.AdImageLink.Add(apc2);
                }
            }

            for (int i = 0; i < TotalUsersCount; i++)
            {
                var vkUser = VkUser.CreateSampleUser(random);
                context.VkUsers.Add(vkUser);

                var appUser = AppUser.CreateSampleUser(vkUser, Guid.NewGuid());
                context.Users.Add(appUser);

                var claimName = AppUser.CreateSampleClaim(appUser, ClaimTypes.Name, appUser.UserName);
                var claimRole = AppUser.CreateSampleClaim(appUser, ClaimTypes.Role,
                    i % 5 == 0 ? "Admin" : "User");
                context.UserClaims.Add(claimName);
                context.UserClaims.Add(claimRole);

                foreach (var scrapedAd in sampleAds)
                {
                    var comment = Comment.GetSampleComment(scrapedAd, vkUser);
                    scrapedAd.Comments.Add(comment);

                    var rating = AdRating.GetSampleRating(vkUser, scrapedAd, random);
                    scrapedAd.Ratings.Add(rating);
                }
            }

            await context.SaveChangesAsync();
        }

        private const string SuperAdminUid = "3596363";

        public async Task RemoveSampleData()
        {
            var serviceScope = _scopeFactory.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<DataContext>();

            var vkUsers = await context.VkUsers.Where(user => user.Name.Contains(VkUser.SampleAdder)).ToListAsync();
            if (!vkUsers.Any()) return;

            var ads = await context.ScrapedAds.Where(ad => ad.City.Contains(VkUser.SampleAdder)).ToListAsync();

            var superAdmin = await context.Users.FirstAsync(c => c.Uid == SuperAdminUid);
            var users = await context.Users.Where(c => c.Uid != SuperAdminUid).ToListAsync();

            var claims =
                await context.UserClaims.Where(c => c.UserId != superAdmin.Id).ToListAsync();

            context.Users.RemoveRange(users);
            context.UserClaims.RemoveRange(claims);
            context.VkUsers.RemoveRange(vkUsers);
            context.ScrapedAds.RemoveRange(ads);
            //ratings and comments should be cascade deleted

            await context.SaveChangesAsync();
        }
    }
}