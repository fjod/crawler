﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace DataBase
{
    public sealed class DataContext : IdentityDbContext<AppUser>
    {
        private readonly IHostingDbEnv _env;
        public DbSet<ScrapedAd> ScrapedAds { get; set; }
        public DbSet<AdPic> AdPics { get; set; }
        public DbSet<AdsAndImages> AdImageLink { get; set; }
        public DbSet<AdPicUrl> AdPicUrls { get; set; }
        public DbSet<AdPicsLinks> AdPicsLinks { get; set; }

        public DbSet<VkUser> VkUsers { get; set; }

        public DbSet<AdRating> Ratings { get; set; }

        public DbSet<Comment> Comments { get; set; }

       
        //migrations ctor
        // public DataContext()
        // {
        //     
        // }
        /// <summary>
        /// debug/prod ctor
        /// </summary>
        public DataContext(IHostingDbEnv env)
        {
            _env = env;
          
            Database.SetCommandTimeout(30);
        }

        /// <summary>
        /// this constructor is for tests only!
        /// </summary>
        /// <param name="options"></param>
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            _skipConfiguringForTest = true;
        }

        private readonly bool _skipConfiguringForTest;


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_skipConfiguringForTest) return;

            Settings settings = new Settings();
            string connectionString;
          
            if (_env == null)
                connectionString = settings.Get("ConnectionStringProduction");
            else
            {
                if (_env.IsDebugEnvironment())
                    connectionString = settings.Get("ConnectionStringDebug");
                else
                    connectionString = settings.Get("ConnectionStringProduction");
            }
           
            //connectionString = "server=db;port=3306;userid=dbuser;password=dbuserpassword;database=accountowner;";
            
            optionsBuilder.UseMySql(connectionString,
                builder =>
                {
                    builder.ServerVersion(new Version(5, 6, 46), ServerType.MySql);
                    builder.CommandTimeout(10);
                    builder.EnableRetryOnFailure(5);
                });
            optionsBuilder.EnableDetailedErrors();
         
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<AppUser>(entity => entity.Property(m => m.Id).HasMaxLength(127));

            builder.Entity<IdentityUserLogin<string>>(e => e.Property(p => p.UserId).HasMaxLength(127));
            builder.Entity<IdentityUserRole<string>>(e => e.Property(p => p.UserId).HasMaxLength(127));
            builder.Entity<IdentityUserToken<string>>(e => e.Property(p => p.UserId).HasMaxLength(127));

            builder.Entity<AdPic>().HasIndex(p => p.ID);
            builder.Entity<AdsAndImages>().HasIndex(p => p.ID);
            builder.Entity<ScrapedAd>().HasIndex(p => p.ID);
            builder.Entity<AdPicUrl>().HasIndex(p => p.ID);
            builder.Entity<AdPicsLinks>().HasIndex(p => p.ID);
            builder.Entity<VkUser>().HasIndex(p => p.ID);


            builder.Entity<ScrapedAd>()
                .HasMany(c => c.AdPics)
                .WithOne(e => e.Ad);

            builder.Entity<AdPic>()
                .HasMany(c => c.AdditionalUrls)
                .WithOne(e => e.AdPic);

            builder.Entity<IdentityUserLogin<string>>().HasKey(p => p.UserId);
            builder.Entity<IdentityUserRole<string>>().HasKey(p => p.UserId);
            builder.Entity<IdentityUserToken<string>>().HasKey(p => p.UserId);

            builder.Entity<IdentityRole>(e => e.Property(p => p.Id).HasMaxLength(127));
            builder.Entity<IdentityRole>(e => e.Property(p => p.ConcurrencyStamp).HasMaxLength(255));

            builder.Entity<AppUser>(entity =>
            {
                entity.Property(m => m.Email).HasMaxLength(127);
                entity.Property(m => m.NormalizedEmail).HasMaxLength(127);
                entity.Property(m => m.NormalizedUserName).HasMaxLength(127);
                entity.Property(m => m.UserName).HasMaxLength(127);
            });
            builder.Entity<IdentityRole>(entity =>
            {
                entity.Property(m => m.Name).HasMaxLength(127);
                entity.Property(m => m.NormalizedName).HasMaxLength(127);
            });
            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.Property(m => m.LoginProvider).HasMaxLength(127);
                entity.Property(m => m.ProviderKey).HasMaxLength(127);
            });
            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.Property(m => m.UserId).HasMaxLength(127);
                entity.Property(m => m.RoleId).HasMaxLength(127);
            });
            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.Property(m => m.UserId).HasMaxLength(127);
                entity.Property(m => m.LoginProvider).HasMaxLength(127);
                entity.Property(m => m.Name).HasMaxLength(127);
            });
        }
    }
}