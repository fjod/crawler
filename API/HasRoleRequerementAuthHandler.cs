﻿using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace API
{
    internal class HasRoleRequerementAuthHandler : AuthorizationHandler<HasRoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            HasRoleRequirement requirement)
        {
            if (context.User.HasClaim(c => c.Type == JwtRegisteredClaimNames.NameId))
                if (context.User.FindFirst(c => c.Type == JwtRegisteredClaimNames.Typ).Value == requirement.RoleName)
                    context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
    
    internal class HasRoleRequirement : IAuthorizationRequirement
    {
        public readonly string RoleName;

        public HasRoleRequirement(string roleName)
        {
            RoleName = roleName;
        }
    }
}