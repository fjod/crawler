using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class MediatorBase : ControllerBase
    {
        protected IMediator Mediator =>
             (IMediator) HttpContext.RequestServices.GetService(typeof(IMediator));
    }
}