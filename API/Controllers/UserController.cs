using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTO;
using Application.MediatR.User;
using Application.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : MediatorBase
    {
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<ActionResult<SpaUser>> Register(Register.Command command)
        {
            if (string.IsNullOrEmpty(command.Id)) return BadRequest(new {Error = "no id"});
            return await Mediator.Send(command);
        }

        [HttpGet("info")]
        [Authorize]
        public async Task<ActionResult<SpaUser>> CurrentUser()
        {
            return await Mediator.Send(new CurrentUser.Query());
        }
        
        [HttpPost("assignRole")]
        [Authorize]
        public async Task<IActionResult> AssignRoleToUser(AssignRole.Request command)
        {
            await Mediator.Send(command);
            return new OkResult();
        }
        
        [HttpPost("assignBan")]
        [Authorize]
        public async Task<IActionResult> AssignBanToUser(AssignBan.Request command)
        {
            await Mediator.Send(command);
            return new OkResult();
        }
        
        [HttpGet("list")]
        [Authorize]
        public async Task<ActionResult<List<DTO_UserWithCreds>>> List()
        {
            return await Mediator.Send(new ListUsers.Query());
        }
    }
}