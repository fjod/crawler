using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.MediatR.User;
using Application.Misc;
using Application.User;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;



namespace API.Controllers
{
    public class Home : Controller
    {
        private readonly IMediator _mediator;

        public Home(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET
        [HttpGet("~/Home")]
        public IActionResult Index()
        {
         //   return View("Index", await HttpContext.GetExternalProvidersAsync());
         return Challenge(new AuthenticationProperties {RedirectUri = "/CompleteRegistration"}, "Vkontakte");
        }

        [HttpPost("/Home/signin")]
        public async Task<IActionResult> SignIn([FromForm] string provider)
        {
            // Note: the "provider" parameter corresponds to the external
            // authentication provider chosen by the user agent.
            if (string.IsNullOrWhiteSpace(provider))
            {
                return BadRequest();
            }

            if (!await HttpContext.IsProviderSupportedAsync(provider))
            {
                return BadRequest();
            }

            // Instruct the middleware corresponding to the requested external identity
            // provider to redirect the user agent to its own authorization endpoint.
            // Note: the authenticationScheme parameter must match the value configured in Startup.cs
            //return Challenge(new AuthenticationProperties {RedirectUri = "/Home"}, provider
            return Challenge(new AuthenticationProperties {RedirectUri = "/CompleteRegistration"}, provider);
        }

        [HttpGet("Home/signout")]
        [HttpPost("Home/signout")]
        public async Task<IActionResult> SignOut()
        {
            // Instruct the cookies middleware to delete the local cookie created
            // when the user agent is redirected from the external identity provider
            // after a successful authentication flow (e.g Google or Facebook).
            // return SignOut(new AuthenticationProperties {RedirectUri = "/Home"},
            //     CookieAuthenticationDefaults.AuthenticationScheme);
          
            // // //delete token in front-end after this
            await HttpContext.SignOutAsync();
             //
             // SignOut(new AuthenticationProperties {RedirectUri =    "http://localhost:3000"},
             //    CookieAuthenticationDefaults.AuthenticationScheme);
             //
             //return Redirect("http://localhost:3000"); //when used without wwwroot/static files
             return Redirect("/"); //static files are here!
        }

          
        
        [HttpGet("/CompleteRegistration")]
        [HttpPost("/CompleteRegistration")]
        public async Task<IActionResult> CompleteRegistration()
        {
            var command = new SaveVkUser.Command();
            
            if (!HttpContext.User.Claims.Any()) 
                throw new RestException(HttpStatusCode.NotFound, new {Error = "no claims for user"});
            
            var user = ParseClaims();

            if (String.IsNullOrEmpty(user.Uid))
                throw new RestException(HttpStatusCode.NotFound, new {Error = "no id in claims"});

            command.UserInfo = user;
            command.UserInfo.Guid = command.TempId.ToString();
            
            await _mediator.Send(command);
            
            HttpContext.Response.Cookies.Append("fb_id",command.TempId.ToString());

            //return Redirect("http://localhost:3000"); //when used without wwwroot/static files
            return Redirect("/"); //static files are here!
        }

        private VkUser ParseClaims()
        {
            var user = new VkUser();
            foreach (var userClaim in HttpContext.User.Claims)
            {
                switch (userClaim.Type)
                {
                    case (LoggedInUser.uid):
                    {
                        user.Uid = userClaim.Value;
                        break;
                    }
                    case (LoggedInUser.ava1):
                    {
                        user.Ava1 = userClaim.Value;
                        break;
                    }
                    case (LoggedInUser.ava2):
                    {
                        user.Ava2 = userClaim.Value;
                        break;
                    }
                    case (LoggedInUser.givenname):
                    {
                        user.Name = userClaim.Value;
                        break;
                    }
                    case (LoggedInUser.surname):
                    {
                        user.Surname = userClaim.Value;
                        break;
                    }
                }
            }

            return user;
        }
    }
}