using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MediatR.Ads.Editor;
using Domain.Frontend;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers.DbEditor
{
    [ApiController]
    [Route("api/[controller]")]
    public class EditorController : MediatorBase
    {
        [HttpGet("list")]
        public async Task<List<LatestAd>> Get(int take = 10, int skip = 0)
        {
            return await Mediator.Send(new List.Query{Skip = skip, Take = take});
        }
        
        [HttpGet("details/{id}")]
        public async Task<EditorUniqueAd> GetDetails(int id)
        {
            return await Mediator.Send(new UniqueDetails.Query{ID = id});
        }
    }
}