using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MediatR.Comments;
using Application.Misc;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class CommentController : MediatorBase
    {
        [HttpPost]
        public async Task<Comment> Add([FromBody] Add.Command command)
        {
            return await Mediator.Send(command);
        }
        
        /// <summary>
        /// deletes comment, only for admins
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new Delete.Request{Id = id});
            return new OkResult();
        }
        
        /// <summary>
        /// deletes comment, only for admins
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/delete/{id}")]
        public async Task<IActionResult> UserDelete(int id)
        {
            await Mediator.Send(new UserDelete.Request{Id = id});
            return new OkResult();
        }
        
        [HttpPost]
        [Route("{id}")]
        public async Task<IActionResult> Edit([FromBody] Edit.Request request)
        {
            await Mediator.Send(request);
            return new OkResult();
        }
        
        /// <summary>
        /// list of comments for advert
        /// </summary>
        /// <param name="id">advert id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<List<Comments>> List(int id)
        {
             return await Mediator.Send(new List.Query{Id = id});
        }
        
    }
}