using System.Threading.Tasks;
using Application.MediatR.Ads.Images;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImagesController : MediatorBase
    {
        [HttpGet("big/{id}")]
        public async Task<AdPic> GetBigImage(int id)
        {
            return await Mediator.Send(new GetBigImage.Query {Id = id});
        }
    }
}