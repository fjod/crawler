﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTO;
using Application.MediatR.Ads;
using Application.MediatR.Ads.Images;
using Application.Misc;
using Domain;
using Domain.Frontend;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PageController : MediatorBase
    {
        
        [HttpGet("AnonAccess")]
        [AllowAnonymous]
        public IActionResult AnonAccess()
        {
            return new OkResult();
        }

        
        [HttpGet("TestAuth")]
        [Authorize]
        public IActionResult TestAuth()
        {
            return new OkResult();
        }
        
        [HttpGet("TestAdmin")]
        [Authorize(Policy = "SuperAdmin")]
        public IActionResult TestRole()
        {
            return new OkResult();
        }
         
       
        [HttpGet("{id}")]
        public async Task<ActionResult<ScrapedAd>> Details(int id)
        {
            return await Mediator.Send(new Details.Query{Id = id});
        }

        [HttpGet(template:"list")]
        [AllowAnonymous] 
        public async Task<ActionResult<List<DTO_ScrapedAdPreview>>> List(int take = 4, int skip = 0, SortAds rate = SortAds.ByDate)
        {
            return await Mediator.Send(new List.Query{Skip = skip, Take = take, Sort = rate});
        }
        /// <summary>
        /// many small images for uniqueAd
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous] 
        [HttpGet("images")] 
        public async Task<ActionResult<List<AdPic>>> Images(int id)
        {           
            return await Mediator.Send(new SmallForAd.Query{UniqueId = id});
        }
        
        /// <summary>
        /// textfield changes history for unique id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       
        [HttpGet("history")] 
        public async Task<ActionResult<List<AdHistory>>> History(int id)
        {
            return await Mediator.Send(new History.Query{UniqueId = id});
        }
        /// <summary>
        /// what images were added from prevId ad to nextId ad
        /// </summary>
        /// <param name="prevId"></param>
        /// <param name="nextId"></param>
        /// <returns></returns>
         
        [HttpGet("picsDifference")]
        public async Task<ActionResult<List<AdPic>>> PicsDifference(int prevId, int nextId)
        {
            return await Mediator.Send(new PicsDifference.Query{NextId = nextId, PrevId = prevId});
        }

        [HttpPost("rating")]
        public async Task<ActionResult> SetRating([FromBody] SetRating.Command command)
        {
            await Mediator.Send(command);
            return new OkResult();
        }

        [HttpGet("rating")]
        public async Task<ActionResult<DTO_AdRating>> GetRating(int uniqueId)
        {
            return await Mediator.Send(new GetRating.Request{AdUniqueId = uniqueId});
        }
      
        [AllowAnonymous]
        [HttpGet("totalPriceChange")]
        public async Task<ActionResult<DTO_AdsPricesChange>> GetPricesChange()
        {
            return await Mediator.Send(new GetTotalPriceChange.Request());
        }
        
        [Authorize]
        [HttpGet("priceChange/{id}")]
        public async Task<ActionResult<DTO_AdsPricesChange>> GetPriceChange(int id)
        {
            return await Mediator.Send(new PriceChange.Request{UniqueId = id});
        }
    }
}