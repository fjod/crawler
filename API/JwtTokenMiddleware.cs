﻿using System.Security.Principal;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;

namespace API
{
    public static class JwtTokenMiddleware
    {
        public static IApplicationBuilder UseJwtTokenMiddleware(this IApplicationBuilder app,
            string schema = "Bearer")
        {
            return app.Use(async (ctx, next) =>
            {
                IIdentity identity = ctx.User.Identity;
                if (identity == null || !identity.IsAuthenticated)
                {
                    AuthenticateResult result = await ctx.AuthenticateAsync(schema);
                    if (result.Succeeded && result.Principal != null)
                        ctx.User = result.Principal;
                }

                await next();
            });
        }
    }
}