using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Text;
using Application.DTO;
using Application.Interfaces;
using Application.MediatR.Ads;
using Application.User;
using AutoMapper;
using DataBase;
using Domain;
using InfraStructure;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
        }

        private IWebHostEnvironment CurrentEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var s = new Settings();
            services.AddControllers().AddNewtonsoftJson(opt =>
            {
                opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddScoped<DataContext, DataContext>();
            services.AddScoped<IJWTGenerator, JwtGenerator>();
            services.AddMediatR(typeof(List.Handler).Assembly);
            services.AddSingleton<IAuthorizationHandler, HasRoleRequerementAuthHandler>();
            services.AddScoped<IDbInitializer, DbInit>();
            services.AddSingleton<IHostingDbEnv, HostingEnv>();

            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:3000")
                        .WithExposedHeaders("WWW-Authenticate"); //react app port!
                });
            });


            var build = services.AddIdentityCore<AppUser>();
            var identityBuilder = new IdentityBuilder(build.UserType, build.Services);
            identityBuilder.AddEntityFrameworkStores<DataContext>();
            identityBuilder.AddSignInManager<SignInManager<AppUser>>();


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(s.Get("TokenKey")));


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
            {
                opt.SecurityTokenValidators.Clear();
                opt.SecurityTokenValidators.Add(new JwtSecurityTokenHandler
                {
                    InboundClaimTypeMap = new Dictionary<string, string>()
                });
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = key,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserAccessor, UserAccessor>();

            services.AddMvc(options => options.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            var vkSettings = s.GetVkCreds(CurrentEnvironment.IsProduction());

            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie(options =>
                {
                    options.LoginPath = "/signin"; //this works for MVC login only
                    options.LogoutPath = "/signout";
                }).AddVkontakte(options =>
                {
                    options.ClientId = vkSettings.ClientId;
                    options.ClientSecret = vkSettings.ClientSecret;
                    options.CorrelationCookie.SameSite = SameSiteMode.Unspecified;
                });


            services.AddAutoMapper(Assembly.GetAssembly(typeof(DTO_VkUser)));

            services.AddAuthorization(opt =>
            {
                var defaultBuilder = new AuthorizationPolicyBuilder();

                var defaultPolicy = defaultBuilder.RequireAuthenticatedUser()
                    .RequireClaim(JwtRegisteredClaimNames.NameId).Build();
                opt.DefaultPolicy = defaultPolicy;

                opt.AddPolicy(UserType.Admin.ToString(),
                    builder => { builder.RequireClaim(JwtRegisteredClaimNames.Typ, UserType.Admin.ToString()); });

                opt.AddPolicy(UserType.SuperAdmin.ToString(),
                    builder => { builder.RequireClaim(JwtRegisteredClaimNames.Typ, UserType.SuperAdmin.ToString()); });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using var scope = scopeFactory.CreateScope();
            var dbEnv = scope.ServiceProvider.GetService<IHostingDbEnv>();
            (dbEnv as HostingEnv).IsDebug = env.IsDevelopment();

            var dbInitializer = scope.ServiceProvider.GetService<IDbInitializer>();
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                dbInitializer.Initialize();
                dbInitializer.SeedData();
                dbInitializer.AddSampleData();
            }
            else
            {
                dbInitializer.Initialize();
                dbInitializer.SeedData();
                dbInitializer.RemoveSampleData();
            }

            app.UseXContentTypeOptions(); //content-sniffing
            app.UseReferrerPolicy(apt => apt.NoReferrer()); //do not pass info to other sites when refer to them
            app.UseXXssProtection(opt => opt.EnabledWithBlockMode()); //stop from loading when XSS detected
            app.UseXfo(opt => opt.Deny()); //block i-frames & clickjacking

            if (env.IsProduction())
            {
                app.UseDefaultFiles(); //use index.html
                // Required to serve files with no extension in the .well-known folder
                var options = new StaticFileOptions()
                {
                    ServeUnknownFileTypes = true
                };
                app.UseStaticFiles(options);
            }


            //app.UseHttpsRedirection();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseJwtTokenMiddleware();
            app.UseAuthorization();

            app.UseCors("CorsPolicy");

            // app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");

                endpoints.MapFallbackToController("Index", "Fallback");
            });
        }
    }

   
}