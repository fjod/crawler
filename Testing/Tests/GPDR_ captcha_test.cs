﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using BrowseSharp.Common;
using Crawler;
using Crawler.Browser;
using DataBase;
using Domain;
using InfraStructure.Captcha;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{

  
    public class GdprCaptchaTest : BaseTest
    {
        private readonly ITestOutputHelper _helper;

        public GdprCaptchaTest(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        //has internet connection
        [Fact]
        public void Captcha_ImageToText_Test()
        {
            ICaptchaSolver sut = Injection.GetType<ICaptchaSolver>();
            Assert.NotNull(sut);

            var ret = sut.GetBalance();
            _helper.WriteLine($"Captcha has balance = {ret}");
            Assert.True(ret > 0);
        }

        [Fact]
        public void InjectorTest()
        {
            var sut = Injection;
            Assert.NotNull(sut);

            _helper.WriteLine("log to file create attempt");
            var logToFile = sut.GetType<ISpiderLog>();
            Assert.NotNull(logToFile);

            _helper.WriteLine("DataContext create attempt");
            var dataContext = sut.GetType<DataContext>();
            Assert.NotNull(dataContext);

            _helper.WriteLine("ISaveResult create attempt");
            var saveResult = sut.GetType<ISaveResult>();
            Assert.NotNull(saveResult);

            _helper.WriteLine("IWebAccess create attempt");
            var picDownload = sut.GetType<IWebAccess>();
            Assert.NotNull(picDownload);

            _helper.WriteLine("ICompareString create attempt");
            var compareString = sut.GetType<ICompareString>();
            Assert.NotNull(compareString);

            _helper.WriteLine("ICaptchaSolver create attempt");
            var captchaSolver = sut.GetType<ICaptchaSolver>();
            Assert.NotNull(captchaSolver);

            _helper.WriteLine("IGetImageForCaptchaURL create attempt");
            var getImageForCaptchaUrl = sut.GetType<IGetImageForCaptchaUrl>();
            Assert.NotNull(getImageForCaptchaUrl);

            _helper.WriteLine("Settings create attempt");
            var settings = sut.GetType<Settings>();
            Assert.NotNull(settings);
        }

        //also has connection to the internet
        [Fact]
        public async void CanDownloadAndSaveImage()
        {
            GetCaptchaUrlFromAutoRuUrl sut = new GetCaptchaUrlFromAutoRuUrl();
            Assert.NotNull(sut);
            _helper.WriteLine("Started to download file");
            Assert.NotNull(Client);
            var filePath = await sut.GetFileAndSaveIt("https://picsum.photos/id/975/200/300", Client);
            _helper.WriteLine("File is here");
            Bitmap bm = new Bitmap(filePath);
            Assert.NotNull(bm);
            bm.Dispose();
            File.Delete(filePath);
            _helper.WriteLine("File is a picture!");
        }

       

        [Fact]
        public async Task Test_GDPR_Captcha()
        {
            var spiderLog = new TestSpiderLog(_helper);
            Mock<IInjection> injectorMock = new Mock<IInjection>();
            injectorMock.Setup(foo => foo.GetType<ISpiderLog>()).Returns(spiderLog);
            //injectorMock.Setup(foo => foo.GetType<DataContext>()).Returns(new DataContext());
            
            injectorMock.Setup(foo => foo.GetType<ICaptchaSolver>())
                .Returns(new CaptchaSolver(new Settings(), spiderLog));
            injectorMock.Setup(foo => foo.GetType<IGetImageForCaptchaUrl>())
                .Returns(new GetCaptchaUrlFromAutoRuUrl());
            injectorMock.Setup(foo => foo.GetType<Settings>()).Returns(new Settings());
            injectorMock.Setup(foo => foo.GetType<IWebAccess>()).Returns(new WebAccess(spiderLog));


            CuteSpider spider = new CuteSpider(injectorMock.Object);
            var advTable = new AdTableLoader(spiderLog);
            var tableUri = InitialSettings.GetBaseUrl(0);

            // var test = spider.Navigate("http://www.reliply.org/tools/requestheaders.php");  //tool to check headers

            //check if we have any connection
            spiderLog.LogMessage("Test_GDPR_Captcha: test connection..");

            bool gotWorkingProxy = false;
            for (int i = 0; i < 10; i++)
            {
                //for some reason free proxy sometimes works badly
                spiderLog.LogMessage($"Test_GDPR_Captcha: Attempt to use check connection #{i} ");
                var proxyOk = await spider.WebAccess.Navigate(InitialSettings.RobotsUrl);

                if (proxyOk.Response.StatusCode == HttpStatusCode.OK)
                {
                    gotWorkingProxy = true;
                    break;
                }

                Thread.Sleep(5000);
            }

            IDocument document;
            Assert.True(gotWorkingProxy);
            spiderLog.LogMessage("Test_GDPR_Captcha: internet connection ok");

            spiderLog.LogMessage($"looking for uri {tableUri}");
            
            EResponseType responseType = EResponseType.Error;
            for (int i = 0; i < 5; i++)
            {
                spiderLog.LogMessage($"Test_GDPR_Captcha: Get document, attempt #{i}");
                document = await spider.WebAccess.Navigate(tableUri);
                responseType = advTable.FindDocumentType_Table(document);
                spiderLog.LogMessage($"Test_GDPR_Captcha: doc is here, it has type ${responseType}");
                if ((responseType == EResponseType.EnterCaptcha) || (responseType == EResponseType.AcceptRules))
                    break;
                
                if (responseType == EResponseType.GoodResponse) return; //no proxy for test - no test at all
                Thread.Sleep(5000);
            }

            Assert.True((responseType == EResponseType.EnterCaptcha) || (responseType == EResponseType.AcceptRules));
            spiderLog.LogMessage("Test_GDPR_Captcha: Assert if it has good cookies");
            Assert.Contains(spider.WebAccess.PrevCookies, c => c.Name == "_csrf_token");
            //Assert.Contains(spider.WebAccess.PrevCookies, c => c.Name == "autoru_sid");
            //Assert.Contains(spider.WebAccess.PrevCookies, c => c.Name == "autoruuid");
            spiderLog.LogMessage("Test_GDPR_Captcha: cookies ok");

            //var gdprWasFixed = false;
            Uri actualAdUri = null;
            if (responseType == EResponseType.AcceptRules)
            {
                _helper.WriteLine("Test_GDPR_Captcha: found gdpr, attempting to fix it");
                var workaround = new GDPR_Workaround(new TestSpiderLog(_helper));
                var ret = await workaround.FixGDPR(spider.WebAccess.PrevCookies);
                Assert.True(ret);
                
                Thread.Sleep(3333);
                //3. reload page
                if (ret)
                {
                    _helper.WriteLine("Test_GDPR_Captcha: gdpr returned ok, starting reloading page..");
                    document = await spider.WebAccess.Navigate(tableUri);
                    responseType = advTable.FindDocumentType_Table(document);
                    Assert.True(responseType != EResponseType.AcceptRules);
                    _helper.WriteLine("Test_GDPR_Captcha: gdpr installed, test ok");
                    //Assert.True(spider.WebAccess.PrevCookies.First(c => c.Name == "gdpr") != null);
                   // gdprWasFixed = true;
                   actualAdUri = advTable.ParseAds(document).First();
                   _helper.WriteLine($"Test_GDPR_Captcha: found uri {actualAdUri}");
                }
                else
                {
                    _helper.WriteLine("Test_GDPR_Captcha: gdpr returned false");
                    Assert.True(false);
                }
            }
            Thread.Sleep(3333);
            Assert.NotNull(actualAdUri);
            document = await spider.WebAccess.Navigate(actualAdUri.ToString());
            responseType = advTable.FindDocumentType_Table(document);
            Assert.True(responseType == EResponseType.GoodResponse);
            _helper.WriteLine($"Test_GDPR_Captcha: actual ad uri parsing returned {responseType}");
            
            #region captcha test

            
            //Need to check in prod if I even have to use captcha
            // if ((responseType == EResponseType.GoodResponse) || (responseType == EResponseType.EnterCaptcha))
            // {
            //     spiderLog.LogMessage($"solve captcha part");
            //     if (responseType == EResponseType.GoodResponse)
            //         for (int i = 0; i < 50; i++)
            //         {
            //             spiderLog.LogMessage($"get document for captcha, attempt #{i}");
            //             document = await spider.Navigate(InitialSettings.GetBaseUrl(0));
            //             responseType = advTable.FindDocumentType_Table(document);
            //             spiderLog.LogMessage($"doc is here, it has type ${responseType}");
            //             if (responseType == EResponseType.EnterCaptcha)
            //                 break;
            //             if (gdprWasFixed) Assert.False(responseType == EResponseType.AcceptRules);
            //         }
            //
            //     Assert.True(responseType == EResponseType.EnterCaptcha);
            //     _helper.WriteLine("got captcha, attempting to fix it");
            //     var workaroundC = new CaptchaWorkaround(spiderLog,
            //         new CaptchaSolver(new Settings(), spiderLog),
            //         new GetCaptchaUrlFromAutoRuUrl(spiderLog));
            //     var ret = await workaroundC.Start(document, spider);
            //     
            //     document = await spider.Navigate(ret);
            //     responseType = advTable.FindDocumentType_Table(document);
            //     Assert.True(responseType!=EResponseType.EnterCaptcha);
            //     
            //     bool captchaWasSolved = !String.IsNullOrEmpty(ret);
            //     _helper.WriteLine($"captcha was solved? {captchaWasSolved}");
            //     Assert.True(captchaWasSolved);
            // }
            //
            // document = await spider.Navigate(InitialSettings.GetBaseUrl(0));
            // responseType = advTable.FindDocumentType_Table(document);
            // _helper.WriteLine($"after captcha doc type returned as ${responseType}");
            // Assert.True(responseType == EResponseType.GoodResponse || responseType == EResponseType.RedirectToMobile);
            //

            #endregion
        }
    }
}