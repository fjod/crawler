﻿using System.Net.Http;
using Application.Interfaces;
using Domain;
using InfraStructure;
using Xunit.Abstractions;

namespace Tests
{
    public class BaseTest
    {
        protected readonly IInjection Injection;
        protected static HttpClient Client;
        readonly object _clientLock = new object();
        protected BaseTest()
        {
            Injection = new Injector();
            lock (_clientLock)
            {
                if (Client == null) Client = new HttpClient();    
            }
            
        }
    }
    
    public  class TestSpiderLog : ISpiderLog
    {
        private readonly ITestOutputHelper _helper;

        public TestSpiderLog(ITestOutputHelper helper)
        {
            _helper = helper;
        }
        public void LogAd(ScrapedAd ad)
        {
                
        }

        public void LogMessage(string msg)
        {
            _helper.WriteLine(msg);
        }
    }
}