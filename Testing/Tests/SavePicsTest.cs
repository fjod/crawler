﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataBase;
using Domain;
using Domain.ImageComparison;
using InfraStructure;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class SavePicsTest : BaseTest
    {
        private readonly ITestOutputHelper _helper;
        
        [Fact]
        public void Test()
        {
            _helper.WriteLine("started save pics test");
            TestSpiderLog spiderLog = new TestSpiderLog(_helper);
            
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;

            using var context = new DataContext(options);
            
            //some pics will be already in db, some will be new; so code must not upload same images
            List<ImgHash> imagesFromResources = new List<ImgHash>();
            imagesFromResources.Add(new ImgHash(Resource.sampleImg1));
            imagesFromResources.Add(new ImgHash(Resource.sampleImg2));
            imagesFromResources.Add(new ImgHash(Resource.sampleImg3));
            imagesFromResources.Add(new ImgHash(Resource.sampleImg4));
            imagesFromResources.Add(new ImgHash(Resource.sampleImg5));
            imagesFromResources.Add(new ImgHash(Resource.sampleImg6));
            
            _helper.WriteLine("Checking hashes to be unique..");
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (i!=j) Assert.True(imagesFromResources[i].CompareWith(imagesFromResources[j])>0);
                }
            }

            var oneImage = TempFileName.Get();
            try
            {
                Resource.sampleImg1.Save(oneImage);//
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                oneImage = Path.GetTempFileName();
                Resource.sampleImg1.Save(oneImage);
            }
            
            List<AdPic> pics = new List<AdPic>();
            foreach (var imagesFromResource in imagesFromResources)
            {
                pics.Add(new AdPic{ImgHash = imagesFromResource.ToString(), FileName = oneImage});
            }

            context.AdPics.Add(pics[0]);
            context.AdPics.Add(pics[2]);
            context.AdPics.Add(pics[5]);
            context.SaveChanges();
            Assert.True(context.AdPics.Count() == 3);
            
            var scrapedAd = new ScrapedAd();
            
            SaveToDb sut = new SaveToDb(context);
            var first = pics.First();
            pics.Remove(first);
            sut.Save(scrapedAd,pics, spiderLog); //12345
            context.SaveChanges();
            
            //now id db must be one ad, 6 images and 6 links
            var savedAd = context.ScrapedAds.First();
            _helper.WriteLine("Sut is ready for test..");
            Assert.NotNull(savedAd);
            
            Assert.True(context.AdPics.Count()==6);
            Assert.True(context.AdImageLink.Count()==5);
            
            _helper.WriteLine("Check that image saved well");
            foreach (var adsAndImagese in context.AdImageLink)
            {
                var pic = context.AdPics.First(p=> p ==adsAndImagese.Image);
                Assert.Contains(pic, pics);
                Assert.False(pic == first);
            }
            _helper.WriteLine("save pic test done");
            
            File.Delete(oneImage);
        }
                     
        public SavePicsTest(ITestOutputHelper helper)
        {
            _helper = helper;
            
        }
    }
}