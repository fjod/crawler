
using Application.DTO;
using AutoMapper;
using DataBase;
using Domain;
using Xunit.Abstractions;

using System.Linq;


namespace Tests.API
{
    public class ApiBase
    {
        protected string tokenValue = "CoolJwToken";
        protected ITestOutputHelper Helper { get; }
        protected DataContext Context = TestDbContext.Create();
        protected IMapper mapper;

        protected ApiBase(ITestOutputHelper helper)
        {
            Helper = helper;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<VkUser, DTO_VkUser>();
                cfg.CreateMap<ScrapedAd, DTO_ScrapedAdPreview>()
                    .ForMember(dto => dto.Rating, 
                        source => 
                            source.MapFrom(ad => ad.Ratings.Average(r=> r.Rating)))
                    .ForMember(dto => dto.CommentsCount, 
                        source=> 
                            source.MapFrom(ad => ad.Comments.Count))
                    .ForMember(dto=> dto.UniqueId, 
                        source => 
                            source.MapFrom(ad => ad.UniqueId ?? 0)); 
            });
            
            mapper = config.CreateMapper();
        }

    }
}