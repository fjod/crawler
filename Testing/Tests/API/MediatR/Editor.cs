using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Ads.Editor;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class Editor : ApiBase
    {
        public Editor(ITestOutputHelper helper) : base(helper)
        {
        }

        [Theory]
        [InlineData(0,2)]
        [InlineData(2,2)]
        async Task TestList(int skip, int take)
        {
            var querySut = new List.Query();
            querySut.Skip = skip;
            querySut.Take = take;
            
            var handlerSut = new List.Handler(Context);
            var sut = await handlerSut.Handle(querySut, CancellationToken.None);
            
            Assert.Equal(take,sut.Count);
            Assert.True(sut[0].Current.ScrapedAt > sut[0].Unique.ScrapedAt);
            Assert.True(sut[1].Current.ScrapedAt > sut[1].Unique.ScrapedAt);

            if (skip > 0)
            {
                var skipAndTake = await Context.ScrapedAds.OrderByDescending(ad => ad.ScrapedAt).Skip(skip).Take(take)
                    .FirstOrDefaultAsync();
                Assert.NotSame(sut[0].Current, skipAndTake);
            }
        }

        [Fact]
        async Task TestDetails()
        {
            var querySut = new UniqueDetails.Query();
            var firstAd = Context.ScrapedAds.FirstOrDefault(ad => ad.ID == ad.UniqueId);
            Assert.NotNull(firstAd);
            querySut.ID = firstAd.ID;
            
            var handlerSut = new UniqueDetails.Handler(Context);
            var sut = await handlerSut.Handle(querySut, CancellationToken.None);

            var allPicsForThisAd = from pics in Context.AdImageLink
                join ad in Context.ScrapedAds.Where(_ad => _ad.UniqueId == firstAd.UniqueId)
                    on pics.Ad equals ad
                join pic in Context.AdPics on pics.Image equals pic
                select pic;
            
            Assert.Same(firstAd,sut.Ad); //got ad
            //all pics are different and belong to this ad
            List<string> hashes = new List<string>();
            sut.SmallPics.ForEach(adPic =>
            {
                if (!hashes.Contains(adPic.ImgHash))
                    hashes.Add(adPic.ImgHash);

                Assert.True(allPicsForThisAd.Contains(adPic));

            });
            Assert.Equal(hashes.Count,sut.SmallPics.Count);
            
        }
    }
}