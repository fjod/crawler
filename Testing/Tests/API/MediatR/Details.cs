using System.Threading;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class Details : ApiBase

    {
        public Details(ITestOutputHelper helper) : base(helper)
        {
        }
        
        [Fact]
        public async void Test()
        {
            var querySut = new Application.MediatR.Ads.Details.Query();
            var expectedAd = await Context.ScrapedAds.FirstOrDefaultAsync();
            querySut.Id = expectedAd.ID;
            
            var handlerSut = new Application.MediatR.Ads.Details.Handler(Context);
            var response = await handlerSut.Handle(querySut, CancellationToken.None);
            
            Assert.Equal(expectedAd,response);
        }
    }
}