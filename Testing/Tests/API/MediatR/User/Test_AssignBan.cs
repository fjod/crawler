﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.User;
using Domain;
using MediatR;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.User
{
    public class Test_AssignBan : ApiBase
    {
        public Test_AssignBan(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            var testRoleId = "12345";
            var user = new AppUser {Uid = testRoleId};
            Context.Users.Add(user);
            Context.SaveChanges();

            var nullOffset = Context.Users.First(c => c.Uid == testRoleId).LockoutEnd;
            Assert.Null(nullOffset);
            
            //set ban
            AssignBan.Request sr = new AssignBan.Request {Uid = testRoleId, Days =  7};
            IRequestHandler<AssignBan.Request> sut = new AssignBan.Handler(Context);
            await sut.Handle(sr, CancellationToken.None);
            
            var userNotNullOffset = Context.Users.First(c => c.Uid == testRoleId);
            Assert.NotNull(userNotNullOffset.LockoutEnd);
            var banTimeSpan = userNotNullOffset.LockoutEnd.Value.Date - DateTime.Now;
            Assert.True(banTimeSpan.Days >= 6);
            Assert.True(userNotNullOffset.LockoutEnabled);
            
            //clear ban
            sr = new AssignBan.Request {Uid = testRoleId, Days =  0};
            await sut.Handle(sr, CancellationToken.None);
            var userWithNullOffset = Context.Users.First(c => c.Uid == testRoleId);
            Assert.Null(userWithNullOffset.LockoutEnd);
            Assert.False(userWithNullOffset.LockoutEnabled);
        }
    }
}