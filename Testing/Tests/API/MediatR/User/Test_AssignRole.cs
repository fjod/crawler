﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.User;
using Application.User;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.User
{
    public class Test_AssignRole : ApiBase
    {
        public Test_AssignRole(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            var testRoleId = "12345";
            var user = new AppUser {Uid = testRoleId};
            Context.Users.Add(user);
            Context.SaveChanges();
            AssignRole.Request sr = new AssignRole.Request {Uid = testRoleId, Role = 0};

            var mockManager = new Mock<UserManager<AppUser>>(new Mock<IUserStore<AppUser>>().Object, null, null,
                null, null, null, null, null, null);

            mockManager.Setup(m => m.GetClaimsAsync(It.IsAny<AppUser>()))
                .Returns<AppUser>(async u => await Task.Run(() => new List<Claim>()));

            Claim claim = null;
            mockManager.Setup(m => m.AddClaimAsync(It.IsAny<AppUser>(), It.IsAny<Claim>()))
                .Returns<AppUser, Claim>(async (appUser, claim1) =>
                {
                    claim = claim1;
                    return await Task.Run(() => new Mock<IdentityResult>().Object);
                });


            IRequestHandler<AssignRole.Request> sut = new AssignRole.Handler(Context, mockManager.Object);
            await sut.Handle(sr, CancellationToken.None);

            Assert.NotNull(claim);
            Assert.Equal(ClaimTypes.Role,claim.Type);
            Assert.Equal(UserType.SuperAdmin.ToString(),claim.Value);

            List<Claim> admin = new List<Claim> {new Claim(ClaimTypes.Role, UserType.SuperAdmin.ToString())};
            mockManager.Setup(m => m.GetClaimsAsync(It.IsAny<AppUser>()))
                .Returns<AppUser>(async u => await Task.Run(() => admin));
            
            sr = new AssignRole.Request {Uid = testRoleId, Role = 2}; //change role for that user
            sut = new AssignRole.Handler(Context, mockManager.Object);
            await sut.Handle(sr, CancellationToken.None);

            Assert.NotNull(claim);
            Assert.Equal(ClaimTypes.Role,claim.Type);
            Assert.Equal(UserType.User.ToString(),claim.Value);
        }
    }
}