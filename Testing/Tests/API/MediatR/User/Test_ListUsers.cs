﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.User;
using Application.User;
using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Identity;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.User
{
    public class TestListUsers : ApiBase
    {
        public TestListUsers(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            
            for (int i = 0; i < 10; i++)
            {
                var stringI = i.ToString();
                var appUser = new AppUser
                {
                    Ava1 = stringI,
                    Uid = stringI
                };
                Context.Users.Add(appUser);
            }
            
            //save changes to have users
            Context.SaveChanges();

            var usersWithIDs = Context.Users.ToList();
            int counter = 0;
            foreach (var userWithId in usersWithIDs)
            {
                counter++;
                var vkUser = new VkUser {Ava1 = userWithId.Ava1, Name = userWithId.Ava1, Surname = userWithId.Ava1, Uid = userWithId.Uid};
                Context.VkUsers.Add(vkUser);
                
                Random st = new Random();
                var rand = st.Next(0, 9);
                var userType = rand switch
                {
                    0 => UserType.Admin.ToString(),
                    1 => UserType.SuperAdmin.ToString(),
                    _ => UserType.User.ToString()
                };

                //assign claim to user
                Context.UserClaims.Add(new IdentityUserClaim<string>
                    {ClaimType = ClaimTypes.Role, ClaimValue = userType, UserId = userWithId.Id, Id = counter});    
            }
            
            //users with claims
            Context.SaveChanges();

            
            var config = new MapperConfiguration(cfg => cfg.CreateMap<string,UserType>());
            mapper = config.CreateMapper();

            var query = new ListUsers.Query();
            var sut = new ListUsers.Handler(Context);
            var result = await sut.Handle(query, CancellationToken.None);
            
            Assert.NotEmpty(result);
            
            foreach (var dtoUserWithCreds in result)
            {
                Assert.IsType<UserType>(dtoUserWithCreds.Type);
                var vk = Context.VkUsers.First(v => v.Uid == dtoUserWithCreds.Uid);
                Assert.NotNull(vk);
            }
        }
    }
}