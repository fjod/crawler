using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.MediatR.User;
using Application.Misc;
using Application.User;
using Domain;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class RegisterUser : ApiBase
    {
        public RegisterUser(ITestOutputHelper helper) : base(helper)
        {
        }

        
        string tempId = "TestId";
        
        [Fact]
        public async Task NewUser()
        {
            var sutQuery = new Register.Command {Id = tempId};

            //must have vk user in db with this tempiId
            VkUser user = new VkUser {Uid = "12345", Guid = tempId, Ava1 = "ava1"};
            Context.VkUsers.Add(user);
            Context.SaveChanges();

            var jwtMock = new Mock<IJWTGenerator>();
            var resultToken = "";
            jwtMock.Setup(m => m.CreateToken(user, UserType.User)).
                Returns<VkUser,UserType>((u,s) =>
            {
                resultToken = tokenValue;
                return tokenValue;
            });


            var userContextMock = new Mock<UserManager<AppUser>>(new Mock<IUserStore<AppUser>>().Object, null, null,
                null, null, null, null, null, null);

            userContextMock.Setup(m => m.CreateAsync(It.IsAny<AppUser>()))
                .Returns<AppUser>((u) => Task.Run((() => IdentityResult.Success)));

            var userAccessorMock = new Mock<IUserAccessor>();
            userAccessorMock.
                Setup(accessor => accessor.GetCurrentUserType("")).Returns(UserType.User);
            
            //first test if we dont have registered user
            var handler = new Register.Handler(Context, jwtMock.Object, userContextMock.Object,mapper,userAccessorMock.Object);
            var ret = await handler.Handle(sutQuery, CancellationToken.None);
            Assert.NotNull(ret);
            Assert.Equal(resultToken, ret.token);
            Assert.Equal("ava1", ret.user.Ava1);


            var sutQuery3 = new Register.Command {Id = "errorPlz"};
            await Assert.ThrowsAsync<RestException>(() => handler.Handle(sutQuery3, CancellationToken.None));
        }
        
        

        [Fact]
        public async Task ExistingUser()
        {
            //must have vk user in db with this tempId
            //now lets add user beforehand
            VkUser user2 = new VkUser {Uid = "123456", Guid = tempId + "1", Ava1 = "ava2"};
            Context.VkUsers.Add(user2);

            var jwtMock = new Mock<IJWTGenerator>();
            var resultToken = "";
            jwtMock.Setup(m => m.CreateToken(user2, UserType.User)).
                Returns<VkUser,UserType>((user, s) =>
            {
                resultToken = tokenValue;
                return tokenValue;
            });

            var userContextMock = new Mock<UserManager<AppUser>>(new Mock<IUserStore<AppUser>>().Object, null, null,
                null, null, null, null, null, null);

            userContextMock.Setup(m => m.CreateAsync(It.IsAny<AppUser>()))
                .Returns<AppUser>(user => Task.Run((() => IdentityResult.Success)));


            var appUser = new AppUser
            {
                Ava1 = "ava2",
                Uid = "123456"
            };
            Context.Users.Add(appUser);
            Context.SaveChanges();
            
            var userAccessorMock = new Mock<IUserAccessor>();
            userAccessorMock.
                Setup(accessor => accessor.GetCurrentUserType(appUser.Id)).Returns(UserType.User);

            var sutQuery2 = new Register.Command {Id = tempId + "1"};

            var handler2 = new Register.Handler(Context, jwtMock.Object, userContextMock.Object,mapper,userAccessorMock.Object);
            var ret2 = await handler2.Handle(sutQuery2, CancellationToken.None);
            
            Assert.NotNull(ret2);
            Assert.Equal(resultToken, ret2.token);
            Assert.Equal("ava2", ret2.user.Ava1);
        }
    }
}