using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.MediatR.User;
using Application.User;
using Domain;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.User
{
    public class GetCurrentUser: ApiBase
    {
        public GetCurrentUser(ITestOutputHelper helper) : base(helper)
        {
        }
        
        [Fact]
        public async Task Test()
        {
            string sampleUid = "12345";
            VkUser vkUser = new VkUser {Uid = sampleUid, Guid = "TempID", Ava1 = "ava1", Name = "TestName"};
            AppUser appUser = new AppUser
            {
                Id = Guid.NewGuid().ToString(),
                Uid = sampleUid,
                LockoutEnabled = false
            };
            
            Context.VkUsers.Add(vkUser);
            Context.Users.Add(appUser);
            Context.SaveChanges();

            var userAccessMock = new Mock<IUserAccessor>();
            userAccessMock.Setup(m => m.GetCurrentUserName()).Returns(vkUser.Uid);
            
            var jwtMock = new Mock<IJWTGenerator>();
            
            jwtMock.Setup(m => m.CreateToken(It.IsAny<VkUser>(),It.IsAny<UserType>())).
                Returns<VkUser,UserType>((vkUser1, type) => tokenValue);

            var sutQuery = new CurrentUser.Query();
            var sutHandler = new CurrentUser.Handler(jwtMock.Object, userAccessMock.Object, Context,mapper);
            var ret = await sutHandler.Handle(sutQuery, CancellationToken.None);
            
            Assert.Equal(tokenValue,ret.token);
            var clean = vkUser.CleanUp();
            Assert.Equal(clean.Ava1,ret.user.Ava1);

            //user is banned, for a week, request should not change it
            appUser.LockoutEnabled = true;
            appUser.LockoutEnd = DateTimeOffset.Now + TimeSpan.FromDays(7);
            Context.SaveChanges();
            await sutHandler.Handle(sutQuery, CancellationToken.None);

            appUser = Context.Users.FirstOrDefault(u => u.Uid == appUser.Uid);
            Assert.NotNull(appUser); 
            Assert.True(appUser.LockoutEnabled);
            Assert.True(appUser.LockoutEnd?.DateTime > DateTime.Now);
            
            appUser.LockoutEnd = DateTimeOffset.Now - TimeSpan.FromDays(7);
            Context.SaveChanges();
            await sutHandler.Handle(sutQuery, CancellationToken.None);
            
            appUser = Context.Users.FirstOrDefault(u => u.Uid == appUser.Uid);
            Assert.NotNull(appUser); 
            Assert.False(appUser.LockoutEnabled);
          
        }
    }
}