using System;
using System.Linq;
using System.Threading;
using Application.Misc;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class HistoryT : ApiBase
    {
        public HistoryT(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async void TestMediatrHandler()
        {
            var uniqueAd = Context.ScrapedAds.First(ad => ad.UniqueId == ad.ID);
            var allAdsForThis = Context.ScrapedAds.Where(ad => ad.UniqueId == uniqueAd.ID);
            var querySut = new Application.MediatR.Ads.History.Query();
            querySut.UniqueId = uniqueAd.ID;
            
            var queryHandler = new Application.MediatR.Ads.History.Handler(Context);
            var ret = await queryHandler.Handle(querySut, CancellationToken.None);
            
            Assert.Equal(allAdsForThis.Count()-1,ret.Count); //just basic test if handler can find diffs
        }
        
        [Fact]
        public void TestFieldDiffs()
        {
            var uniqueAd = Context.ScrapedAds.First(ad => ad.UniqueId == ad.ID);
            var allAdsForThis = Context.ScrapedAds.Where(ad => ad.UniqueId == uniqueAd.ID).ToList();

            var ad1 = allAdsForThis[0];
            var ad2 = allAdsForThis[1];

            var sut1 = ad1.FindDifferences(ad2);
            Assert.Empty(sut1); //ads are created with same properties under one unique ID

            var secondUniqueAd = Context.ScrapedAds.First(ad => ad.UniqueId != uniqueAd.ID);
            var sut2 = ad1.FindDifferences(secondUniqueAd);
            Assert.NotEmpty(sut2); //they can have same year, so just check if there is some diffs 
        }
        
        [Fact]
        public async System.Threading.Tasks.Task TestThrowsExceptionWithBadUniqueId()
        {
            var uniqueAd = from ad in Context.ScrapedAds where ad.UniqueId == ad.ID select new
            {
                ad.UniqueId
            };

            var gotNonExistantId = false;
            var thatNonExistantId = 0;
            var rand = new Random();
            while (!gotNonExistantId)
            {
                thatNonExistantId = rand.Next(0, 50000);
                gotNonExistantId = !uniqueAd.Any(id => id.UniqueId == thatNonExistantId);
            }
            
            var querySut = new Application.MediatR.Ads.History.Query();
            querySut.UniqueId = thatNonExistantId;
            
            var queryHandler = new Application.MediatR.Ads.History.Handler(Context);
            await Assert.ThrowsAsync<RestException>(() => queryHandler.Handle(querySut, CancellationToken.None));



        }

    }
}