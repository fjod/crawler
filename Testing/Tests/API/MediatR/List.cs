using System.Linq;
using System.Threading;
using Application.Misc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class ListTest : ApiBase
    {
        public ListTest(ITestOutputHelper helper) : base(helper)
        {
        }

        [Theory]
        [InlineData(0,3,SortAds.ByDate)]
        [InlineData(2,3,SortAds.ByDate)]
        [InlineData(0,3,SortAds.ByRating)]
        [InlineData(2,3,SortAds.ByRating)]
        [InlineData(0,3,SortAds.NotRated)]
        [InlineData(2,3,SortAds.NotRated)]
        public async void Test(int skip, int take, SortAds sortAds)
        {
            var querySut = new Application.MediatR.Ads.List.Query {Skip = skip, Take = take, Sort = sortAds};

            var handlerSut = new Application.MediatR.Ads.List.Handler(Context, mapper);
            var sut = await handlerSut.Handle(querySut, CancellationToken.None);
            
            Assert.Equal(take,sut.Count);

            var allAds = Context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId).
                Include(ad => ad.Ratings).ToList();
            if (sortAds == SortAds.ByDate)
            {
                allAds = allAds.OrderByDescending(ad => ad.ScrapedAt).Skip(skip).Take(take).ToList();
                for (int i = 0; i < allAds.Count; i++)
                {
                    Assert.Equal(allAds[i].UniqueId,sut[i].UniqueId);
                    Assert.True(sut[i].CommentsCount > 0);
                }
                Assert.Equal(allAds.Count, sut.Count);
            }

            if (sortAds == SortAds.ByRating)
            {
                allAds = allAds.OrderByDescending(ad => ad.Ratings.Average(r => r.Rating)).Skip(skip).Take(take).ToList();
                for (int i = 0; i < allAds.Count; i++)
                {
                    Assert.Equal(allAds[i].UniqueId,sut[i].UniqueId);
                    Assert.True(sut[i].CommentsCount > 0);
                    if (i < allAds.Count - 1)
                    {
                        var curRating = allAds[i].Ratings.Average(r => r.Rating);
                        var nextRating= allAds[i+1].Ratings.Average(r => r.Rating);
                        Assert.True(curRating >= nextRating);
                    }
                }
                Assert.Equal(allAds.Count, sut.Count);
            }
            if (sortAds == SortAds.NotRated)
            {
                allAds = allAds.OrderBy(ad => ad.Ratings.Average(r => r.Rating)).Skip(skip).Take(take).ToList();
                for (int i = 0; i < allAds.Count; i++)
                {
                    Assert.Equal(allAds[i].UniqueId,sut[i].UniqueId);
                    Assert.True(sut[i].CommentsCount > 0);
                    if (i < allAds.Count - 1)
                    {
                        var curRating = allAds[i].Ratings.Average(r => r.Rating);
                        var nextRating= allAds[i+1].Ratings.Average(r => r.Rating);
                        Assert.True(curRating <= nextRating);
                    }
                }
                Assert.Equal(allAds.Count, sut.Count);
                
            }

        }
    }
}
