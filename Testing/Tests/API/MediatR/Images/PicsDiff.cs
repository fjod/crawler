using System.Linq;
using System.Threading;
using Application.MediatR.Ads.Images;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Images
{
    public class PicsDiff : ApiBase
    {
        public PicsDiff(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async void Test()
        {
            var firstUniqueAd = Context.ScrapedAds.First(ad => ad.ID == ad.UniqueId);
            var secondUniqueAd = Context.ScrapedAds.First(ad => ad.UniqueId != firstUniqueAd.UniqueId);

            var sutQuery = new PicsDifference.Query();
            sutQuery.NextId = secondUniqueAd.ID;
            sutQuery.PrevId = firstUniqueAd.ID;
            
            var picsForFirst = from link in Context.AdImageLink
                where link.Ad.ID == firstUniqueAd.ID
                join adPic in Context.AdPics on link.Image equals adPic
                select adPic;
            
            var picsForSecond = from link in Context.AdImageLink
                where link.Ad.ID == secondUniqueAd.ID
                join adPic in Context.AdPics on link.Image equals adPic
                select adPic;

            var diffPics =picsForFirst.ToList().FindAll(currentPic =>
                picsForSecond.Any(nextPic => nextPic.Equals(currentPic)) == false);
            
            var sutHandler = new PicsDifference.Handler(Context);
            var ret = await sutHandler.Handle(sutQuery, CancellationToken.None);
            
            Assert.NotEmpty(ret);
            
            foreach (var adPic in ret)
            {
                Assert.Contains(adPic, diffPics);
            }
        }
    }
}