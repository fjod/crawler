using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Ads.Images;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Images
{
   
    public class Image : ApiBase
    {
        public Image(ITestOutputHelper helper) : base(helper)
        {
          
        }

       
        [Fact]
        public async void GetBigImage()
        {
            var allSmallImages = Context.AdPics.Where(pic => !pic.IsBig);

            foreach (var smallImage in allSmallImages)
            {
                var sutQuery = new GetBigImage.Query();
                sutQuery.Id = smallImage.ID;
                var sutHandler = new GetBigImage.Handler(Context);
                var sut = await sutHandler.Handle(sutQuery, CancellationToken.None);
                Assert.True(sut.IsBig);
            }
        }
    }
}