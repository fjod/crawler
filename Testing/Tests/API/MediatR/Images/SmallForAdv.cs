using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Application.MediatR.Ads.Images;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Images
{
    public class SmallForAdv : ApiBase
    {
        public SmallForAdv(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async void TestGettingSmallPics()
        {
            var ad = await Context.ScrapedAds.FirstOrDefaultAsync();
            var picsForIt = from link in Context.AdImageLink
                where link.Ad.ID == ad.ID
                join adPic in Context.AdPics on link.Image equals adPic
                select adPic;

            var sut = await PicsForAd.GetSmall(ad.ID, Context, CancellationToken.None);
            foreach (var adPic in sut)
            {
                Assert.True(picsForIt.Contains(adPic));
            }
        }

        [Fact]
        public async void Test()
        {
            var sutQuery = new SmallForAd.Query();
            var ad = await Context.ScrapedAds.FirstOrDefaultAsync();
            if (ad.UniqueId != null) sutQuery.UniqueId = (int) ad.UniqueId;

            var picsForIt = from link in Context.AdImageLink
                where link.Ad.ID == ad.ID
                join adPic in Context.AdPics on link.Image equals adPic
                select adPic;

            var sut = new SmallForAd.Handler(Context);
            var sutHandler = await sut.Handle(sutQuery,CancellationToken.None);
            
            
            foreach (var adPic in sutHandler)
            {
                Assert.True(picsForIt.Contains(adPic));
            }
        }
    }
    
    
}