using System;
using System.Linq;
using System.Threading;
using Application.Interfaces;
using Application.MediatR.Ads;
using Application.Misc;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class Rating : ApiBase
    {
        private string _testUserName = "TestId";

        public Rating(ITestOutputHelper helper) : base(helper)
        {
            _vk = new VkUser {Uid = _testUserName};
            Context.VkUsers.Add(_vk);
            Context.SaveChanges();
        }

        private readonly VkUser _vk;

        [Fact]
        public async void TestSetRating()
        {
            var allUniqueIds = from ad in Context.ScrapedAds where ad.ID == ad.UniqueId select ad.UniqueId;


            Random r = new Random();
            int someOtherUniqueId = r.Next(0, 50000);
            while (allUniqueIds.Contains(someOtherUniqueId))
            {
                someOtherUniqueId = r.Next(0, 50000);
            }

            SetRating.Command sc = new SetRating.Command();
            sc.value = 1;
            sc.advUniqueId = someOtherUniqueId;
           

            var userAccessorMock = new Mock<IUserAccessor>();
            userAccessorMock.Setup(m => m.GetCurrentUserName()).Returns("fakeName");
            IRequestHandler<SetRating.Command> sut = new SetRating.Handler(Context, userAccessorMock.Object);
            await Assert.ThrowsAsync<RestException>(async () => await sut.Handle(sc,CancellationToken.None)); //bad user

            userAccessorMock = new Mock<IUserAccessor>();
            userAccessorMock.Setup(m => m.GetCurrentUserName()).Returns(_testUserName);
            sut = new SetRating.Handler(Context, userAccessorMock.Object);
            await Assert.ThrowsAsync<RestException>(async () => await sut.Handle(sc,CancellationToken.None)); //bad uniqueId

            userAccessorMock = new Mock<IUserAccessor>();
            userAccessorMock.Setup(m => m.GetCurrentUserName()).Returns(_testUserName);
            sut = new SetRating.Handler(Context, userAccessorMock.Object);
            var firstId = allUniqueIds.First();
            Assert.True(firstId.HasValue);
            sc.advUniqueId = (int) firstId;

            var adWithRating = Context.ScrapedAds.Where(ad => ad.ID == sc.advUniqueId).
                Include(ad => ad.Ratings).First();
            var prevCount = adWithRating.Ratings.Count;
            
            await sut.Handle(sc,CancellationToken.None);

            adWithRating = Context.ScrapedAds.Where(ad => ad.ID == sc.advUniqueId).
                Include(ad => ad.Ratings).First();
            var nextCount = adWithRating.Ratings.Count;

         
            Assert.NotEqual(prevCount,nextCount);
        }

        [Fact]
        public async void TestGetRating()
        {
            var allUniqueIds = from ad in Context.ScrapedAds where ad.ID == ad.UniqueId select ad.UniqueId;


            Random r = new Random();
            int someOtherUniqueId = r.Next(0, 50000);
            while (allUniqueIds.Contains(someOtherUniqueId))
            {
                someOtherUniqueId = r.Next(0, 50000);
            }


            var sutQuery = new GetRating.Request();
            sutQuery.AdUniqueId = someOtherUniqueId;

            var sut = new GetRating.Handler(Context);
            var ret = await sut.Handle(sutQuery, CancellationToken.None);
            var prevVotes = ret.TotalVotes;
            
            var setRating = new AdRating();
            setRating.Rating = 1;
            setRating.ScrapedAd = Context.ScrapedAds.First(ad => ad.ID == ad.UniqueId);
            setRating.VkUser = _vk;
            Context.Ratings.Add(setRating);
            Context.SaveChanges();

            if (!setRating.ScrapedAd.UniqueId.HasValue)
                throw new Exception("must have unique id");

            sutQuery.AdUniqueId = (int) setRating.ScrapedAd.UniqueId;
            ret = await sut.Handle(sutQuery, CancellationToken.None);
            Assert.True(ret.TotalVotes > 0);
            Assert.NotEqual(ret.TotalVotes, prevVotes);
        }
    }
}