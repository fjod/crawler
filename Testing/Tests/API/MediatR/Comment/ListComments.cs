using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Comments;
using Domain;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Comment
{
    public class ListComments : ApiBase
    {
        public ListComments(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            var oldComments = await Context.Comments.ToListAsync();
            var oldUsers = await Context.VkUsers.ToListAsync();
            Context.Comments.RemoveRange(oldComments);
            Context.VkUsers.RemoveRange(oldUsers);
            await Context.SaveChangesAsync();
            
            var ad = Context.ScrapedAds.First();
            var vkUser = new VkUser {Ava1 = "sampleAva", Name = "sampleUser"};
            Context.VkUsers.Add(vkUser);

            var p1 = new Domain.Comment {Text = "P1",  VkUser = vkUser};
            var p2 = new Domain.Comment {Text = "P2",  VkUser = vkUser};
            var n1 = new Domain.Comment {Text = "N1",  Parent = p1,VkUser = vkUser};
            var n2 = new Domain.Comment {Text = "N2",  Parent = p1,VkUser = vkUser};
            var n3 = new Domain.Comment {Text = "N3",  Parent = p2,VkUser = vkUser};
            var m1 = new Domain.Comment {Text = "M1",  Parent = n1,VkUser = vkUser};
            var m2 = new Domain.Comment {Text = "M2",  Parent = n1,VkUser = vkUser};
            var m3 = new Domain.Comment {Text = "M3",  Parent = n2,VkUser = vkUser};

            //             P1 --------- P2  top level with no parents
            //          N1    N2         N3  level 1
            //        M1  M2    M3            level 2
            ad.Comments.Clear();
            await Context.SaveChangesAsync();
            
            ad.Comments.Add(p1);
            ad.Comments.Add(p2);
            ad.Comments.Add(n1);
            ad.Comments.Add(n2);
            ad.Comments.Add(n3);
            ad.Comments.Add(m1);
            ad.Comments.Add(m2);
            ad.Comments.Add(m3);
            //await Context.Comments.AddRangeAsync(p1, p2, n1, n2, n3, m1, m2, m3);
            await Context.SaveChangesAsync();

            var sutQuery = new List.Query {Id = ad.ID};
            var sutHandler = new List.Handler(Context);
            var result = await sutHandler.Handle(sutQuery, CancellationToken.None);

            //on the first level we should have two comments - P1 & P2
            Assert.Equal(2, result.Count);

            //P1 should also have two - N1 & N2
            var retP1 = result.First(c => c.Comment.Text == "P1");
            Assert.Equal(2, retP1.NestedComments.Count());
            var retN1 = retP1.NestedComments.First(c => c.Comment.Text == "N1");
            var retN2 = retP1.NestedComments.First(c => c.Comment.Text == "N2");
            Assert.Equal(2, retN1.NestedComments.Count());
            Assert.Single(retN2.NestedComments);
            Assert.Empty(retN1.NestedComments.First(c => c.Comment.Text == "M1").NestedComments);
            Assert.Empty(retN1.NestedComments.First(c => c.Comment.Text == "M2").NestedComments);
            Assert.Empty(retN2.NestedComments.First(c => c.Comment.Text == "M3").NestedComments);

            var retP2 = result.First(c => c.Comment.Text == "P2");
            var retN3 = retP2.NestedComments.First(c => c.Comment.Text == "N3");
            Assert.Single(retP2.NestedComments);
            Assert.Empty(retN3.NestedComments);
        }
    }
}