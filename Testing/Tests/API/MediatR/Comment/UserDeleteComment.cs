using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Comments;
using Application.Misc;
using MediatR;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Comment
{
    public class UserDeleteComment : ApiBase
    {
        public UserDeleteComment(ITestOutputHelper helper) : base(helper)
        {
        }
        
        [Fact]
        public async Task Test()
        {
            var sutQuery = new UserDelete.Request();
            IRequestHandler<UserDelete.Request> sutHandler = new UserDelete.Handler(Context);
            await Assert.ThrowsAsync<RestException>(() => sutHandler.Handle(sutQuery,CancellationToken.None));
            
            var commentToDelete = new Domain.Comment();
            commentToDelete.Text = "sample";
            Context.Comments.Add(commentToDelete);
            Context.SaveChanges();
            
            sutQuery.Id = commentToDelete.ID;
            await sutHandler.Handle(sutQuery,CancellationToken.None);

            var deletedComment = Context.Comments.First(c => c.ID == commentToDelete.ID);
            Assert.True(deletedComment.Text == "");
            Assert.Null(deletedComment.VkUser);
        }
    }
}