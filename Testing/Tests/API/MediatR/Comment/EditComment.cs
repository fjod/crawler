using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Comments;
using Application.Misc;
using MediatR;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Comment
{
    public class EditComment : ApiBase
    {
        public EditComment(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            var sutQuery = new Edit.Request {Id = 0, Text = ""};
            IRequestHandler<Edit.Request> sutHandler = new Edit.Handler(Context);
            await Assert.ThrowsAsync<RestException>(() => sutHandler.Handle(sutQuery,CancellationToken.None));
            
            var newComment = new Domain.Comment();
            newComment.Text = "startText";
            Context.Comments.Add(newComment);
            Context.SaveChanges();

            var editedText = "editedText";
            var commentToEdit = Context.Comments.First();
            sutQuery.Id = commentToEdit.ID;
            sutQuery.Text = editedText;
            await sutHandler.Handle(sutQuery,CancellationToken.None);

            var editedComment = Context.Comments.First();
            Assert.Equal(editedText,editedComment.Text);

        }
    }
}