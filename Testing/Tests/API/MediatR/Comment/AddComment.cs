using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.MediatR.Comments;
using Application.Misc;
using Application.User;
using Domain;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Comment
{
    public class AddComment : ApiBase
    {
        public AddComment(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task Test()
        {
            var userAccessMock = new Mock<IUserAccessor>();
            var jwtMock = new Mock<IJWTGenerator>();
            var sutQuery = new Add.Command();
            var sutHandler = new Add.Handler(Context,userAccessMock.Object);
            await Assert.ThrowsAsync<RestException>(() =>sutHandler.Handle(sutQuery,CancellationToken.None)); //no user name in accessor 
            
            VkUser user = new VkUser {Uid = "12345", Guid = "TempID", Ava1 = "ava1", Name = "TestName"};
            Context.VkUsers.Add(user);
            var appUser = AppUser.CreateSampleUser(user, Guid.NewGuid());
            Context.Add(appUser);
            Context.SaveChanges();

            userAccessMock.Setup(m => m.GetCurrentUserName()).Returns(user.Uid);
            jwtMock.Setup(m => m.CreateToken(user, UserType.User)).Returns(tokenValue);

            var testText = "testText"; 
            sutQuery.Text = testText;
            var firstAd = Context.ScrapedAds.First(ad => ad.ID == ad.UniqueId);
            sutQuery.AdId = firstAd.ID;
            sutQuery.ParentId = null;
            var ret = await sutHandler.Handle(sutQuery,CancellationToken.None);
            Assert.NotNull(ret);


            var firstComment = firstAd.Comments.First(c => c.Text == testText);
            Assert.Equal(testText,firstComment.Text);
            Assert.Equal(firstAd,firstComment.ScrapedAd);
            Assert.Equal(user,firstComment.VkUser);

            testText += "1";
            sutQuery.Text = testText;
            sutQuery.AdId = firstAd.ID;
            sutQuery.ParentId = firstComment.ID;
            ret = await sutHandler.Handle(sutQuery,CancellationToken.None);
            Assert.NotNull(ret);
            Helper.WriteLine(ret.ID.ToString());

            var secondComment = firstAd.Comments.First(c => c.Text == testText);
            Assert.NotNull(secondComment);
            Assert.Equal(testText,secondComment.Text);
            Assert.Equal(firstAd,secondComment.ScrapedAd);
            Assert.Equal(user,secondComment.VkUser);

        }
    }
}