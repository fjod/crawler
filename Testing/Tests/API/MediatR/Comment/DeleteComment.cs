using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.MediatR.Comments;
using Application.Misc;
using MediatR;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR.Comment
{
    public class DeleteComment : ApiBase
    {
        public DeleteComment(ITestOutputHelper helper) : base(helper)
        {
        }
        
        [Fact]
        public async Task Test()
        {
            var sutQuery = new Delete.Request();
            IRequestHandler<Delete.Request> sutHandler = new Delete.Handler(Context);
            
            
            await Assert.ThrowsAsync<RestException>(() => sutHandler.Handle(sutQuery,CancellationToken.None));
            
            var commentToDelete = new Domain.Comment();
            commentToDelete.Text = "sample";
           
            Context.Comments.Add(commentToDelete);
            Context.SaveChanges();
            var totalCommentsPrev = Context.Comments.Count();
            
            commentToDelete = Context.Comments.First();
            Assert.NotNull(commentToDelete);

            sutQuery.Id = commentToDelete.ID;
            await sutHandler.Handle(sutQuery,CancellationToken.None);
            var totalCommentsAfter = Context.Comments.Count();
            Assert.NotEqual(totalCommentsPrev,totalCommentsAfter);
        }
    }
}