﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.MediatR
{
    public class TotalPriceChange: ApiBase
    {
        public TotalPriceChange(ITestOutputHelper helper) : base(helper)
        {
        }

        [Fact]
        public async Task TestPrices()
        {
            var query = new Application.MediatR.Ads.GetTotalPriceChange.Request();
            var response = new Application.MediatR.Ads.GetTotalPriceChange.Handler(Context);
            var result = await response.Handle(query, CancellationToken.None);
            
            Assert.NotEmpty(result.Prices);
            DateTime? start = null;
            foreach (var resultPrice in result.Prices)
            {
                if (start != null)
                {
                    Assert.True(resultPrice.PriceDate > start);
                }
                
                start = resultPrice.PriceDate;
            }

            var someUniqueAd = Context.ScrapedAds.First(ad => ad.ID == ad.UniqueId);
            if (someUniqueAd.UniqueId != null)
            {
                var query2 = new Application.MediatR.Ads.PriceChange.Request{ UniqueId = someUniqueAd.UniqueId.Value};
                var response2 = new Application.MediatR.Ads.PriceChange.Handler(Context);
                var result2 = await response2.Handle(query2, CancellationToken.None);
            
                Assert.NotEmpty(result2.Prices);
                start = null;
                foreach (var resultPrice in result2.Prices)
                {
                    if (start != null)
                    {
                        Assert.True(resultPrice.PriceDate > start);
                    }
                
                    start = resultPrice.PriceDate;
                }
            }
        }
    }
}