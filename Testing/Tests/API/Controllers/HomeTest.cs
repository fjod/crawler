using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using API.Controllers;
using Application.MediatR.User;
using Application.User;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests.API.Controllers
{
    public class TestHome : ApiBase
    {
        readonly Mock<IMediator> _mediatrMock = new Mock<IMediator>();
        readonly Mock<IServiceProvider> _servicesMock = new Mock<IServiceProvider>();

        private class TestPrincipal : ClaimsPrincipal
        {
            public TestPrincipal(params Claim[] claims) : base(new TestIdentity(claims))
            {
            }
        }

        class TestIdentity : ClaimsIdentity
        {
            public TestIdentity(params Claim[] claims) : base(claims)
            {
            }
        }

        [Fact]
        public async void VkRegister()
        {
            var ad = new Home(_mediatrMock.Object);
            var claims = new TestPrincipal(
                new Claim(LoggedInUser.uid, "TestId"),
                new Claim(LoggedInUser.givenname, "TestGivenName"),
                new Claim(LoggedInUser.surname, "TestSurName"),
                new Claim(LoggedInUser.ava1, "TestAva1"),
                new Claim(LoggedInUser.ava2, "TestAva2")
            );


            var mockContextBase = new Mock<HttpContext>();
            mockContextBase.Setup(m => m.User).Returns(claims);

            string cookiesKey = "";
            string cookiesValue = "";
            var cookiesMock = new Mock<IResponseCookies>();
            cookiesMock.Setup(m => m.Append(It.IsAny<string>(), It.IsAny<string>()))
                .Callback<string, string>((s1, s2) =>
                {
                    cookiesKey = s1;
                    cookiesValue = s2;
                });


            var responseMock = new Mock<HttpResponse>();

            responseMock.Setup(m => m.Cookies).Returns(cookiesMock.Object);
            mockContextBase.Setup(m => m.Response).Returns(() => responseMock.Object);


            IRequestHandler<SaveVkUser.Command> sut = new SaveVkUser.Handler(Context);

            SaveVkUser.Command command = new SaveVkUser.Command();
            _mediatrMock.Setup(m =>
                m.Send(It.IsAny<SaveVkUser.Command>(),
                    It.IsAny<CancellationToken>())).Callback<IRequest<Unit>, CancellationToken>(
                async (request, cancellationToken) =>
                {
                    command = request as SaveVkUser.Command;
                    await sut.Handle(request as SaveVkUser.Command, cancellationToken);
                });


            ad.ControllerContext = new ControllerContext();
            ad.ControllerContext.HttpContext = mockContextBase.Object;

            await ad.CompleteRegistration(); //we have new user in db with TempId

            var newUser = Context.VkUsers.First(u => u.Uid == "TestId");
            Assert.NotNull(newUser);
            Assert.Equal("fb_id", cookiesKey);
            Assert.Equal(command.TempId.ToString(), cookiesValue);
        }

        public TestHome(ITestOutputHelper helper) : base(helper)
        {
            _servicesMock.Setup(provider => provider.GetService(typeof(IMediator))).Returns(() =>
                _mediatrMock.Object);
        }
    }
}