using System;
using System.Collections.Generic;
using System.Threading;
using DataBase;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Tests.API
{
    public class TestDbContext
    {
        private static int _dbCounter;

        static int _uniqueAmount = 5;
        private static int _siblingAnalog = 5;

        static List<int> ids = new List<int>();

        static int GetId()
        {
            Random r = new Random();
            do
            {
                var rand = r.Next(0, 50000);
                if (!ids.Contains(rand))
                {
                    ids.Add(rand);
                    return rand;
                }
            } while (true);
        }

        static Mutex m = new Mutex();

        public static DataContext Create()
        {
            m.WaitOne();
            Interlocked.Add(ref _dbCounter, 1);

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("AsyncBase"+_dbCounter)
                .Options;

            var context = new DataContext(options);
            Random random = new Random();
            //need to simulate db structure from crawler here
            for (int i = 0; i < _uniqueAmount; i++)
            {
                var scrapedDate = DateTime.Now;
                scrapedDate -= TimeSpan.FromDays(i + 10);
                var ad = ScrapedAd.CreateMockAd(scrapedDate);
                ad.ID = GetId();
                ad.UniqueId = ad.ID;
                
                VkUser vk = VkUser.CreateSampleUser(random);
                context.ScrapedAds.Add(ad);
                context.SaveChanges();
                
                AddPicsForAd(ad, context);
                AddRatingsForAd(ad,random,vk);
                AddSampleComments(ad, vk, random);
                context.VkUsers.Add(vk);
             
                context.SaveChanges();
               

                for (int j = 0; j < _siblingAnalog; j++)
                {
                    scrapedDate = DateTime.Now - TimeSpan.FromHours(j);

                    var siblingAd = ad.Copy();
                    siblingAd.UniqueId = ad.UniqueId;
                    siblingAd.ID = GetId();
                    siblingAd.ScrapedAt = scrapedDate;
                    context.ScrapedAds.Add(siblingAd);
                    AddPicsForAd(siblingAd, context);
                    
                   
                }

                context.SaveChanges();
            }

            m.ReleaseMutex();
            return context;
        }

        static void AddSampleComments(ScrapedAd ad, VkUser user, Random random)
        {
            var amount = random.Next(5, 10);
            for (int i = 0; i< amount; i++)
            {
                ad.Comments.Add(Comment.GetSampleComment(ad,user));
            }
        }
        private static void AddPicsForAd(ScrapedAd ad, DataContext context)
        {
            //now some pictures for this ad
            for (int p = 0; p < 3; p++)
            {
                var bigPic = AdPic.CreateMock(true);
                var smallPic = AdPic.CreateMock(false);
                bigPic.ID = GetId();
                smallPic.ID = GetId();

                context.AdPicsLinks.Add(new AdPicsLinks
                {
                    BigImage = bigPic,
                    SmallImage = smallPic,
                    ID = GetId()
                });
                ad.AdPics = new List<AdPic>();
                ad.AdPics.Add(bigPic);
                ad.AdPics.Add(smallPic);
                context.AdPics.Add(bigPic);
                context.AdPics.Add(smallPic);
                context.AdImageLink.Add(new AdsAndImages {Ad = ad, Image = bigPic, ID = GetId()});
                context.AdImageLink.Add(new AdsAndImages {Ad = ad, Image = smallPic, ID = GetId()});
            }
        }

        private static void AddRatingsForAd(ScrapedAd ad,Random random, VkUser user)
        {
            for (int i = 0; i < 10; i++)
            {
                var sample = AdRating.GetSampleRating(user, ad, random);
                ad.Ratings.Add(sample);
            }
        }
    }
}