﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AngleSharp.Html.Parser;
using Application.Interfaces;
using BrowseSharp.Common;
using Crawler.Parsers;
using DataBase;
using Domain;
using InfraStructure;
using Microsoft.EntityFrameworkCore;
using Moq;
using RestSharp;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Parsing
{
    public abstract class ContainsAd
    {
        public ScrapedAd Ad;
        public List<AdPic> Images;
        private readonly DataContext _context;
        protected readonly Mock<IInjection> InjectorMock;

        protected ContainsAd(string dbName,ITestOutputHelper helper)
        {
            var helper1 = helper;
            
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            _context = new DataContext(options);

            var downloader = new TestDownloader();
            
                

            InjectorMock = new Mock<IInjection>();
            InjectorMock.Setup(foo => foo.GetType<ICompareString>()).Returns(new CrawlerStringComparer());
            InjectorMock.Setup(foo => foo.GetType<DataContext>()).Returns(_context);
            InjectorMock.Setup(foo => foo.GetType<ISpiderLog>()).Returns(new TestSpiderLog(helper1));
            InjectorMock.Setup(foo => foo.GetType<ISaveResult>()).Returns(new AdTestSaver(this));
            InjectorMock.Setup(foo => foo.GetType<IWebAccess>()).Returns(downloader);
            

        }

        ~ContainsAd()
        {
            _context.Dispose();
        }
    }

    class AdTestSaver : ISaveResult
    {
        private readonly ContainsAd _test;

        public AdTestSaver(ContainsAd test)
        {
            _test = test;
        }
        public void Save(ScrapedAd ad, List<AdPic> images, ISpiderLog log)
        {
            _test.Ad = ad;
            _test.Images = images;
        }
    }

    class TestDownloader : IWebAccess
    {
#pragma warning disable 1998
        public async Task<AdPic> GetAdPic(string uri, bool isBigPic)
#pragma warning restore 1998
        {
             return  new AdPic();
        }

        public void Dispose()
        {
            
        }

        public void RemoveAllCookiesExceptGdprRelated()
        {
            
        }

        public Task<IDocument> Navigate(string url)
        {
            return null;
        }

        public IList<RestResponseCookie> PrevCookies { get; }
    }

    public class AdParseTest : ContainsAd
    {
        public AdParseTest(ITestOutputHelper helper) : base("t1", helper)
        {
        }

        [Fact]
        public void TestParsing()
        {
            
            AdParser ad = new AdParser(InjectorMock.Object);

            var q = new HtmlParser().ParseDocument(Resource.AdResponse_txt);
            IDocument z = new Document(new RestRequest(), new RestResponse(), q);
            z.Data = Resource.AdResponse_txt;

            ad.Parse(z);

            Assert.False(String.IsNullOrEmpty(this.Ad.BaseUrl.ToString()));
            Assert.False(String.IsNullOrEmpty(this.Ad.City));
            Assert.False(String.IsNullOrEmpty(this.Ad.Color));
            Assert.False(String.IsNullOrEmpty(this.Ad.Description));
            Assert.True(this.Ad.ManufactureYear > 0);
            Assert.True(this.Ad.Mileage > 0);
            Assert.True(this.Ad.Price > 0);
            Assert.False(String.IsNullOrEmpty(this.Ad.SellerName));
            var closeOne = new TimeSpan();
            closeOne = closeOne.Add(TimeSpan.FromDays(365 * 10));
            Assert.True(this.Ad.CreateDate - DateTime.Now < closeOne);
            Assert.True(this.Ad.FreshDate - DateTime.Now < closeOne);

           // Assert.True(this.Images.Count == 10);  look at last test
        }
    }

    public class AdParseNewTest : ContainsAd
    {
        public AdParseNewTest(ITestOutputHelper helper) : base("t2", helper)
        {
        }

        [Fact]
        public void TestParsing_NewType()
        {
            


            AdParser ad = new AdParser(InjectorMock.Object);

            var q = new HtmlParser().ParseDocument(Resource.AdResponse_2_txt);
            IDocument z = new Document(new RestRequest(), new RestResponse(), q);
            z.Data = Resource.AdResponse_2_txt;

            ad.Parse(z);

            Assert.False(String.IsNullOrEmpty(this.Ad.BaseUrl.ToString()));
            Assert.False(String.IsNullOrEmpty(this.Ad.City));
            Assert.False(String.IsNullOrEmpty(this.Ad.Color));
            Assert.False(String.IsNullOrEmpty(this.Ad.Description));
            Assert.True(this.Ad.ManufactureYear > 0);
            Assert.True(this.Ad.Mileage > 0);
            Assert.True(this.Ad.Price > 0);
            Assert.False(String.IsNullOrEmpty(this.Ad.SellerName));
            var closeOne = new TimeSpan();
            closeOne = closeOne.Add(TimeSpan.FromDays(365 * 10));
            Assert.True(this.Ad.CreateDate - DateTime.Now < closeOne);
            Assert.True(this.Ad.FreshDate - DateTime.Now < closeOne);

            //Assert.True(this.Images.Count == 20); //look at last test
        }
    }
    
    public class AdParseBadPic : ContainsAd
        {
            public AdParseBadPic(ITestOutputHelper helper) : base("t3", helper)
            {
            }

            [Fact]
            public void TestParsing_BadPic()
            {
                
                AdParser ad = new AdParser(InjectorMock.Object);
    
                var q = new HtmlParser().ParseDocument(Resource.AdResponse3_badImgPic);
                IDocument z = new Document(new RestRequest(), new RestResponse(), q);
                z.Data = Resource.AdResponse3_badImgPic;
    
                ad.Parse(z);
    
                Assert.False(String.IsNullOrEmpty(this.Ad.BaseUrl.ToString()));
                Assert.False(String.IsNullOrEmpty(this.Ad.City));
                Assert.False(String.IsNullOrEmpty(this.Ad.Color));
                Assert.False(String.IsNullOrEmpty(this.Ad.Description));
                Assert.True(this.Ad.ManufactureYear > 0);
                Assert.True(this.Ad.Mileage > 0);
                Assert.True(this.Ad.Price > 0);
                Assert.False(String.IsNullOrEmpty(this.Ad.SellerName));
                var closeOne = new TimeSpan();
                closeOne = closeOne.Add(TimeSpan.FromDays(365 * 10));
                Assert.True(this.Ad.CreateDate - DateTime.Now < closeOne);
                Assert.True(this.Ad.FreshDate - DateTime.Now < closeOne);
    
                //Assert.True(this.Images.Count == 10);  assert removed because now parser checks for link validity
                //                                       and if link is not valid it does not download it
                //                                       as file during test may be outdated, test can fail
            }
        }
}