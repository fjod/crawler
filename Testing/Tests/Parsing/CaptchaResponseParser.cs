﻿using AngleSharp.Html.Parser;
using BrowseSharp.Common;
using Crawler.Parsers;
using RestSharp;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Parsing
{
    public class CaptchaResponseParser : BaseTest
    {
        private readonly ITestOutputHelper _helper;


        public CaptchaResponseParser(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        [Fact]
        public void CaptchaParser_Test()
        {
            var q = new HtmlParser().ParseDocument(Resource.captcha_response_txt);
            IDocument doc = new Document(new RestRequest(), new RestResponse(), q);
            doc.Data = Resource.captcha_response_txt;
            doc.Response.Headers.Add(new Parameter("Location",
                "https://auto.ru/showcaptcha?retpath=https%3A//auto.ru/motorcycle/yamaha/fz6/all%3F_f09b0e60822ed3b4ab550f1b71858b7e&t=0/1574934623/2541cfc8e0c1ddc923742c8c95d70729&s=69317738d22c18efb009eea2f444608b",
                ParameterType.HttpHeader));


            CaptchaParser sut = new CaptchaParser(new TestSpiderLog(_helper));
            var ret = sut.Parse(doc);
            _helper.WriteLine($"s param = {ret.s}");
            _helper.WriteLine($"t param = {ret.t}");
            _helper.WriteLine($"retpath param = {ret.RetPath}");
            _helper.WriteLine($"url param = {ret.Url}");
            _helper.WriteLine($"key param = {ret.Key}");
            _helper.WriteLine($"req id = {ret.Req_id}");

            Assert.True(ret.RetPath ==
                        "https://auto.ru/motorcycle/yamaha/fz6/all?beaten=1&custom_state_key=CLEARED&image=true&sort_offers=cr_date-DESC&top_days=off&currency=RUR&output_type=list&page_num_offers=0&mark-model-nameplate=YAMAHA%23FZ6_d7452076b53126e246a571b9106bdd69");
            Assert.True(
                ret.Key ==
                "001obECmICyDnJn5xfxbFd2Vkm19EdhJ_0/1574857440/4c4f88189a77f9d2274dffedab3f2ce1_82eee5296b2102d2aca2693bbf19904a");
            Assert.True(ret.s == "69317738d22c18efb009eea2f444608b");
            Assert.True(ret.t == "0/1574934623/2541cfc8e0c1ddc923742c8c95d70729");
            Assert.True(
                ret.Url ==
                "https://auto.ru/captchaimg?aHR0cHM6Ly9leHQuY2FwdGNoYS55YW5kZXgubmV0L2ltYWdlP2tleT0wMDFvYkVDbUlDeURuSm41eGZ4YkZkMlZrbTE5RWRoSiZzZXJ2aWNlPWF1dG9ydQ,,_0/1574857440/4c4f88189a77f9d2274dffedab3f2ce1_ac98dde3a5bd985cab8c768bb61273e8");
            Assert.True(ret.Req_id == "1574857440319623-3876207305581685653");
            Assert.True(ret.ErrorRnd == "236442167520");
        }
    }
}