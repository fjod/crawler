using AngleSharp.Html.Parser;
using Application.Interfaces;
using BrowseSharp.Common;
using Crawler;
using Crawler.Browser;
using Moq;
using RestSharp;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Parsing
{
    public class GoodResponseTest
    {
        private readonly ITestOutputHelper _helper;

        public GoodResponseTest(ITestOutputHelper helper)
        {
            _helper = helper;
        }
      
      
        [Fact]
        public void TestTableLoader()
        {
            _helper.WriteLine("Test for loading table ads started...");
            
            var q = new HtmlParser().ParseDocument(Resource.GoodResponse_txt);
            IDocument z = new Document(new RestRequest(),new RestResponse(),q);
            z.Data = Resource.GoodResponse_txt;
            

            var logMock = new Mock<ISpiderLog>();
            
            var tableLoader = new AdTableLoader(logMock.Object );

            var docType = tableLoader.FindDocumentType_Table(z);
            
            _helper.WriteLine("Response type is: " +docType.ToString());
            
            Assert.True(docType == EResponseType.GoodResponse);
            var ret = tableLoader.ParseAds(z);
            Assert.True(ret.Count == 37);
            _helper.WriteLine($"Amount of found items is {ret.Count}");
        }
    }
}