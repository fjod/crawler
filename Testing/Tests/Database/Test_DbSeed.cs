﻿using System;
using System.Linq;
using System.Security.Claims;
using DataBase;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Tests.Database
{
    public class DbSeed
    {
        [Fact]
        public async void Test()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("SeedBase")
                .Options;

            var context = new DataContext(options);
            
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(typeof(DataContext)))
                .Returns(context);

            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);

            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory
                .Setup(x => x.CreateScope())
                .Returns(serviceScope.Object);

            serviceProvider
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);

            
            var sut = new DbInit(serviceScopeFactory.Object);

            await sut.SeedData();
            
            //now we must have one user in db
            var vkUser = context.VkUsers.First();
            Assert.Equal("3596363", vkUser.Uid);

            var appUser = context.Users.First();
            Assert.Equal("3596363", appUser.Uid);

            var nameClaim = context.UserClaims.First(c => c.ClaimType == ClaimTypes.Name);
            Assert.Equal(nameClaim.ClaimValue,appUser.UserName);
            Assert.Equal(appUser.Id,nameClaim.UserId);
 
            var roleClaim = context.UserClaims.First(c => c.ClaimType == ClaimTypes.Role);
            Assert.Equal("SuperAdmin",roleClaim.ClaimValue);
            Assert.Equal(appUser.Id,roleClaim.UserId);

            await sut.AddSampleData();
            
            //10 ads, 20 users, 10*20 comments and ratings
            var totalAds = await context.ScrapedAds.ToListAsync();
            Assert.True(totalAds.Count == sut.TotalAdsCount);
            //each city should start with sampleName
            Assert.True(totalAds.All(a => a.City.StartsWith(VkUser.SampleAdder)));

            var comments = await context.Comments.ToListAsync();
            Assert.True(comments.Any());
            foreach (var comment in comments)
            {
                Assert.Contains(comment.ScrapedAd, totalAds);
            }
            var ratings = await context.Ratings.ToListAsync();
            foreach (var rating in ratings)
            {
                Assert.Contains(rating.ScrapedAd, totalAds);
            }

            var users = await context.Users.ToListAsync();
            Assert.True(users.Count == sut.TotalUsersCount +1);
            var vkUsers = await context.VkUsers.ToListAsync();
            Assert.True(vkUsers.Count == sut.TotalUsersCount +1);

            //must have only one superAdmin 
            var claims = await context.UserClaims.ToListAsync();
            var superAdmin = claims.Where(c => c.ClaimValue == "SuperAdmin");
            Assert.Single(superAdmin);
            
            
            await sut.RemoveSampleData();
            
            claims = await context.UserClaims.ToListAsync();
            superAdmin = claims.Where(c => c.ClaimValue == "SuperAdmin");
            Assert.Single(superAdmin); //still have one superAdmin
            Assert.Equal(2,claims.Count()); //and only two claims for him
            
            users = await context.Users.ToListAsync();
            Assert.True(users.Count == 1);
            vkUsers = await context.VkUsers.ToListAsync();
            Assert.True(vkUsers.Count == 1);
            
            totalAds = await context.ScrapedAds.ToListAsync();
            Assert.Empty(totalAds);
            comments = await context.Comments.ToListAsync();
            Assert.Empty(comments);
            ratings = await context.Ratings.ToListAsync();
            Assert.Empty(ratings);
        }
    }
}