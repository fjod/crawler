using System;
using System.IO;
using Ductus.FluentDocker.Builders;
using Ductus.FluentDocker.Extensions;
using Ductus.FluentDocker.Services;
using MoreLinq.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Database
{
    public class DockerIntegration : BaseTest, IDisposable
    {
        private readonly ITestOutputHelper _helper;

        public DockerIntegration(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        private ICompositeService _svc;
        /// <summary>
        /// to run this test in CI we must have docker installed!
        /// </summary>
        [Fact]
        public void Test()
        {
            var launchPath = Directory.GetCurrentDirectory();
            launchPath = launchPath.Replace("Testing\\Tests\\bin\\Debug\\netcoreapp3.1", "");
            launchPath = launchPath.Replace("Testing", "");
            launchPath = launchPath.Replace("Tests", "");
            launchPath = launchPath.Replace("bin", "");
            launchPath = launchPath.Replace("Debug", "");
            launchPath = launchPath.Replace("netcoreapp3.1", "");
            while (launchPath.EndsWith("/") || launchPath.EndsWith("\\"))
            {
                launchPath= launchPath.Remove(launchPath.Length - 1, 1);
            }
            launchPath = Path.Combine(launchPath, "tortest2");
            launchPath = Path.Combine(launchPath, "docker-compose.yml");
            _helper.WriteLine($"Looking for compose file at {launchPath}");
            Assert.True(File.Exists(launchPath));

             _svc = new Builder()
                .UseContainer()
                .UseCompose()
                .FromFile(launchPath)
                .RemoveOrphans()
                .Build().Start();
                    
            //var installPage = await "http://localhost/".Wget();

            //_helper.WriteLine($"installPage looks like: {installPage}");
            // Assert.True(installPage.Length > 0); //this means that db and webapi works
            // Assert.Equal(1, _svc.Hosts.Count); 
            // Assert.Equal(3, _svc.Containers.Count);  
            // Assert.Equal(3, _svc.Images.Count);

            _svc.Containers.ForEach(c =>
            {
                _helper.WriteLine($"Container {c.Name} is {c.State}");
            });
            _svc.Containers.ForEach(c =>
            {
               
                Assert.True(c.State == ServiceRunningState.Running);
            });
           
        }


        public void Dispose()
        {
            _svc?.Dispose();
        }
    }
}