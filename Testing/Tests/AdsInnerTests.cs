﻿using System;
using System.Linq;
using Application.Interfaces;
using Application.Misc;
using Crawler.Analyzer;
using DataBase;
using Domain;
using InfraStructure;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class AdsInnerTests : BaseTest
    {
        private readonly ITestOutputHelper _helper;

        public AdsInnerTests(ITestOutputHelper helper)
        {
            _helper = helper;
        }
        [Fact]
        public void CompareAdsTest()
        {
            _helper.WriteLine("started CompareAdTest test");

                        
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "CompareAds")
                .Options;

            using var context = new DataContext(options);
            CrawlerStringComparer csc = new CrawlerStringComparer();
          
            var scrapedDate = DateTime.Now;
            scrapedDate = scrapedDate - TimeSpan.FromDays(5);
            context.ScrapedAds.Add(ScrapedAd.CreateMockAd(scrapedDate));
            context.SaveChanges();

            var sample = context.ScrapedAds.First();
            var sut = sample.Copy();
            var mock = ScrapedAd.CreateMockAd(DateTime.Now);
            
            Assert.True(sample.IsEqual(sut,csc));
            Assert.True(sample.IsEqual(sample,csc));
            Assert.False(sample.IsEqual(null,csc));
            
            Assert.False(sample.IsEqual(mock,csc));

            //even with different URL it should be same ad
            sut.BaseUrl = mock.BaseUrl;
            Assert.True(sample.IsEqual(sut,csc));

            //even with different createDate it should be same ad
            sut.CreateDate = mock.CreateDate;
            Assert.True(sample.IsEqual(sut,csc));
            
            //user deleted vin
            sut.Vin = "";
            Assert.True(sample.IsEqual(sut,csc));

            var tempDesc = sut.Description;
            sut.Description = mock.Description;
            // name + color + city + year = not enough
            Assert.False(sample.IsEqual(sut,csc)); 

            // name + color + city + year + desc = enough
            sut.Description = tempDesc.Substring(0, tempDesc.Length-tempDesc.Length / 5);
            Assert.True(sample.IsEqual(sut,csc));
            
            _helper.WriteLine("finished CompareAdTest test");
        }

        [Fact]
        public void AnalyzeAdsTest()
        {
            int uniqueAmount = 5;
            
            _helper.WriteLine("started AnalyzeAdsTest test");
            
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "CompareAds")
                .Options;

            using var context = new DataContext(options);
            context.ScrapedAds.RemoveRange(context.ScrapedAds);
            context.SaveChanges();
            
            _helper.WriteLine($"we have {context.ScrapedAds.Count()} total ads now");
            
            for (int i = 0; i < uniqueAmount; i++)
            {
                var scrapedDate = DateTime.Now;
                scrapedDate -= TimeSpan.FromDays(i+10);
                var ad = ScrapedAd.CreateMockAd(scrapedDate);
                ad.ID = 351+i;
                ad.UniqueId = 351+i;
                context.ScrapedAds.Add(ad);
                _helper.WriteLine($"created ad # {i} with id = {ad.ID}");
            }

            context.SaveChanges();
            _helper.WriteLine($"created {context.ScrapedAds.Count()} uniques");

            var sut1 = context.ScrapedAds.First().Copy();
            sut1.UniqueId = null;
            sut1.ScrapedAt = DateTime.Now;
            sut1.ID = 451;
            context.ScrapedAds.Add(sut1); //10 uniques + 1 not (urls are same)
            context.SaveChanges();
            _helper.WriteLine($"sut1, now we have {context.ScrapedAds.Count()} ads in total");
            
            var sut2 = context.ScrapedAds.Skip(1).First().Copy();
            sut2.UniqueId = null;
            sut2.ScrapedAt = DateTime.Now;
            sut2.BaseUrl = new Uri("https://www.qwerty.com/asdf"); //user created new one
            sut2.ID = 452;
            context.ScrapedAds.Add(sut2); //10 uniques + 2 not (create time is same)
            context.SaveChanges();
            _helper.WriteLine($"sut2, now we have {context.ScrapedAds.Count()} ads in total");

            var sut3 = ScrapedAd.CreateMockAd(DateTime.Now);
            sut3.ID = 453;
            sut3.CreateDate = DateTime.Now;
            sut3.CreateDate -= TimeSpan.FromDays(25);
            context.ScrapedAds.Add(sut3); //11 uniques + 2 not  (completely new one)
            context.SaveChanges();
            _helper.WriteLine($"sut3, now we have {context.ScrapedAds.Count()} ads in total");
            
            var sut4 = context.ScrapedAds.Skip(3).First().Copy();
            sut4.UniqueId = null;
            sut4.ScrapedAt = DateTime.Now;
            sut4.BaseUrl = new Uri("https://www.zxcvbnmmgggtt.com/asdf"); //user created new one with new date
            sut4.CreateDate = DateTime.Now;
            sut4.CreateDate -= TimeSpan.FromDays(15);
            sut4.ID = 454;
            context.ScrapedAds.Add(sut4); //11 uniques + 3 not (different url and create time)
            context.SaveChanges();
            _helper.WriteLine($"sut4, now we have {context.ScrapedAds.Count()} ads in total");

            context.SaveChanges();
            
            Mock<IInjection> injectorMock = new Mock<IInjection>();
            injectorMock.Setup(foo => foo.GetType<ICompareString>()).Returns(new CrawlerStringComparer());
            injectorMock.Setup(foo => foo.GetType<DataContext>()).Returns(context);
            injectorMock.Setup(foo => foo.GetType<ISpiderLog>()).Returns(new TestSpiderLog(_helper));
            
            AnalyzeAds analyzeAds = new AnalyzeAds(injectorMock.Object);
            analyzeAds.Start();
            
            var allAds = context.ScrapedAds.ToList();
            _helper.WriteLine("total ads after analysis " + allAds.Count);

            
            var uniqueCount = (from scrapedAd in context.ScrapedAds where scrapedAd.ID == scrapedAd.UniqueId select scrapedAd).Count();
            _helper.WriteLine($"unique expected = 6 got {uniqueCount}");
            Assert.Equal(6, uniqueCount );

            var notUniqueCount = (from scrapedAd in context.ScrapedAds where scrapedAd.ID != scrapedAd.UniqueId select scrapedAd).Count();
            _helper.WriteLine($"notUniqueCount expected = 3, got {notUniqueCount}");
            Assert.Equal(3, notUniqueCount );
            
            _helper.WriteLine("finished AnalyzeAdsTest test");
        }

        [Fact]
        public void CheckLastAdTest()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "CheckLastAd")
                .Options;

            using var context = new DataContext(options);

            Random random = new Random();
            
            var unique = ScrapedAd.CreateMockAd(DateTime.Now);
            unique.ID = random.Next(5000, 10000);
            var path = "https://www.auto.ru" + unique.BaseUrl.AbsolutePath + "?sort=cr_date-desc";
            unique.BaseUrl = new Uri(path);
            unique.UniqueId = unique.ID;

            for (int i = 1; i < 6; i++)
            {
                var scrapedDate = DateTime.Now;
                var ad = ScrapedAd.CreateMockAd(scrapedDate);
                ad.ID = unique.ID+i;
                ad.UniqueId = unique.ID+i;
                path = "https://www.auto.ru" + ad.BaseUrl.AbsolutePath + "?sort=cr_date-desc";
                ad.BaseUrl = new Uri(path);
                context.ScrapedAds.Add(ad);
                _helper.WriteLine($"created ad # {i} with id = {ad.ID}");
            }

            context.ScrapedAds.Add(unique);
            context.SaveChanges();

            var uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(6,uniqueAmount);

            var sut = unique.Copy();
            sut.UniqueId = null;
            sut.ScrapedAt = DateTime.Now;
            sut.ID = unique.ID+10;
            context.ScrapedAds.Add(sut);
            
            
            var sut2 = unique.Copy();
            sut2.UniqueId = null;
            sut2.ScrapedAt = DateTime.Now;
            sut2.ID = unique.ID+11;
            sut2.BaseUrl = new Uri("https://www.qwerty.com/asdf"); //user created new one
            path = "https://www.auto.ru" + sut2.BaseUrl.AbsolutePath + "?sort=cr_date-desc";
            sut2.BaseUrl = new Uri(path);

            context.ScrapedAds.Add(sut2);

            context.SaveChanges();
            
            Mock<IInjection> injectorMock = new Mock<IInjection>();
            injectorMock.Setup(foo => foo.GetType<ICompareString>()).Returns(new CrawlerStringComparer());
            injectorMock.Setup(foo => foo.GetType<DataContext>()).Returns(context);
            injectorMock.Setup(foo => foo.GetType<ISpiderLog>()).Returns(new TestSpiderLog(_helper));
            
            AnalyzeAds analyzeAds = new AnalyzeAds(injectorMock.Object);
            
            analyzeAds.CheckLastAd(sut);
            uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(6,uniqueAmount);
            Assert.Equal(sut.UniqueId,unique.ID);
            
            analyzeAds.CheckLastAd(sut2);
            uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(6,uniqueAmount);
            Assert.Equal(sut2.UniqueId,unique.ID);
        }

        [Fact]
        public void CheckFirstParsing()
        {
                var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "CheckLastAd2")
                .Options;

            using var context = new DataContext(options);
            
            

            Random random = new Random();
            
            var unique = ScrapedAd.CreateMockAd(DateTime.Now);
            unique.ID = random.Next(5000, 10000);
            var path = "https://www.auto.ru" + unique.BaseUrl.AbsolutePath + "?sort=cr_date-desc";
            unique.BaseUrl = new Uri(path);
            context.ScrapedAds.Add(unique);
            context.SaveChanges();

            var uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(0,uniqueAmount);
            
            Mock<IInjection> injectorMock = new Mock<IInjection>();
            injectorMock.Setup(foo => foo.GetType<ICompareString>()).Returns(new CrawlerStringComparer());
            injectorMock.Setup(foo => foo.GetType<DataContext>()).Returns(context);
            injectorMock.Setup(foo => foo.GetType<ISpiderLog>()).Returns(new TestSpiderLog(_helper));
            
            AnalyzeAds analyzeAds = new AnalyzeAds(injectorMock.Object);

            analyzeAds.CheckLastAd(unique);
            
            uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(1,uniqueAmount);

            
            var sut = unique.Copy();
            sut.UniqueId = null;
            sut.ScrapedAt = DateTime.Now;
            sut.ID = unique.ID+10;
            context.ScrapedAds.Add(sut);
            
            
            var sut2 = unique.Copy();
            sut2.UniqueId = null;
            sut2.ScrapedAt = DateTime.Now;
            sut2.ID = unique.ID+11;
            sut2.BaseUrl = new Uri("https://www.qwerty.com/asdf"); //user created new one
            path = "https://www.auto.ru" + sut2.BaseUrl.AbsolutePath + "?sort=cr_date-desc";
            sut2.BaseUrl = new Uri(path);
            context.ScrapedAds.Add(sut2);

            context.SaveChanges();

            analyzeAds.CheckLastAd(sut);
            uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(1,uniqueAmount);
            Assert.Equal(sut.UniqueId,unique.ID);
            
            analyzeAds.CheckLastAd(sut2);
            uniqueAmount = context.ScrapedAds.Count(ad => ad.ID == ad.UniqueId);
            Assert.Equal(1,uniqueAmount);
            Assert.Equal(sut2.UniqueId,unique.ID);
        }
    }
}