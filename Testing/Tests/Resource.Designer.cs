﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tests {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Tests.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!doctype html&gt;
        ///&lt;html class=&quot;html_controller_card&quot; lang=&quot;ru&quot; data-reactroot=&quot;&quot;&gt;
        ///&lt;head&gt;
        ///  &lt;meta charSet=&quot;utf-8&quot;/&gt;
        ///  &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;/&gt;
        ///  &lt;meta name=&quot;format-detection&quot; content=&quot;telephone=no&quot;/&gt;
        ///  &lt;link rel=&quot;stylesheet&quot;
        ///        href=&quot;//auto.ru/_crpd/ogz269U27/ff852efBk5/AwmGAzHhrpIizxQj06LLIzQyPpF0sydL8WC7KeLqSADtYydLDFO4zsH2U5onJ-F1w80BLb5fNbcexVdwGjxYH1JJlld0sfqMfpJUNMWp12MzcnzMKf0ySr5boDbrzDM06lgSWCEylvjazFC-LzKZaMDmLx2mQmCNYtdsaS8dTBYkEaAbI8uI8yH7IWnV5JAMGTopkVP9Dk [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string AdResponse_2_txt {
            get {
                return ResourceManager.GetString("AdResponse_2.txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html&gt;
        ///&lt;html class=&quot;ua_js_no&quot;&gt;
        ///&lt;head&gt;
        ///    &lt;meta charset=&quot;utf-8&quot;/&gt;
        ///    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;/&gt;
        ///    &lt;title&gt;ÐÑÐ¿Ð¸ÑÑ Ð±/Ñ Yamaha FZ6 Ð² ÐÐ¾ÑÐºÐ²Ðµ: ÑÑÑÐ½ÑÐ¹ ÐºÐ»Ð°ÑÑÐ¸Ðº 2005 Ð³Ð¾Ð´Ð° Ð¿Ð¾ ÑÐµÐ½Ðµ
        ///        240Â 000 ÑÑÐ±Ð»ÐµÐ¹ Ð½Ð° ÐÐ²ÑÐ¾.ÑÑ&lt;/title&gt;
        ///    &lt;script nonce=&quot;DmfckWx+o7/TLfh5nC/NNQ==&quot;&gt;(function (e, c) {
        ///        e[c] = e[c].replace(/(ua_js_)no/g, &quot;$1yes&quot;);
        ///    })(document.documentElement, &quot;className&quot;);&lt;/script&gt;
        ///    &lt;meta class=&quot;meta&quot; property=&quot;f [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string AdResponse_txt {
            get {
                return ResourceManager.GetString("AdResponse.txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!doctype html&gt;&lt;html class=&quot;html_controller_card&quot; lang=&quot;ru&quot; data-reactroot=&quot;&quot;&gt;&lt;head&gt;&lt;meta charSet=&quot;utf-8&quot;/&gt;&lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;/&gt;&lt;meta name=&quot;format-detection&quot; content=&quot;telephone=no&quot;/&gt;&lt;link rel=&quot;stylesheet&quot; href=&quot;//auto.ru/_crpd/Lnc1J3204/26b1864Q/Nv126h9EkzAB1nXc6MiqFC5n0ycNso-hCNeHCW2NGWJsv9MvQIDLUhWLfyIMMKyy0lwVeJbKVJN2gxZn3fsKFCSY984aUJagIAMVqMlM72DqcvdGDXdbQFjDdSgffRn33B8DTxCBDODW2L-jbuIetMF_d2qH2dYUpgUEFf1L6FQUTuapbhdjIncEhT6_azwALGdzFpxDCOFZR5UpD41JRNzcMr0B480SxkoMYHzTb [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string AdResponse3_badImgPic {
            get {
                return ResourceManager.GetString("AdResponse3_badImgPic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html&gt;
        ///&lt;html class=&quot;i-ua_js_no i-ua_css_standard&quot; lang=&quot;ru&quot;&gt;
        ///&lt;head&gt;
        ///    &lt;meta charset=&quot;utf-8&quot;/&gt;
        ///    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;/&gt;
        ///    &lt;title&gt;Ой!&lt;/title&gt;
        ///    &lt;script&gt;;
        ///    (function (d, e, c, r) {
        ///        e = d.documentElement;
        ///        c = &quot;className&quot;;
        ///        r = &quot;replace&quot;;
        ///        e[c] = e[c][r](&quot;i-ua_js_no&quot;, &quot;i-ua_js_yes&quot;);
        ///        if (d.compatMode != &quot;CSS1Compat&quot;) e[c] = e[c][r](&quot;i-ua_css_standart&quot;, &quot;i-ua_css_quirks&quot;)
        ///    })(document);
        ///    ;(function (d, e, [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string captcha_response_txt {
            get {
                return ResourceManager.GetString("captcha_response.txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!doctype html&gt;
        ///&lt;html lang=&quot;ru&quot; data-reactroot=&quot;&quot;&gt;
        ///&lt;head&gt;
        ///    &lt;meta charSet=&quot;utf-8&quot;/&gt;
        ///    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge&quot;/&gt;
        ///    &lt;meta name=&quot;format-detection&quot; content=&quot;telephone=no&quot;/&gt;
        ///    &lt;link rel=&quot;stylesheet&quot;
        ///          href=&quot;//yastatic.net/s3/vertis-frontend/autoru-frontend-desktop/_/common_06b9bdc62d30794b641f.css&quot;/&gt;
        ///    &lt;link rel=&quot;stylesheet&quot;
        ///          href=&quot;//yastatic.net/s3/vertis-frontend/autoru-frontend-desktop/_/listing_173b16ecf8ca98d7e7c3.css&quot;/&gt;
        ///    &lt;link rel=&quot;shortcut icon&quot; h [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string GoodResponse_txt {
            get {
                return ResourceManager.GetString("GoodResponse.txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html&gt;
        ///&lt;html lang=&quot;ru&quot;&gt;
        ///&lt;head&gt;
        ///    &lt;meta charset=&quot;UTF-8&quot;/&gt;
        ///    &lt;link rel=&quot;alternate&quot; hreflang=&quot;en&quot; href=&quot;/en/proxylist/country/RU/all/date/all&quot;/&gt;
        ///    &lt;link rel=&quot;alternate&quot; hreflang=&quot;cs&quot; href=&quot;/cs/proxylist/country/RU/all/date/all&quot;/&gt;
        ///    &lt;link rel=&quot;alternate&quot; hreflang=&quot;fr&quot; href=&quot;/fr/proxylist/country/RU/all/date/all&quot;/&gt;
        ///    &lt;link rel=&quot;alternate&quot; hreflang=&quot;ru&quot; href=&quot;/ru/proxylist/country/RU/all/date/all&quot;/&gt;
        ///    &lt;link rel=&quot;alternate&quot; hreflang=&quot;zh&quot; href=&quot;/zh/proxylist/country/RU/all/date/all&quot;/&gt;
        ///    &lt;link [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string proxy_response_txt {
            get {
                return ResourceManager.GetString("proxy_response.txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg1 {
            get {
                object obj = ResourceManager.GetObject("sampleImg1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg2 {
            get {
                object obj = ResourceManager.GetObject("sampleImg2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg3 {
            get {
                object obj = ResourceManager.GetObject("sampleImg3", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg4 {
            get {
                object obj = ResourceManager.GetObject("sampleImg4", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg5 {
            get {
                object obj = ResourceManager.GetObject("sampleImg5", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap sampleImg6 {
            get {
                object obj = ResourceManager.GetObject("sampleImg6", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
