﻿namespace Crawler
{
    
    public enum EResponseType
    {
        /// <summary>
        /// вернулся ответ на наш запрос
        /// </summary>
        GoodResponse,
        /// <summary>
        /// происходит, когда запрос из нерусской зоны адресов
        /// </summary>
        AcceptRules,
        /// <summary>
        /// один раз он подумал и отправил меня на мобильную версию
        /// </summary>
        RedirectToMobile,
        /// <summary>
        /// нас спалили и нужно вводить капчу
        /// </summary>
        EnterCaptcha,
        Error
    }
}