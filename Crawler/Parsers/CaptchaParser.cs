﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using AngleSharp.Html.Dom;
using Application.Interfaces;
using BrowseSharp.Common;
using Crawler.Browser;
using RestSharp;

namespace Crawler.Parsers
{
    public class CaptchaParser
    {
        private readonly ISpiderLog _log;


        public CaptchaParser(ISpiderLog log)
        {
            _log = log;
        }

        public CaptchaDocValues Parse(IDocument document)
        {
            CaptchaDocValues ret = new CaptchaDocValues();
            /*
             3 - in response headers I get Location: https://auto.ru/showcaptcha?retpath=https%3A//auto.ru/motorcycle/yamaha/fz6/all%3F_f09b0e60822ed3b4ab550f1b71858b7e&t=0/1574934623/2541cfc8e0c1ddc923742c8c95d70729&s=69317738d22c18efb009eea2f444608b
          where I have 3 params:
          "retpath":"https://auto.ru/motorcycle/yamaha/fz6/all?_f09b0e60822ed3b4ab550f1b71858b7e",
             "t":"0/1574934623/2541cfc8e0c1ddc923742c8c95d70729",
             "s":"69317738d22c18efb009eea2f444608b"}}
             
             * <input class="form__key" type="hidden" name="key"
               value="001obECmICyDnJn5xfxbFd2Vkm19EdhJ_0/1574857440/4c4f88189a77f9d2274dffedab3f2ce1_82eee5296b2102d2aca2693bbf19904a"/>
        <input
            class="form__retpath" type="hidden" name="retpath"
            value="https://auto.ru/motorcycle/yamaha/fz6/all?beaten=1&amp;custom_state_key=CLEARED&amp;image=true&amp;sort_offers=cr_date-DESC&amp;top_days=off&amp;currency=RUR&amp;output_type=list&amp;page_num_offers=0&amp;mark-model-nameplate=YAMAHA%23FZ6_d7452076b53126e246a571b9106bdd69"/>
        
             */
            
            #region from doc body (key, retpath, image url)

            foreach (var element in document.Forms[0].HtmlForm.ChildNodes)
            {
                if (element is IHtmlInputElement)
                {
                    
                    if ((element as IHtmlInputElement).ClassName == "form__key")
                    {
                        ret.Key = (element as IHtmlInputElement).DefaultValue;
                    }
                    if ((element as IHtmlInputElement).ClassName == "form__retpath")
                    {
                        ret.RetPath = (element as IHtmlInputElement).DefaultValue;
                    }
                }
                
            }

            foreach (var attribute in document.Forms[0].HtmlForm.Attributes)
            {
                if (attribute.Name == "onsubmit")
                {
                    //ym(10630330, 'reachGoal', 'enter_captcha_value', { 'req_id': '1574857440319623-3876207305581685653' }); return true;
                    var start = attribute.Value.IndexOf("'req_id':", StringComparison.Ordinal);
                    var end = attribute.Value.IndexOf("}); return true;", StringComparison.Ordinal);
                    ret.Req_id = attribute.Value.Substring(start+"'req_id':".Length+2,end-start-"}); return true;".Length+3);
                    
                }
            }

            /*
             *  <div class="captcha__image"><img
                        src="https://auto.ru/captchaimg?aHR0cHM6Ly9leHQuY2FwdGNoYS55YW5kZXgubmV0L2ltYWdlP2tleT0wMDFvYkVDbUlDeURuSm41eGZ4YkZkMlZrbTE5RWRoSiZzZXJ2aWNlPWF1dG9ydQ,,_0/1574857440/4c4f88189a77f9d2274dffedab3f2ce1_ac98dde3a5bd985cab8c768bb61273e8"/>
                </div>
             */

            var imgElements = document.HtmlDocument.All.Where((e => e is IHtmlImageElement));
            foreach (var imgElement in imgElements)
            {
                if ((imgElement as IHtmlImageElement).Source.Contains("captcha"))
                {
                    ret.Url = (imgElement as IHtmlImageElement).Source;
                }
            }
            #endregion

            Parameter locationHeader = null;
            try
            {
                  locationHeader =document.Response.Headers.First(header => header.Name == "Location"); 
            }
            catch (Exception)
            {
                var locValue =WebAccess.LastResponseHeaders.First(header => header.Key == "Location").Value; 
                locationHeader = new Parameter("Location",locValue, locValue,ParameterType.HttpHeader);
            }
            if (locationHeader == null)
            {
                _log.LogMessage("Captcha parser: cant find location header in response");
                return null;
            }

            var locationHeaderValue = locationHeader.Value.ToString();
            var tLocation = locationHeaderValue.IndexOf("&t=", StringComparison.Ordinal);
            var sLocation = locationHeaderValue.IndexOf("&s=", StringComparison.Ordinal);
            var tValue = locationHeaderValue.Substring(tLocation+3, sLocation - tLocation-3);
            var sValue = locationHeaderValue.Substring(sLocation+3, locationHeaderValue.Length - sLocation-3);

            ret.s = sValue;
            ret.t = tValue;

            var head = document.Head.ChildNodes.Where(c => c is IHtmlLinkElement);
            if (head.Any())
            {
                foreach (var node in head)
                {
                    var link =(node as IHtmlLinkElement).Href;
                    var indexOfRnd = link.IndexOf("rnd=", StringComparison.Ordinal);
                    if (indexOfRnd > 0)
                        ret.ErrorRnd = link.Substring(indexOfRnd).Replace("rnd=",String.Empty);
                }
            }
            else
            {
                _log.LogMessage("Captcha parser: cant find script rnd num in response");
                return null;
            }
            
            
            return ret;
        }
        
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class CaptchaDocValues
    {
        public string Key { get; set; }
        public string RetPath { get; set; }
        public string Url { get; set; }
        public string Req_id { get; set; }
        public string s { get; set; }
        public string t { get; set; }

        public string ErrorRnd { get; set; }
    }
}