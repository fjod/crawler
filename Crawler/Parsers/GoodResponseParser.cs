﻿using System;
using System.Collections.Generic;
using AngleSharp.Html.Dom;
using BrowseSharp.Common;
using System.Linq;
using Application.Interfaces;

namespace Crawler.Parsers
{
    public class GoodResponseParser
    {
        
        public static string AdsTableNameSelector => "ListingItemTitle-module__link";
        
        
        public List<Uri> Parse(IDocument document, ISpiderLog log)
        {
            List<Uri> adUris = new List<Uri>();    
            var adContainers = document.HtmlDocument.All.Where
                (m=>m.ClassList.Contains(AdsTableNameSelector));

            var totalAds = 0;
            foreach (var adContainer in adContainers)
            {
                if (!(adContainer is IHtmlAnchorElement element)) continue;
                adUris.Add(new Uri(element.Href));
                log.LogMessage($"Found ad url = {element.Href}");
                totalAds += 1;
            }
            log.LogMessage($"Spider found {totalAds} of ads on this table");
            return adUris;
        }
    }
}