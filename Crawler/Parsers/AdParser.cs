﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Misc;
using BrowseSharp.Common;
using Crawler.Browser;
using DataBase;
using Domain;
using Newtonsoft.Json.Linq;

namespace Crawler.Parsers
{
    public class AdParser
    {
        private readonly ISaveResult _saveResult;
        private readonly ISpiderLog _log;
        private readonly IWebAccess _webAccess;
        private readonly ICompareString _comparer;
        private readonly DataContext _context;

        public AdParser(IInjection injection)
        {
            _saveResult = injection.GetType<ISaveResult>();
            _log = injection.GetType<ISpiderLog>();
           
            _comparer = injection.GetType<ICompareString>();
            _context = injection.GetType<DataContext>();
            _webAccess = injection.GetType<IWebAccess>();
        }

        public EResponseType FindDocumentType_Ad(IDocument document)
        {
            if (document == null) return EResponseType.Error;
            var lastDocType = EResponseType.Error;

            var text = document.Response.Content;
            if (String.IsNullOrEmpty(text))
                text = document.Data.ToString();

            var pos = text.IndexOf("INITIAL_STATE", StringComparison.Ordinal);
            pos += text.IndexOf("initial-state", StringComparison.Ordinal);
            if (pos > 0) lastDocType = EResponseType.GoodResponse;
            if (document.Response.Content.Contains(AdTableLoader.RulesString)) lastDocType = EResponseType.AcceptRules;
            if (document.Response.Content.Contains(AdTableLoader.CaptchaString))
                lastDocType = EResponseType.EnterCaptcha;

            _log.LogMessage($"Spider found document type for ad {document.RequestUri} = {lastDocType.ToString()}");
            return lastDocType;
        }

        /// <summary>
        /// найденное уникальное объявление для текущего объявления
        /// </summary>
        ScrapedAd _uniqueForCurrentAd;
        /// <summary>
        /// parse ad from page and store to db, no logic here
        /// </summary>
        /// <param name="document"></param>
        /// <exception cref="Exception"></exception>
        public async Task<ScrapedAd> Parse(IDocument document)
        {
            var text = document.Response.Content;
            if (string.IsNullOrEmpty(text))
                text = document.Data.ToString();

            JObject adInfo;
            try
            {
                var pos = text.IndexOf("INITIAL_STATE", StringComparison.Ordinal);
                var better = text.Substring(pos + 15);
                var pos2 = better.IndexOf("</script>", StringComparison.Ordinal);
                var result = better.Substring(0, pos2 - 1);

                adInfo = JObject.Parse(result);
            }
            catch (Exception e)
            {
                try
                {
                    var pos = text.IndexOf("initial-state", StringComparison.Ordinal);
                    var better = text.Substring(pos + 15);
                    var pos2 = better.IndexOf("</script>", StringComparison.Ordinal);
                    var result = better.Substring(0, pos2 - 1);
                    result += "}";
                    adInfo = JObject.Parse(result);
                }
                catch (Exception e2)
                {
                    _log.LogMessage("Cant parse ad");
                    _log.LogMessage(e.ToString());
                    _log.LogMessage(e2.ToString());
                    return null;
                }
            }

            var card = adInfo["card"];
            var ad = new ScrapedAd();

            var descContent = card["description"]?.ToString().Decode();
            if (descContent?.Split('?').Length > 2)
                descContent = card["description"]?.ToString();

            ad.Description = descContent;

            var sFreshDate = card["additional_info"]?["fresh_date"]?.ToString(); 
            if (!String.IsNullOrEmpty(sFreshDate)) ad.FreshDate = sFreshDate.ToDateTime();
            var sCreateDate = card["additional_info"]?["creation_date"]?.ToString();
            if (!String.IsNullOrEmpty(sCreateDate)) ad.CreateDate = sCreateDate.ToDateTime();

            var manufactureYearToParse = card["documents"]?["year"]?.ToString();
            var manufactureYear = int.Parse(manufactureYearToParse ?? "0");
            ad.ManufactureYear = manufactureYear;

            var vin = card["documents"]?["vin"]?.ToString();
            ad.Vin = vin ?? "";

            var price = card["price_info"]?["price"]?.ToString(); //bike was sold, so auto.ru does not show price anymore
            if (price == null) price = "0";
            ad.Price = int.Parse(price);
            
            ad.SellerName = card["seller"]["name"].ToString().Decode();
            if (ad.SellerName.Split('?').Length > 2)
                ad.SellerName = card["seller"]["name"].ToString();

            var mileageToParse = card["state"]?["mileage"]?.ToString();
            ad.Mileage = int.Parse(mileageToParse ?? "0");
            
            ad.Color = card["color"]?.ToString() ?? card["color_hex"]?.ToString() ?? "";
            ad.City = card["poi"]?["city"]?.ToString().Decode()
                      ?? card["seller"]?["location"]?["region_info"]?["name"]?.ToString() ?? "";

            var adUri = document.RequestUri;
            if (adUri == null)
            {
                var uriString = adInfo["config"]?["data"]?["meta"]?["pageUrl"]?.ToString();
                if (!String.IsNullOrWhiteSpace(uriString)) adUri = new Uri(uriString);
            }

            if (adUri == null)
            {
                var uriString = adInfo["config"]?["data"]?["url"].ToString();
                if (!String.IsNullOrWhiteSpace(uriString)) adUri = new Uri("https://www.auto.ru" + uriString);
            }

            ad.BaseUrl = adUri;
            if (ad.BaseUrl == null)
                throw new Exception("Cant find URI for current AD!");

            ad.ScrapedAt = DateTime.Now;


            var images = card["state"]?["image_urls"];
            var scrapedPics = new List<AdPic>();


            
            if (_comparer != null)
            {
                var uniqueAds = _context.ScrapedAds.Where(_ad => _ad.ID == _ad.UniqueId).ToList();
                
                _uniqueForCurrentAd = 
                    uniqueAds.FirstOrDefault(unAd => unAd.IsEqual(ad, _comparer));
                _log.LogMessage(_uniqueForCurrentAd == null ? "Current ad is unique" : "Found parent ad");
               
            }

            if (images != null)
            {
                try
                {
                    var photoTuples = (
                        from im in images
                        from childIm in im.Children()
                        from size in childIm.Children()
                        select (size["320x240"].ToString(), size["1200x900n"].ToString())
                    ).ToList();

                    foreach (var (smallPic, bigPic) in photoTuples)
                    {
                        var gotSmall = GotUrlHash(GetHttpString(smallPic));
                        if (!gotSmall)
                            scrapedPics.Add(await _webAccess.GetAdPic(GetHttpString(smallPic), false));

                        var gotBig = GotUrlHash( GetHttpString(bigPic));
                        if (!gotBig)
                            scrapedPics.Add(await _webAccess.GetAdPic(GetHttpString(bigPic), true));
                    }
                }
                catch (Exception) //page of new format, continue with second parser
                {
                    await ParseImages(images, scrapedPics);
                }
            }

            
            _log.LogAd(ad);
            _log.LogMessage($"Scraped {scrapedPics.Count} pics for this ad");


            _saveResult.Save(ad, scrapedPics, _log);
            return ad;
        }

        private static string GetHttpString(string input, bool withAddition = false)
        {
            if (!withAddition) return "http:" + input;
            return "http://auto.ru" + input;
        }

        private bool GotUrlHash(string picUrl)
        {
            if (_context == null) return false;
            if (_uniqueForCurrentAd == null) return false;
           
            var gotImageAdditional = from adPic in _context.AdPicUrls
                where adPic.Url == picUrl
                select adPic;

            var gotImageBase = from adPic in _context.AdPics
                where adPic.Ad.ID == _uniqueForCurrentAd.ID
                where adPic.Url == picUrl
                select adPic;
             
            
            
            var ret = gotImageBase.Any() || gotImageAdditional.Any();

            if (ret) _log.LogMessage($"found saved url for this image, in base pics = {gotImageBase}");
            return ret;
        }

        private async Task ParseImages(JToken images, List<AdPic> scrapedPics)
        {
            AdPic lastBigPic = null;
            AdPic lastSmallPic = null;

            void LogError(Exception e, string url)
            {
                _log.LogMessage($"cant get pic {url}");
                _log.LogMessage(e.Message);
                _log.LogMessage(e.StackTrace);
                _log.LogMessage(e.Source);
                _log.LogMessage(e.InnerException?.Message);
            }
            
            async Task<bool> DownloadImage(string picAddress, bool isItBig)
            {
                var niceUrl = CorrectLocation(picAddress); //check if url is fine
                if (String.IsNullOrEmpty(niceUrl)) return false; //cant find good url

                if (GotUrlHash(niceUrl)) return true; //check if we already have downloaded it before

                try
                {
                    var pic = await _webAccess.GetAdPic(niceUrl, isItBig); //download and store in tempfile
                    if (isItBig) lastBigPic = pic;
                    else lastSmallPic = pic;
                    scrapedPics.Add(pic);
                }
                catch (ArgumentException ea) //downloaded file is not an image
                {
                    _log.LogMessage("-------cant get image------, see next log entry for url");
                    LogError(ea, niceUrl);
                    //GDPR_Workaround workaround = new GDPR_Workaround(_log);
                    //await workaround.FixGDPR(_webAccess.PrevCookies);
                    //await _webAccess.Navigate(niceUrl);
                    //_log.LogMessage("----gdpr should be fixed----");
                    try
                    {   _log.LogMessage($"current url: {niceUrl}");
                        niceUrl = CorrectLocation(picAddress); //check if url is fine again
                        _log.LogMessage($"rechecked url: {niceUrl}");
                        var pic = await _webAccess.GetAdPic(niceUrl, isItBig);
                        if (isItBig) lastBigPic = pic;
                        else lastSmallPic = pic;
                        scrapedPics.Add(pic);
                    }
                    catch (Exception e)
                    {
                        _log.LogMessage("--------cant download image even after fix-------------");
                        LogError(e,niceUrl);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _log.LogMessage("--------another exception-------------");
                    LogError(e,niceUrl);
                    return false;
                }
                return true;
            }
        
            try
            {
                if (images == null) return;
                var ims = from im in images select im;
                foreach (var jToken in ims)
                {
                    string smallPic = jToken["sizes"]?["320x240"]?.ToString();
                    string bigPic = jToken["sizes"]?["1200x900n"]?.ToString();
                    if (string.IsNullOrEmpty(smallPic)|| string.IsNullOrEmpty(bigPic)) continue;

                    _log.LogMessage($"Trying to get small pic at {smallPic}");
                    var gotSmall = await DownloadImage(smallPic,false);

                    _log.LogMessage($"Trying to get big pic at {bigPic}");
                    var gotBig = await DownloadImage(bigPic,true);

                    if (!gotBig || !gotSmall) continue;
                    _log.LogMessage("Got both pics");
                    if (lastBigPic == null || lastSmallPic == null) continue;
                    
                    
                    lastBigPic.Sibling = lastSmallPic;
                    lastSmallPic.Sibling = lastBigPic;
                }
            }
            catch (Exception e2)
            {
                _log.LogMessage("New parsing failed " + Environment.NewLine + e2.Message);
                _log.LogMessage(e2.StackTrace);
                _log.LogMessage(e2.Source);
            }
        }

        string CorrectLocation(string url)
        {
            string first = GetHttpString(url);
            if (URLExists(first)) return first;
            string second = GetHttpString(url, true);
            if (URLExists(second)) return second;

            return String.Empty;
        }
        
        bool URLExists(string url)
        {
            bool IsValidURI(string uri)
            {
                if (!Uri.IsWellFormedUriString(uri, UriKind.Absolute)) return false;
                if (!Uri.TryCreate(uri, UriKind.Absolute, out var tmp)) return false;
                return tmp.Scheme == Uri.UriSchemeHttp || tmp.Scheme == Uri.UriSchemeHttps;
            }

            if (!IsValidURI(url)) return false;

            try
            {
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Timeout = 500; // milliseconds
                webRequest.Method = "HEAD";

                try
                {
                    webRequest.GetResponse();
                }
                catch (WebException e)
                {
                    return e.Message.Contains("405");
                }
                catch (Exception)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        
        
    }
}