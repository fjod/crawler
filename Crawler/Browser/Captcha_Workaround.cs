﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Crawler.Parsers;
using Domain;
using Newtonsoft.Json;
using IDocument = BrowseSharp.Common.IDocument;

// ReSharper disable InconsistentNaming

namespace Crawler.Browser
{
    public class CaptchaWorkaround
    {
        private const string FirstRndUrl = "https://auto.ru/captcha.error-counter.js?rnd=";
        private const string SecondRndUrl = "https://auto.ru/captcha.min.css?rnd=";
        private const string ThirdRndUrl = "https://auto.ru/captcha.min.js?rnd=";
        private const string RetPathUrl = "https://auto.ru/showcaptcha?retpath=";
        private const string VerochkaUrl = "https://auto.ru/captcha_verochka";
        private const string CaptchaUrl = "https://auto.ru/checkcaptcha";

        private readonly ISpiderLog _log;
        private readonly ICaptchaSolver _captchaSolver;
        private readonly IGetImageForCaptchaUrl _image;


        public CaptchaWorkaround(ISpiderLog log, ICaptchaSolver captchaSolver, IGetImageForCaptchaUrl image)
        {
            _log = log;
            _captchaSolver = captchaSolver;
            _image = image;
        }

        
        public async Task<string> Start(IDocument document, WebAccess access)
        {
            CaptchaParser parser = new CaptchaParser(_log);
            var values = parser.Parse(document);

            //get captcha from url
            //solve it using interface


            //here we have all that we need, so continue with captured workflow
            var ret = await CuteRequest.SendGet(FirstRndUrl + values.ErrorRnd, WebAccess.Client);
            _log.LogMessage(
                $"captcha: request towards first error counter returned with {ret.Content.ToString().Length} chars");


            var ret2 = await CuteRequest.SendGet(SecondRndUrl + values.ErrorRnd, WebAccess.Client);
            _log.LogMessage(
                $"captcha: request towards second error counter returned with {ret2.Content.ToString().Length} chars");


            _log.LogMessage("Getting captcha image...");
            var fileWithImagePath = await _image.Start(values.Url, WebAccess.Client);
            if (!System.IO.File.Exists(fileWithImagePath))
            {
                _log.LogMessage("Was not able to download captcha image");
                return values.RetPath;
            }

            _log.LogMessage("Solving captcha..");
            var captchaString = _captchaSolver.SolveImageCaptcha(fileWithImagePath);

            if (String.IsNullOrEmpty(captchaString))
            {
                _log.LogMessage("captcha: can not solve it");
                return String.Empty;
            }

            // var captchas =captchaString.Split(' ');
            // captchaString = captchas[0] + "%20" + captchas[1];
             _log.LogMessage($"Solved as {captchaString}");


            var ret3 = await CuteRequest.SendGet(ThirdRndUrl + values.ErrorRnd, WebAccess.Client);
            _log.LogMessage(
                $"captcha: request towards third error counter returned with {ret3.Content.ToString().Length} chars");

            StringBuilder makeRetPath = new StringBuilder();
            makeRetPath.Append(RetPathUrl);
            makeRetPath.Append(values.RetPath);
            makeRetPath.Append("&t=");
            makeRetPath.Append(values.t);
            makeRetPath.Append("&s=");
            makeRetPath.Append(values.s);

            var requestContent = verochkaRequestContent(values);
            var retVerochka = await CuteRequest.SendPost(VerochkaUrl, WebAccess.Client,
                requestContent.content,
                new List<(string name, string value)>
                {
                    ("Referer", makeRetPath.ToString()),
                    //("Content-Type", "text/plain;charset=UTF-8"),
                    //("Content-Length", requestContent.count.ToString())
                });
            _log.LogMessage(
                $"captcha: request towards Verochka returned with {retVerochka.Content.ToString().Length} chars");

            
            var betterRetpath = "https://auto.ru/motorcycle/yamaha/fz6/all" + "?_" + values.RetPath.Split("?_")[1];
            
            StringBuilder captchaGetUrl = new StringBuilder();
            captchaGetUrl.Append(CaptchaUrl);
            captchaGetUrl.Append("?key=");
            captchaGetUrl.Append(System.Web.HttpUtility.UrlEncode(values.Key,Encoding.GetEncoding(1251)));
            captchaGetUrl.Append("&retpath=");
            captchaGetUrl.Append(System.Web.HttpUtility.UrlEncode(betterRetpath,Encoding.GetEncoding(1251)));
            //captchaGetUrl.Append("https://auto.ru/motorcycle/yamaha/fz6/all/");
            captchaGetUrl.Append("&rep=");
            captchaGetUrl.Append(System.Web.HttpUtility.UrlEncode(captchaString,Encoding.GetEncoding(1251)));

            //var finalRequest = await CuteRequest.SendGet(captchaGetUrl.ToString(), CuteSpider.Client);
        
            string send = captchaGetUrl.ToString();
            access.RemoveAllCookiesExceptGdprRelated();
            await access.Navigate(send);
        
            var gotLocationHeader = WebAccess.LastResponseHeaders.Any(header => header.Key == "Location");
            if (gotLocationHeader)
            {
                var locationHeader = WebAccess.LastResponseHeaders.First(header => header.Key == "Location");
                return locationHeader.Value;
            }

            return String.Empty;
        }

        (HttpContent content, int count) verochkaRequestContent(CaptchaDocValues values)
        {
            var ret = new VerochkaPost
            {
                chromeNavigatorPropPresence = false,
                geo = true,
                languages = new[] {"en-US", "en"},
                permission = "default",
                permissionStatus = "prompt",
                plugins = new string[0],
                t = values.t,
                userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0",
                verochkaParams = new VerochkaParams {req_id = values.Req_id, service = "autoru"},
                webdriverDocumentElementPropPresence = false,
                webdriverNavigatorPropPresence = true,
                webdriverSpecificVarsPresence = new string[0]
            };

            var stringPayload = JsonConvert.SerializeObject(ret);

            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "text/plain");
            return (httpContent, stringPayload.Length);
        }

        private class VerochkaPost
        {
            [JsonProperty("chromeNavigatorPropPresence")]

            public bool chromeNavigatorPropPresence { get; set; }

            [JsonProperty("geo")] public bool geo { get; set; }

            [JsonProperty("languages")] public string[] languages;

            [JsonProperty("permission")] public string permission { get; set; }

            [JsonProperty("permissionStatus")] public string permissionStatus { get; set; }

            [JsonProperty("plugins")] public string[] plugins;

            [JsonProperty("t")] public string t { get; set; }

            [JsonProperty("userAgent")] public string userAgent { get; set; }

            [JsonProperty("verochkaParams")] public VerochkaParams verochkaParams { get; set; }

            [JsonProperty("webdriverDocumentElementPropPresence")]
            public bool webdriverDocumentElementPropPresence { get; set; }

            [JsonProperty("webdriverNavigatorPropPresence")]
            public bool webdriverNavigatorPropPresence { get; set; }

            [JsonProperty("webdriverSpecificVarsPresence")]
            public string[] webdriverSpecificVarsPresence;
        }

        private class VerochkaParams
        {
            [JsonProperty("req_id")] public string req_id { get; set; }

            [JsonProperty("service")] public string service { get; set; }
        }
    }
}