﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interfaces;
using BrowseSharp.Common;
using Crawler.Parsers;

namespace Crawler.Browser
{
    public class AdTableLoader
    {
        //when requested from foreign IP address
        public static string RulesString => "Продолжая использование данного сайта, я соглашаюсь с тем,";
        public static string CaptchaString => "Ой!";
        
        private readonly ISpiderLog _log;
        public AdTableLoader(ISpiderLog log)
        {
            _log = log;
        }

        private EResponseType _lastDocType;


        public EResponseType FindDocumentType_Table(IDocument document)
        {
            _lastDocType = EResponseType.Error;
            if (document == null) return _lastDocType;

            var adContainers = document.HtmlDocument.All.Where
                (m => m.ClassList.Contains(GoodResponseParser.AdsTableNameSelector));
            if (adContainers.Any()) _lastDocType = EResponseType.GoodResponse;
            if (document.Body.TextContent.StartsWith("Привет")) 
                _lastDocType = EResponseType.GoodResponse;
            if (string.IsNullOrEmpty(document.Response.Content))
            {
                if (document.Body.InnerHtml.Contains(RulesString)) _lastDocType = EResponseType.AcceptRules;
                if (document.Body.InnerHtml.Contains(CaptchaString)) _lastDocType = EResponseType.EnterCaptcha;
            }
            else
            {
                if (document.Response.Content.Contains(RulesString)) _lastDocType = EResponseType.AcceptRules;
                if (document.Response.Content.Contains(CaptchaString))
                    _lastDocType = EResponseType.EnterCaptcha;    
            }
            

            _log.LogMessage($"Spider found document type in table = {_lastDocType.ToString()}");
            return _lastDocType;
        }

        /// <summary>
        /// Find ads on page and return uri's to each ad 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public List<Uri> ParseAds(IDocument document)
        {
            switch (_lastDocType)
            {
                case EResponseType.GoodResponse:
                {
                    GoodResponseParser parser = new GoodResponseParser();
                    return parser.Parse(document, _log);
                }
            }

            return null;
        }
    }
}