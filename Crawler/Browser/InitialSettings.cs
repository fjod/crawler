﻿using System.Collections.Generic;

namespace Crawler.Browser
{
  
    public static class InitialSettings

    {
        public static string RobotsUrl => "https://auto.ru/robots.txt";
        public static string GetBaseUrl(int page=1)
        {
            return $"https://auto.ru/motorcycle/yamaha/fz6/all/?top_days=off&currency=RUR&output_type=list&damage_group=NOT_BEATEN&customs_state_group=CLEARED&sort=cr_date-desc&page={page}";
        }
        public static string GetMobileUrl(int page)
        {
            return $"https://m.auto.ru/motorcycle/yamaha/fz6/all/?beaten=1&custom_state_key=CLEARED&image=true&sort_offers=cr_date-DESC&top_days=off&currency=RUR&output_type=list&page_num_offers={page}&mark-model-nameplate=YAMAHA%23FZ6";
        }
        
        
      public static  Dictionary<string, string> GetHeaders => new Dictionary<string, string>
      {
          //{"Host","auto.ru"},
          {"Connection","keep-alive"},
          {"Cache-Control","max-age=0"},
         // {"Upgrade-Insecure-Requests","1"},
          {"User-Agent","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Mobile Safari/537.36"},
          {"Sec-Fetch-Mode","navigate"},
          {"Sec-Fetch-User","?1"},
        //  {"Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"},
          {"Sec-Fetch-Site","same-origin"},
         // {"Accept-Encoding","gzip, deflate, br"},
         // {"Accept-Language","en-US,en;q=0.5"}
      }; 
    }
}
