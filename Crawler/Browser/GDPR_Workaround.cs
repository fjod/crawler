﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using RestSharp;

namespace Crawler.Browser
{
    // ReSharper disable once InconsistentNaming
    public class GDPR_Workaround
    {
        private readonly ISpiderLog _log;

        public GDPR_Workaround(ISpiderLog log)
        {
            _log = log;
        }

        // ReSharper disable once InconsistentNaming
        public async Task<bool> FixGDPR(IList<RestResponseCookie> cookies)
        {
            // must accept gdpr-thing; so
            //1. add such cookie to our cookies
            //document.cookie = 'gdpr=1;path=/;max-age=31536000;domain=.auto.ru'
            //at feb 2020 :
            //document.cookie = 'autoru_gdpr=1;path=/;max-age=31536000;secure;domain=' + config.cookies_domain;
          //  cookies.Add(new RestResponseCookie {Name = "gdpr", Value = "1", Path = "/", Domain = ".auto.ru"});
          //  document.cookie = 'autoru_gdpr=1;path=/;max-age=31536000;secure;domain=' + config.cookies_domain;
            cookies.Add(new RestResponseCookie {Name = "autoru_gdpr", Value = "1"});
            cookies.Add(new RestResponseCookie {Name = "path", Value = "/"});
            cookies.Add(new RestResponseCookie {Name = "max-age", Value = "31536000"});
            cookies.Add(new RestResponseCookie {Name = "secure"});
            cookies.Add(new RestResponseCookie {Name = "domain", Value = ".auto.ru"});

            //2. send POST request as
            // https://auto.ru/-/ajax/desktop/gdprConfirm/
            //with headers
            // xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
            //xhr.setRequestHeader('x-csrf-token', cookies['_csrf_token']);
            string csrfValue;
            try
            {
                var csrf = cookies.First(c => c.Name == "_csrf_token");
                csrfValue = csrf.Value;
            }
            catch (Exception)
            {
                _log.LogMessage("cant find csrf token");
                return false;
            }

            StringBuilder cookieBuilder = new StringBuilder();
            foreach (var restResponseCookie in cookies)
            {
                cookieBuilder.Append($" {restResponseCookie.Name}={restResponseCookie.Value};");
            }

            //cookieBuilder.Append(" los=1; bltsr=1; ");
            
            var request = new HttpRequestMessage(HttpMethod.Post, "https://auto.ru/-/ajax/desktop/gdprConfirm/")
            {
                Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>())
            };
            request.Headers.Add("x-csrf-token", csrfValue);
            // request.Headers.Add("User-Agent",
            //     "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0");
            request.Headers.Add("Accept", "*/*");
            request.Headers.Add("Accept-Language", "en-US,en;q=0.5");
            request.Headers.Add("Origin", "https://auto.ru");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Referer", "https://auto.ru/moskva/motorcycle/yamaha/fz6/all/");
            request.Headers.Add("Cookie", cookieBuilder.ToString());

            request.Headers.Add("Pragma", "no-cache");
            request.Headers.Add("Cache-Control", "no-cache");

            var gdprResponse = await WebAccess.Client.SendAsync(request);
            _log.LogMessage(await gdprResponse.Content.ReadAsStringAsync());
            foreach (var gdprResponseHeader in gdprResponse.Headers)
            {
                _log.LogMessage(gdprResponseHeader.Key);
                var values = gdprResponseHeader.Value.ToList();
                foreach (var value in values)
                {
                    _log.LogMessage(value);
                }
            }
            gdprResponse.Headers.TryGetValues("Set-Cookie", out var cookiesInGdprResponse);

            foreach (var cook in cookiesInGdprResponse)
            {
                var indexOfEqual = cook.IndexOf("=", StringComparison.Ordinal);
                var indexOfEnd = cook.IndexOf(";", StringComparison.Ordinal);
                var name = cook.Substring(0, indexOfEqual);
                var value = cook.Substring(indexOfEqual, indexOfEnd - indexOfEqual);
                cookies.Add(new RestResponseCookie() {Name = name, Value = value});
            }

            // var path = cookies.First(c => c.Name == "path");
            // cookies.Remove(path);
            //
            // var age = cookies.First(c => c.Name == "max-age");
            // cookies.Remove(age);
            //
            // var domain = cookies.First(c => c.Name == "domain");
            // cookies.Remove(domain);

            return gdprResponse.IsSuccessStatusCode;
        }
    }
}