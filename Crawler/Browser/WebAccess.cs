using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Html.Dom;
using AngleSharp.Io;
using AngleSharp.Io.Network;
using Application.Interfaces;
using BrowseSharp.Common;
using Domain;
using Domain.ImageComparison;
using Microsoft.EntityFrameworkCore.Internal;
using RestSharp;

namespace Crawler.Browser
{
    public class WebAccess : IWebAccess
    {
        #region fields

        public static HttpClient Client { get; private set; }
        private readonly CookieContainer _cookieContainer = new CookieContainer();
        private ISpiderLog _log;
        private readonly IBrowsingContext _context;
        private readonly IBrowsingContext _offlineContext;
        public IList<RestResponseCookie> PrevCookies { get; private set; }
        public static IDictionary<String, String> LastResponseHeaders;

        public enum WebClientType
        {
            SimplestOne,
            WithCookies,
            WithProxyAndCookies
        }

        #endregion

        #region ctor

        public WebAccess()
        {
        }

        public WebAccess(ISpiderLog log, WebClientType type = WebClientType.WithCookies)
        {
            HttpClientRequester requester;
            _log = log;

            IConfiguration config;
            switch (type)
            {
                case WebClientType.SimplestOne:
                {
                    Client = new HttpClient();
                    requester = new HttpClientRequester(Client);
                    config = Configuration.Default.WithRequester(requester)
                        .WithDefaultLoader(new LoaderOptions {IsResourceLoadingEnabled = true});
                    break;
                }
                case WebClientType.WithCookies:
                {
                    var httpClientHandler = new HttpClientHandler
                    {
                        CookieContainer = _cookieContainer, UseCookies = true
                    };
                    Client = new HttpClient(httpClientHandler);
                    requester = new HttpClientRequester(Client); //with headers and proxy

                    config =
                        Configuration.Default.WithRequester(requester)
                            .WithDefaultLoader(new LoaderOptions {IsResourceLoadingEnabled = true})
                            .WithCookies(); //cookies enabled

                    break;
                }
                case WebClientType.WithProxyAndCookies:
                {
                    Settings settings = new Settings();
                    var httpClientHandler = new HttpClientHandler
                    {
                        Proxy = settings.GetProxy(),
                        PreAuthenticate = true, UseDefaultCredentials = false,
                        CookieContainer = _cookieContainer, UseCookies = true
                    };
                    Client = new HttpClient(httpClientHandler);
                    requester = new HttpClientRequester(Client); //with headers and proxy
                    config =
                        Configuration.Default.WithRequester(requester).WithDefaultLoader()
                            .WithCookies(); //cookies enabled

                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Wrong enum type, CuteSpider ctor");
            }

            Client.Timeout = TimeSpan.FromSeconds(30);
            _log.LogMessage($"WebAccess created with type = {type.ToString()}");

            _context = BrowsingContext.New(config);

            IConfiguration offlineConfig = Configuration.Default;
            _offlineContext = BrowsingContext.New(offlineConfig);
        }

        #endregion

        public async Task<IDocument> Navigate(string url)
        {
            void LogError(Exception e)
            {
                _log.LogMessage($"Spider error, {e.Message}");
                _log.LogMessage(e.StackTrace);
                _log.LogMessage(e.Source);
            }

            if (PrevCookies != null)
            {
                var existingCookies = _cookieContainer.List();

                foreach (var restResponseCookie in PrevCookies
                ) //check if all existing cookies are added and not expired
                {
                    var added = existingCookies.Find(c => c.Name == restResponseCookie.Name);
                    if (added == null)
                    {
                        var add = new Cookie(restResponseCookie.Name, restResponseCookie.Value, "/", ".auto.ru");
                        var expireTime = DateTime.Now;
                        expireTime = expireTime.AddDays(6);
                        add.Expires = expireTime;
                        add.Expired = false;
                        _cookieContainer.Add(add);
                    }
                }

                _log.LogMessage("existing cookies");
                foreach (var restResponseCookie in _cookieContainer.List())
                {
                    _log.LogMessage(restResponseCookie.Name + " = " + restResponseCookie.Value + " ; isExpired = " +
                                    restResponseCookie.Expired);
                }
            }

            try
            {
                var requestForStatusCode = new DocumentRequest(new Url(url));

                var response = await _context.GetService<IDocumentLoader>().FetchAsync(requestForStatusCode).Task;

                byte[] spanOfBytes = new byte[response.Content.Length + 10];
                LastResponseHeaders = response.Headers;
                LastResponseHeaders.TryAdd("Location", response.Address.ToString());

                response.Content.Read(spanOfBytes);
                var responseContent = Encoding.UTF8.GetString(spanOfBytes.ToArray());

                var doc = await _offlineContext.OpenAsync(req =>
                    req.Content(responseContent));

                IHtmlDocument htmlDocument = doc as IHtmlDocument;
                var ret = new Document(new RestRequest(),
                    new RestResponse
                    {
                        StatusCode = response.StatusCode,
                        Content = responseContent
                        //Headers field has private setter
                    }, htmlDocument);

                var cookiesFromContainer = _cookieContainer.GetAllCookies()
                    .GroupBy(c => c.Name)
                    .Select(cookies => cookies.First());

                var cookiesInResponse_container =
                    cookiesFromContainer.ToDictionary(cookie => cookie.Name,
                        cookie => cookie.Value);

                UpdateCookies(cookiesInResponse_container);
                RemoveAllCookiesExceptGdprRelated();
                return ret;
            }

            catch (HttpRequestException e)
            {
                LogError(e);
            }
            catch (ObjectDisposedException e)
            {
                LogError(e);
            }
            catch (TaskCanceledException e)
            {
                LogError(e);
            }

            return null;
        }

        private void UpdateCookies(IDictionary<string, string> cookiesInResponse)
        {
            if (PrevCookies == null)
            {
                PrevCookies = new List<RestResponseCookie>();
                foreach (var restResponseCookie in cookiesInResponse)
                {
                    PrevCookies.Add(new RestResponseCookie
                        {Name = restResponseCookie.Key, Value = restResponseCookie.Value});
                }
            }
            else
            {
                //save new cookies, overwrite old one
                foreach (var restResponseCookie in cookiesInResponse)
                {
                    try
                    {
                        var foundOldOne = PrevCookies.First(c => c.Name == restResponseCookie.Key);
                        if (foundOldOne == null)
                        {
                            PrevCookies.Add(new RestResponseCookie
                                {Name = restResponseCookie.Key, Value = restResponseCookie.Value});
                        }
                        else
                        {
                            foundOldOne.Value = restResponseCookie.Value;
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        PrevCookies.Add(new RestResponseCookie
                            {Name = restResponseCookie.Key, Value = restResponseCookie.Value});
                    }
                }
            }

            foreach (var restResponseCookie in PrevCookies)
            {
                _log.LogMessage(restResponseCookie.Name + " " + restResponseCookie.Value);
            }
        }

        public void RemoveAllCookiesExceptGdprRelated()
        {
            List<string> cookieNames = new List<string>
                {"autoru_gdpr", "path", "max-age", "secure", "domain", "_csrf_token", "gdpr"};
            List<RestResponseCookie> cookiesToRemove = new List<RestResponseCookie>();

            foreach (var restResponseCookie in PrevCookies) //check if all existing cookies are added
            {
                if (cookieNames.Contains(restResponseCookie.Name)) continue;

                var add = new Cookie(restResponseCookie.Name, restResponseCookie.Value, "/", ".auto.ru");
                var expireTime = DateTime.Now;
                expireTime = expireTime.Subtract(TimeSpan.FromDays(1));
                add.Expires = expireTime;
                add.Expired = true;
                _cookieContainer.Add(add);
                cookiesToRemove.Add(restResponseCookie);
            }

            foreach (var restResponseCookie in cookiesToRemove)
            {
                PrevCookies.Remove(restResponseCookie);
            }
        }

        Random rand = new Random();

        public async Task<AdPic> GetAdPic(string uri, bool isBigPic)
        {
            AdPic image = null;
            var download = _context.GetService<IDocumentLoader>().FetchAsync(new DocumentRequest(new Url(uri)));
            using var response = await download.Task;
            var result = response.Content;
            if (PrevCookies == null)
                UpdateCookies(
                    new Dictionary<string, string>()); //need to create cookies list somewhere, even if it is empty

            using (Bitmap ret = new Bitmap(result))
            {
                var sleepTime = rand.Next(5, 10);
                _log.LogMessage("Sleeping for: " + sleepTime);
                Thread.Sleep(TimeSpan.FromSeconds(sleepTime));

                image = ret.ToAdPic(isBigPic, uri);
            }

            return image;
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}