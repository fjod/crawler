﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Crawler
{
    public class Stopper
    {
        private readonly Action _callback;


        public Stopper(Action callback)
        {
            _callback = callback;
            Task.Factory.StartNew(Checker);
        }

        void Checker()
        {
            while (true)
            {
                var fileLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var directory = Path.GetDirectoryName(fileLocation);
                var files = Directory.EnumerateFiles(directory);
                if (files.Any(f => f.Contains(CuteSpider.StopFileName)))
                {
                    _callback();
                    try
                    {
                        string delFileName = Path.Combine(directory, CuteSpider.StopFileName);
                        File.Delete(delFileName);
                    }
                    catch (Exception)
                    {
                        // ignored, well if we cant delete this file...
                    }

                    return;
                }
                Thread.Sleep(TimeSpan.FromMinutes(1));
                //Thread.Sleep(TimeSpan.FromSeconds(1));

            }
        }
        
    }
}