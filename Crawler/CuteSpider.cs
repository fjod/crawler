﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Crawler.Analyzer;
using Crawler.Browser;
using Crawler.Parsers;
using Domain.ImageComparison;


namespace Crawler
{
    public class CuteSpider : IDisposable
    {
     
        private readonly AdTableLoader _advTable;
        private readonly ISpiderLog _log;
        private int _currentPage;
        private readonly AdParser _adParser;
        private readonly AnalyzeAds _analyzeAds;
        public const string StopFileName = "stopCrawling";
        private bool _shutDown;
        private int _currentAd;
        private List<Uri> _lastAdUris;
        public IWebAccess WebAccess { get; }


        public CuteSpider(IInjection injection)
        {
            
            _log = injection.GetType<ISpiderLog>();
            WebAccess = injection.GetType<IWebAccess>();

            _advTable = new AdTableLoader(_log);
            _adParser = new AdParser(injection);
            _analyzeAds = new AnalyzeAds(injection);

            var stopper = new Stopper(() =>
            {
                _shutDown = true;
                _log.LogMessage($"Received shutdown command at {DateTime.Now}");
            });
            
            TempFileName.CleanTempFolder();

           
        }

        void Sleep()
        {
            var rand = new Random();
            var sleepTime = rand.Next(50, 100);
            _log.LogMessage("Sleeping for: " + sleepTime);
            Thread.Sleep(TimeSpan.FromSeconds(sleepTime));
        }

        public async Task Start()
        {
           // Test t = new Test();
           // await t.Perform(WebAccess, _adParser,_advTable, _log);
            
            DateTime startTime = DateTime.Now;
            var rand = new Random();

            var retDoc = await WebAccess.Navigate(InitialSettings.RobotsUrl);
            if (retDoc?.Response.StatusCode != HttpStatusCode.OK)
            {
                _log.LogMessage("Spider cant get to robots.txt with: " + retDoc?.Response.Content);
            }
            
            Sleep();

            _log.LogMessage("Spider started working on pages");
            for (_currentPage = 1; _currentPage < 8; _currentPage++)
                //hardcoded number of pages, usually there are 5-6 pages 
            {
                if (_shutDown)
                {
                    _log.LogMessage("Exiting...");
                    return;
                }

                await CheckAdsTable();

                if (_lastAdUris == null) continue;

                _lastAdUris = _lastAdUris.OrderBy(ad => rand.Next()).ToList(); //shuffle ads
                _currentAd = 0;
                foreach (var lastAdUri in _lastAdUris)
                {
                    await ParseAd(lastAdUri);
                    if (_shutDown)
                    {
                        _log.LogMessage("Exiting...");
                        return;
                    }
                }
            }

            _log.LogMessage($"Spider finished working on {_currentPage} pages");
            var totalWorkTime = DateTime.Now - startTime;
            _log.LogMessage($"It took  {totalWorkTime.Hours} hours and {totalWorkTime.Minutes} minutes");
        }

        private async Task ParseAd(Uri lastAdUri)
        {
            DateTime adStartTime = DateTime.Now;

            Sleep();
            _log.LogMessage($"Working on ad {_currentAd} from {_lastAdUris.Count}, page {_currentPage}");

            var document = await WebAccess.Navigate(lastAdUri.ToString());
            if (document == null)
            {
                _log.LogMessage($"Got exception with ad {lastAdUri}");
                Sleep();
                document = await WebAccess.Navigate(lastAdUri.ToString());
            }

            var docType = _adParser.FindDocumentType_Ad(document);
            if (docType == EResponseType.GoodResponse)
            {
                _log.LogMessage($"Parsing {lastAdUri} ad page");
                var ad = await _adParser.Parse(document);
                _analyzeAds.CheckLastAd(ad);
            }
            else
            {
                _log.LogMessage($"Going to {lastAdUri}  returned {docType} type");
            }

            var adParseTime = DateTime.Now - adStartTime;
            _log.LogMessage(
                $"It took {adParseTime.Minutes} minutes and {adParseTime.Seconds} seconds for {_currentAd} from {_lastAdUris.Count} Ads on page {_currentPage}");

            _currentAd++;
        }


        /// <summary>
        /// download and parse Ads on (currentPage)
        /// </summary>
        /// <returns></returns>
        async Task CheckAdsTable()
        {
            var document = await WebAccess.Navigate(InitialSettings.GetBaseUrl(_currentPage));
            var responseType = _advTable.FindDocumentType_Table(document);
            if (responseType != EResponseType.Error)
            {
                if (responseType == EResponseType.GoodResponse)
                {
                    _lastAdUris = new List<Uri>();
                    _log.LogMessage($"Spider is parsing ads on page {_currentPage} ...");
                    _lastAdUris = _advTable.ParseAds(document);
                }

                if (responseType == EResponseType.AcceptRules)
                {
                    _log.LogMessage("Got GDPR");
                    GDPR_Workaround workaround = new GDPR_Workaround(_log);
                    var ret = await workaround.FixGDPR((WebAccess as WebAccess)?.PrevCookies);
                    _log.LogMessage($"GDPR was fixed = {ret}");
                    _currentPage -= 1;
                    Thread.Sleep(TimeSpan.FromSeconds(6));
                    return;
                }

                if (responseType == EResponseType.EnterCaptcha)
                {
                    _log.LogMessage("Got captcha, skipping for now");
                    Sleep();
                }
            }
            else
            {
                _log.LogMessage($"Spider got bad result while parsing {InitialSettings.GetBaseUrl(_currentPage)} " +
                                $"with response type = {responseType}");
                Sleep();
            }
        }

        public void Dispose()
        {
            WebAccess.Dispose();
        }
    }
}