﻿using System.Threading.Tasks;
using Application.Interfaces;
using Crawler.Browser;
using Crawler.Parsers;

namespace Crawler
{
    public class Test
    {
        public async Task Perform(IWebAccess access, AdParser parser, AdTableLoader _advTable, ISpiderLog _log)
        { 
            string url = "https://auto.ru/motorcycle/used/sale/yamaha/fz6/3540982-3996590d/";
            var retDoc = await access.Navigate(InitialSettings.RobotsUrl);
            retDoc = await access.Navigate(url);
            var responseType = _advTable.FindDocumentType_Table(retDoc);
            if (responseType == EResponseType.AcceptRules)
            {
                GDPR_Workaround workaround = new GDPR_Workaround(_log);
                var ret = await workaround.FixGDPR(access.PrevCookies);
            }
            //load ad page to see what we will have
            var retDoc2 = await access.Navigate(url);
            responseType = _advTable.FindDocumentType_Table(retDoc2);
            //var  ad1 = await parser.Parse(retDoc2);//all fine, fails because db is on docker which is off
            access.RemoveAllCookiesExceptGdprRelated();
            var retDoc31 = await access.Navigate(url);  //socket exception here
            var responseType2 = _advTable.FindDocumentType_Table(retDoc31);
            var  ad2 = await parser.Parse(retDoc31);//
            
            //302 =	https://autoru.yandex.ru/sync/?retpath=https%3A%2F%2Fauto.ru%2Fautoruyandexcookiesync%2F%3Fretpath%3Dhttps%253A%252F%252Fauto.ru%252Fmotorcycle%252Fused%252Fsale%252Fyamaha%252Ffz6%252F3540982-3996590d%252F
            //      https://autoru.yandex.ru/sync/?retpath=https%3A%2F%2Fauto.ru%2Fautoruyandexcookiesync%2F%3Fretpath%3Dhttps%253A%252F%252Fauto.ru%252Fmotorcycle%252Fused%252Fsale%252Fyamaha%252Ffz6%252F3540982-3996590d%252F
            //it should return 302 with Location header
            var adr2 =
                "https://auto.ru/autoruyandexcookiesync/?retpath=https%3A%2F%2Fauto.ru%2Fmotorcycle%2Fused%2Fsale%2Fyamaha%2Ffz6%2F3540982-3996590d%2F";
            var yandSyncUrlFromHeaders = WebAccess.LastResponseHeaders["Location"];
            var retDoc3 = await access.Navigate(adr2);
                //"https://autoru.yandex.ru/sync/?retpath=https%3A%2F%2Fauto.ru%2Fautoruyandexcookiesync%2F%3Fretpath%3Dhttps%253A%252F%252Fauto.ru%252F"
                var  ad = await parser.Parse(retDoc3);
            var secondSync = WebAccess.LastResponseHeaders["Location"];
            var retDoc4 = await access.Navigate(secondSync);
            
         // var  ad = await parser.Parse(retDoc4);
        }
    }
}