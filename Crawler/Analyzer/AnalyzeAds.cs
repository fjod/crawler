﻿using System;
using System.Linq;
using Application.Interfaces;
using Application.Misc;
using DataBase;
using Domain;

namespace Crawler.Analyzer
{
    public class AnalyzeAds
    {
        private readonly DataContext _context;
        private readonly ICompareString _comparer;
        private readonly ISpiderLog _log;

        public AnalyzeAds(IInjection injection)
        {
            _context = injection.GetType<DataContext>();
            _comparer = injection.GetType<ICompareString>();
            _log = injection.GetType<ISpiderLog>();
        }

        /// <summary>
        /// find if that ad is unique
        /// </summary>
        /// <param name="advertToSave"></param>
        public void CheckLastAd(ScrapedAd advertToSave)
        {
            
            //ad is already in db; get it
            var currentAd = _context.ScrapedAds.Find(advertToSave.ID);
            
            if (currentAd == null)
            {
                _log.LogMessage($"Can not find ad to assign unique Id to it! {advertToSave.BaseUrl}");
                return;
            }
            

            var uniqueAds = _context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId).ToList();
            try
            {
                var similar = uniqueAds.Find(unAd => unAd.IsEqual(currentAd, _comparer)); //ex
                if (similar != null)
                {
                    //found unique one, so it's the same ad but updated
                    _log.LogMessage($"found unique {similar.ID} for {currentAd.ID}");
                    currentAd.UniqueId = similar.ID;
                }
                else
                {
                    //it's unique one, so let's assign it
                    _log.LogMessage($" not found unique for {currentAd.ID}");
                    currentAd.UniqueId = currentAd.ID;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            

            _context.SaveChanges();
        }

        public void Start()
        {
            var diffTime = DateTime.Now;
            diffTime -= TimeSpan.FromHours(12);
            var todayAds = _context.ScrapedAds.Where(ad => ad.ScrapedAt > diffTime);
            _log.LogMessage("found today's ads = " + todayAds.Count());

            var uniqueAds = _context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId).ToList();
            _log.LogMessage("found uniqueAds ads = " + uniqueAds.Count);
            foreach (var scrapedAd in todayAds)
            {
                //check whether some unique ad is equal to scraped today ad
                var similar = uniqueAds.Find(unAd => unAd.IsEqual(scrapedAd, _comparer));
                if (similar != null)
                {
                    //found unique one, so it's the same ad but updated
                    _log.LogMessage($"found unique {similar.ID} for {scrapedAd.ID}");
                    scrapedAd.UniqueId = similar.ID;
                }
                else
                {
                    //it's unique one, so let's assign it
                    _log.LogMessage($" not found unique for {scrapedAd.ID}");
                    scrapedAd.UniqueId = scrapedAd.ID;
                }
            }

            _context.SaveChanges();
        }
    }
}