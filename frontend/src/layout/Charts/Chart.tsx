import React, {Fragment, useContext} from "react";
import {IPrice} from "../../interfaces/IPrice";
import {LineChart,CartesianGrid, Legend, Line, Tooltip, XAxis, YAxis} from "recharts";
import adStore from "../../stores/adStore";

interface IProps {
    inputData: IPrice[];
}

class ChartVals {
    name: string;
    val: number;
    am:number;
    constructor(name:string,val:number,amount:number) {
        this.name = name;
        this.val = val;
        this.am=amount;
    }
}

const Chart: React.FC<IProps> = ({inputData}) => {
    const AdStore = useContext(adStore);

    let ConvertData = function (values: IPrice[]) :object[] {
        return values.map(price =>
        {
          let date = price.priceDate;
          date = date.slice(0,10);
          let value = price.value;
          return new ChartVals(date,value,price.amountOfPrices);
        });

    }

    return <Fragment>
        <h1>График средней цены за день от времени</h1>
        <LineChart width={1200} height={300} data={ConvertData(inputData)}
                   margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <XAxis dataKey="name"/>
            <CartesianGrid strokeDasharray="3 3"/>
            <Tooltip/>
            <Legend />
            <YAxis type="number" domain={[AdStore.chartLowestPrice-10000, AdStore.chartHighestPrice+10000]}/>
            <Line type="monotone" dataKey="val" stroke="#8884d8" activeDot={{r: 8}}/>

        </LineChart>
        <LineChart width={1200} height={300} data={ConvertData(inputData)}
                   margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <XAxis dataKey="name"/>
            <YAxis/>
            <CartesianGrid strokeDasharray="3 3"/>
            <Tooltip/>
            <Legend />

            <Line type="monotone" dataKey="am" stroke="#0084d8" activeDot={{r: 8}}/>
        </LineChart>
        <h1>Количество найденных объявлений за день</h1>
    </Fragment>
}

export default Chart;