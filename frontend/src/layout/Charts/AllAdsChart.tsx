import {Container} from "semantic-ui-react";
import NavBar from "../NavBar/NavBar";

import React, {useContext, useEffect} from "react";
import adStore from "../../stores/adStore";
import LoadingComponent from "../LoadingComponent";
import Chart from "./Chart";
import {observer} from "mobx-react";

const AllAdsChart =() => {
    const AdStore = useContext(adStore);

    useEffect(() => {
        AdStore.loadTotalChartData();
    }, [AdStore]);

    return (
        <Container>
            <NavBar/>
            {
                AdStore.loadingChartData ?

                       <LoadingComponent/> :  <Chart inputData={AdStore.ChartData}/>
            }
        </Container>
    )
};

export default observer(AllAdsChart);