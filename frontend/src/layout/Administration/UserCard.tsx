import {IUserWithCreds_DTO} from "../../interfaces/IUserWithCreds_DTO";
import React, {useContext, useState} from "react";
import {Button, Card, Dropdown, Image} from 'semantic-ui-react'
import {DropdownProps} from "semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown";
import authStore from "../../stores/authStore";
import {CurrentUser} from "../../interfaces/IToken";
import {observer} from "mobx-react";

interface IProps {
    User : IUserWithCreds_DTO;
}

const UserCard: React.FC<IProps> = ({User}) => {

    const UserAsString = (u: string) => {
        if (u === "0") return "SuperAdmin";
        if (u === "1") return "Admin";
        return "User";
    };

    const banOptions = [
        {
            key: '1',
            text: '1 Day',
            value: '1',
        },
        {
            key: '7',
            text: '1 Week',
            value: '7',
        },
        {
            key: '31',
            text: '1 Month',
            value: '31',
        },
        {
            key: '1000',
            text: 'Forever',
            value: '1000',
        },
    ];
    const [banDays,setBanDays] = useState(0);
    const UserStore = useContext(authStore);
    const [changedUserType,SetChangedUserType] = useState(UserAsString(User.type));
    const [changedUserBan,SetChangedUserBan] = React.useState<Date | null>(User.bandatetime);

    function banUser() {
       //call to store method
        console.log("banUser method, selectedBanDays=" + banDays);
        UserStore.banUser(User.uid, banDays);

        let currentDate  = new Date(Date.now()+(1000 * 60 * 60 * banDays));
        SetChangedUserBan(currentDate);
    }

    function unBanUser() {
        //call to store method
        UserStore.banUser(User.uid, 0)
        SetChangedUserBan(null);
    }

    function makeUserAdmin(){
        UserStore.AssignRoleToUser(User.uid,CurrentUser.Admin);
        SetChangedUserType("Admin");
    }

    function makeUserAsUser(){
        UserStore.AssignRoleToUser(User.uid,CurrentUser.User);
        SetChangedUserType("User");
    }

    function handleDropdownChange(e : React.SyntheticEvent<HTMLElement>,data: DropdownProps ) {
        let stringBanDays = data!.value as string;
        setBanDays(+stringBanDays);
    }
    return  <Card>
        <Card.Content>
            <Image
                floated='right'
                size='mini'
                src={User.ava1}
            />
            <Card.Header>{User.name} {User.surname}</Card.Header>
            <Card.Meta>ID = {User.uid}</Card.Meta>
            <Card.Description>
                UserType = {changedUserType}
                {
                    changedUserBan != null ?
                        <br/> +"User is banned till" + User.bandatetime?.getDate() +" "+ User.bandatetime?.getTime() : ""

                }

            </Card.Description>
        </Card.Content>
        <Card.Content extra>

            <div className='ui'>
                <Button basic color='green' onClick={unBanUser} >
                    Unban
                </Button>
                <Button basic color='red' onClick={banUser}>
                    Ban
                </Button>
                <Dropdown
                    placeholder='Select ban duration'
                    fluid
                    selection
                    options={banOptions}
                    onChange={handleDropdownChange}
                />
            </div>

        </Card.Content>

        {
            UserStore.CurrentUserType === CurrentUser.SuperAdmin ?
            <Card.Content extra>
                <div className='ui two buttons'>
                    <Button basic color='green' onClick={makeUserAdmin} >
                        Make Him Admin
                    </Button>
                    <Button basic color='red' onClick={makeUserAsUser}>
                        Make Him User
                    </Button>
                </div>
        </Card.Content> :
                    <div>

                    </div>
        }
    </Card>
}

export default observer(UserCard);
