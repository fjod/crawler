import React, {Fragment, useContext, useEffect, useState} from "react";
import authStore from "../../stores/authStore";
import LoadingComponent from "../LoadingComponent";
import {Button, Card} from 'semantic-ui-react'
import UserCard from "./UserCard";
import {CurrentUser} from "../../interfaces/IToken";
import {Link} from "react-router-dom";
import {observer} from "mobx-react";
import {Container} from "semantic-ui-react";
import NavBar from "../NavBar/NavBar";

const UserList: React.FC = () => {
    const AuthStore = useContext(authStore);
    const [usersLoaded, setUsersLoaded] = useState(false);



    useEffect(() => {

        let userType = AuthStore.CurrentUserType;
        if ((userType === CurrentUser.User) || (userType === null)) return;

        AuthStore.LoadUserList().then(() => setUsersLoaded(true));
    }, [AuthStore]);

    {
        let userType = AuthStore.CurrentUserType; //non-admin users cant be on this page
        if ((userType === CurrentUser.User) || (userType === null))
            return <Fragment>
                <Button fluid={true} basic color={"green"}
                        as={Link} to={`/`}>
                    Home</Button>
            </Fragment>

    } // <NavBar/>
    return <Container>
        <NavBar/>
    <br/><br/><br/><br/>
        <Fragment>
            {
                usersLoaded ?
                    <Card.Group>
                        {
                            AuthStore.UserList.map(c => <UserCard User={c} key ={c.uid}/>)
                        }
                    </Card.Group>
                    :
                    <LoadingComponent content={"Loading users..."}/>
            }
        </Fragment>
    </Container>;

};

export default observer(UserList);