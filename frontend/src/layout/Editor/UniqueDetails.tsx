
import React, { Fragment, useContext, useEffect } from "react";
import { useMediaQuery } from "react-responsive";
import { Grid } from "semantic-ui-react";
import ScrapedAdMin from "../Common/ScrapedAdMin";
import AdPic from "../Common/AdPic";
import EditorStore from "../../stores/editorStore";
import LoadingComponent from "../LoadingComponent";
import { observer } from "mobx-react";
import { RouteComponentProps } from "react-router-dom";

interface DetailParams {
  id:string;
}

const UniqueDetails: React.FC<RouteComponentProps<DetailParams>> = ({match}) => {
  const editorStore = useContext(EditorStore);
  const isDesktopOrLaptop = useMediaQuery({
        query: "(min-device-width: 1224px)"
    });
  useEffect(() => {
    editorStore.loadDetails( +match.params.id);
  }, [editorStore, match]);

  const MarkAsDuplicate = (id: number) => {};

  if (editorStore.loadingInitial)
    return <LoadingComponent content="Loading details" />;
  return (
    <Fragment>
      {isDesktopOrLaptop ? (
        //desktop
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column width={7}>
              <ScrapedAdMin
                id={editorStore.DetailsAd?.ad.id!}
                baseUrl={editorStore.DetailsAd?.ad.baseUrl!}
                scrapedAt={editorStore.DetailsAd?.ad.scrapedAt!}
                uniqueId={editorStore.DetailsAd?.ad.uniqueId!}
                description={editorStore.DetailsAd?.ad.description!}
                freshDate={editorStore.DetailsAd?.ad.freshDate!}
                createDate={editorStore.DetailsAd?.ad.createDate!}
                manufactureYear={editorStore.DetailsAd?.ad.manufactureYear!}
                vin={editorStore.DetailsAd?.ad.vin!}
                price={editorStore.DetailsAd?.ad.price!}
                sellerName={editorStore.DetailsAd?.ad.sellerName!}
                mileage={editorStore.DetailsAd?.ad.mileage!}
                color={editorStore.DetailsAd?.ad.color!}
                city={editorStore.DetailsAd?.ad.city!}
              />
            </Grid.Column>
            <Grid.Column>
              <Grid columns={4}>
                {editorStore.DetailsAd?.smallPics.map(adPic => (
                  <Grid.Column key={adPic.id}>
                    <div key={adPic.id}>
                      <AdPic
                        pic={adPic}
                        showEditorButtons={false}
                        buttonWasClicked={id => MarkAsDuplicate(id)}
                      />
                    </div>
                  </Grid.Column>
                ))}
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      ) : (
        //mobile
        <Grid>
          <Grid.Row>
            <ScrapedAdMin
              id={editorStore.DetailsAd?.ad.id!}
              baseUrl={editorStore.DetailsAd?.ad.baseUrl!}
              scrapedAt={editorStore.DetailsAd?.ad.scrapedAt!}
              uniqueId={editorStore.DetailsAd?.ad.uniqueId!}
              description={editorStore.DetailsAd?.ad.description!}
              freshDate={editorStore.DetailsAd?.ad.freshDate!}
              createDate={editorStore.DetailsAd?.ad.createDate!}
              manufactureYear={editorStore.DetailsAd?.ad.manufactureYear!}
              vin={editorStore.DetailsAd?.ad.vin!}
              price={editorStore.DetailsAd?.ad.price!}
              sellerName={editorStore.DetailsAd?.ad.sellerName!}
              mileage={editorStore.DetailsAd?.ad.mileage!}
              color={editorStore.DetailsAd?.ad.color!}
              city={editorStore.DetailsAd?.ad.city!}
            />
          </Grid.Row>
          <Grid.Row>
            <Grid columns={2}>
              {editorStore.DetailsAd?.smallPics.map(adPic => (
                <Grid.Column key={adPic.id}>
                  <div key={adPic.id}>
                    <AdPic
                      pic={adPic}
                      showEditorButtons={true}
                      buttonWasClicked={id => MarkAsDuplicate(id)}
                    />
                  </div>
                </Grid.Column>
              ))}
            </Grid>
          </Grid.Row>
        </Grid>
      )}
    </Fragment>
  );
};

export default observer(UniqueDetails);
