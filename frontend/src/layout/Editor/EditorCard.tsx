import React from 'react'
import {Button, Card, Feed, Image} from 'semantic-ui-react'
import {ILatestAd} from "../../interfaces/ILatestAd"
import ScrapedAdMin from "../Common/ScrapedAdMin"
import {observer} from "mobx-react";
import {Link} from "react-router-dom";


const EditorCard:React.FC<ILatestAd> = ({current,unique,scrapedLocation}) => (
    <Card fluid={true} style={({marginBottom:20})}>
        <Image src={scrapedLocation} wrapped ui={false} />
        <Card.Content>
            <Card.Header>Объявление в базе</Card.Header>

            <Card.Description>
                <Feed>
                    <Feed.Event>
                        <Feed.Content>
                            <Feed.Label>
                                Текущее объявление
                            </Feed.Label>
                            <Feed.Summary>
                                <ScrapedAdMin id={current.id} baseUrl={current.baseUrl}  scrapedAt={current.scrapedAt}
                                              uniqueId={current.uniqueId} description={current.description}
                                              freshDate={current.freshDate} createDate={current.createDate}
                                              manufactureYear={current.manufactureYear} vin={current.vin}
                                              price={current.price}  sellerName={current.sellerName}
                                              mileage={current.mileage} color={current.color} city={current.city}/>
                            </Feed.Summary>
                        </Feed.Content>
                    </Feed.Event>
                    <Feed.Event>
                        <Feed.Content>
                            <Feed.Label>
                                Уникальное объявление
                            </Feed.Label>
                            <Feed.Summary>
                                <ScrapedAdMin id={unique.id} baseUrl={unique.baseUrl}  scrapedAt={unique.scrapedAt}
                                              uniqueId={unique.uniqueId} description={unique.description}
                                              freshDate={unique.freshDate} createDate={unique.createDate}
                                              manufactureYear={unique.manufactureYear} vin={unique.vin}
                                              price={unique.price}  sellerName={unique.sellerName}
                                              mileage={unique.mileage} color={unique.color} city={unique.city}/>
                            </Feed.Summary>
                        </Feed.Content>
                    </Feed.Event>
                </Feed>
                <Button fluid={true} basic color={"green"}
                    as = {Link} to ={`/editor/details/${unique.id}`}
                >Details</Button>
            </Card.Description>

        </Card.Content>

    </Card>
);

export default observer(EditorCard)