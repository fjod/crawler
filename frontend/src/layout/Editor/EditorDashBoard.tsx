import React, { useContext, useEffect } from "react";
import EditorStore from "../../stores/editorStore";
import LoadingComponent from "../LoadingComponent";

import EditorCard from "./EditorCard";
import {Button, Container, Grid, Item} from "semantic-ui-react";
import { observer } from "mobx-react";
import { useMediaQuery } from "react-responsive";

const EditorDashBoard: React.FC = () => {
  const editorStore = useContext(EditorStore);
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-device-width: 1224px)"
  });

  useEffect(() => {
    editorStore.loadLatestAds();
  }, [editorStore]);


  if (editorStore.loadingInitial)
    return <LoadingComponent content="Загрузка" />;
  return (
    <Container fluid={true}>
        <Grid columns={3}>
        <Grid.Row>
          <Grid.Column width={4}>
            {" "}
            <Button
              disabled={editorStore.shift === 0}
              onClick={() => {
                editorStore.shift -= 10;
                if (editorStore.shift < 0) editorStore.shift = 0;
                editorStore.LatestAds = [];
                editorStore.loadLatestAds(editorStore.shift);
              }}
            >
              {" "}
              Назад{" "}
            </Button>{" "}
          </Grid.Column>
          <Grid.Column width={8}>
            {" "}
            <h1>Объявления</h1>
          </Grid.Column>
          <Grid.Column width={4}>
            {" "}
            <Button
              onClick={() => {
                editorStore.shift += 10;
                editorStore.LatestAds = [];
                editorStore.loadLatestAds(editorStore.shift);
              }}
            >
              {" "}
              Вперед{" "}
            </Button>{" "}
          </Grid.Column>
        </Grid.Row>
        </Grid>
            <Grid.Row>
      {isDesktopOrLaptop ? (
        <Grid columns={5}>
          {editorStore.LatestAds.map(ad => (
            <Grid.Column key={ad.current.id}>
              <div key={ad.current.id}>
                <EditorCard
                  current={ad.current}
                  unique={ad.unique}
                  scrapedLocation={ad.scrapedLocation}
                  fileLocation={ad.fileLocation}
                />
              </div>
            </Grid.Column>
          ))}
        </Grid>
      ) : (
        <Item.Group divided>
          <h1>Mobile</h1>
          {editorStore.LatestAds.map(ad => (
            <div key={ad.current.id}>
              <EditorCard
                current={ad.current}
                unique={ad.unique}
                scrapedLocation={ad.scrapedLocation}
                fileLocation={ad.fileLocation}
              />
            </div>
          ))}
        </Item.Group>
      )}
            </Grid.Row>

    </Container>
  );
};

export default observer(EditorDashBoard);
