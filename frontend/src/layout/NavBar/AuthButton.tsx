import React, {useContext, Fragment} from 'react';

import {observer} from "mobx-react";
import AuthStore from "../../stores/authStore";
import {Button, Grid, Label} from "semantic-ui-react";

const AuthForm: React.FC = () => {
    const authStore = useContext(AuthStore);
    const redirect = () => {


            localStorage.setItem('authWasStarted', "true");

            //window.location.href = 'http://localhost:5000/Home';
            window.location.href = process.env.REACT_APP_AUTH_URL as string;



        return null;
    };
    return (
        <Fragment>
            <Grid>
                {
                    authStore.isLoggedIn ?
                        <Label color={"green"} size={"big"}
                               content={"Привет, " + authStore.UserInfo?.user?.name + " "}/>
                        :
                        ""
                }
                {authStore.isLoggedIn ?
                    <Button color={"red"} onClick={() => {

                        localStorage.removeItem('authWasStarted');
                       // window.location.href = 'http://localhost:5000/Home/signout';
                        window.location.href = process.env.REACT_APP_SIGNOUT_URL as string;
                        authStore.logout();
                    }
                    } content={"Выйти"}/>
                    :
                    <Button color={"green"} onClick={() => redirect()} content={"Войти"}/>}
            </Grid>
        </Fragment>

    )
};
export default observer(AuthForm);

