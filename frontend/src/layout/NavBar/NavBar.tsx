import React, {useContext, useEffect, Fragment} from "react";
import {Button, Dropdown,  Image, Menu} from "semantic-ui-react";
import AuthButton from "./AuthButton";
import AuthStore from "../../stores/authStore";
import {observer} from "mobx-react";
import Cookies from 'universal-cookie';
import {Link} from "react-router-dom";
import {CurrentUser} from "../../interfaces/IToken";
import {DropdownProps} from "semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown";
import adStore from "../../stores/adStore";
import {SortAds} from "../../interfaces/IScrapedAd";


const NavBar: React.FC = () => {
    const authStore = useContext(AuthStore);
    const cookies = new Cookies();
    const AdStore = useContext(adStore);

    useEffect(() => {
        const authWasStarted = localStorage.getItem('authWasStarted');
        if (!authWasStarted) return;
        const cookiesValue = cookies.get('fb_id');
        if (cookiesValue != null) {
            let valToDelete=cookies.get('fb_id').toString();
            authStore.Register(cookies.get('fb_id').toString());
            cookies.remove('fb_id');
            localStorage.removeItem('authWasStarted');
            localStorage.removeItem(valToDelete);
        }

    }, [authStore, cookies]);

    const sortOptions = [
        {
            key: '1',
            text: 'Sort By Date',
            value: '1',
        },
        {
            key: '2',
            text: 'Sort By Rating',
            value: '2',
        },
        {
            key: '3',
            text: 'Look at Not Rated',
            value: '3',
        },
        {
            key: '4',
            text: 'Go To Homepage',
            value: '4',
            as : Link,
            to : '/'
        },
        {
            key: '5',
            text: 'Look at Chart',
            value: '5',
            as : Link,
            to : '/chart'
        }
    ];
    function handleDropdownChange(e : React.SyntheticEvent<HTMLElement>,data: DropdownProps ) {
        let sortCriteria = data!.value as string;
        if (sortCriteria === "1") AdStore.changeSorting(SortAds.ByDate);
        if (sortCriteria === "2") AdStore.changeSorting(SortAds.ByRating);
        if (sortCriteria === "3") AdStore.changeSorting(SortAds.NotRated);
    }


    return (
        <Fragment>
        <Menu fixed="top" inverted size={"mini"}>
            <Menu.Item position={"left"}>
                <Dropdown item text='Menu' onChange={handleDropdownChange}  options={sortOptions} >
                </Dropdown>
            </Menu.Item>

            <Menu.Menu position={"right"} size={"mini"}>
                <Menu.Item>
                    <AuthButton/>
                </Menu.Item>

                {
                    authStore.isLoggedIn && authStore.UserInfo?.user !== undefined  ?
                    <Menu.Item>
                             <Image src={authStore.UserInfo.user.ava1}/>
                    </Menu.Item> : ""
                }
            </Menu.Menu>

        </Menu>
            {
                (authStore.CurrentUserType === CurrentUser.Admin ||
                    authStore.CurrentUserType === CurrentUser.SuperAdmin)
                    ?
                    <div>
                        <br/><br/><br/>
                        <Button as={Link}   to={"/adminPanel"}> AdminPanel </Button>
                    </div>
                    : <h1> </h1>
            }
        </Fragment>

    );
};
export default observer(NavBar);

