import {IAdPic} from "../../interfaces/IAdPic";
import React, { Fragment } from "react";
import {Button, Card} from "semantic-ui-react";
import ImageComponent from "./ImageSmallToBig";

interface IProps {
  pic : IAdPic;
  showEditorButtons: boolean;
  buttonWasClicked: (id:number) => (void);
}

const AdPic : React.FC<IProps> =({pic,showEditorButtons,  buttonWasClicked}) =>{
    return <Fragment>
        <Card fluid>
            <Card.Content>
                <ImageComponent key={pic.id} image={pic}/>
            </Card.Content>
            {showEditorButtons?
                (<Card.Content extra>
                    <Button basic  color='green' onClick={()=>buttonWasClicked(pic.id)}>
                        Duplicate
                    </Button>
            </Card.Content>):""}
        </Card>
    </Fragment>
};

export default AdPic;