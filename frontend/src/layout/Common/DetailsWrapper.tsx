import React, {Fragment, useEffect, useState} from "react";
import {observer} from "mobx-react";
import {IScrapedAd} from "../../interfaces/IScrapedAd";
import LoadingComponent from "../LoadingComponent";
import ScrapedAdDetails from "./ScrapedAdDetails";
import agent from "../../api/agent";
import {RouteComponentProps} from "react-router-dom";

interface IProps {
    id:string
}



const DetailsWrapper: React.FC<RouteComponentProps<IProps>> = ({match}) =>{
    const [scrapedAd,setScrapedAd] = React.useState<IScrapedAd>();
    const [adLoaded,setAdLoaded] = useState(false);
    useEffect(()=>{
          agent.ScrapedAd.details(match.params.id).then(_ad => {
              setScrapedAd(_ad);
              setAdLoaded(true);
          });

    },[match.params.id,setScrapedAd,setAdLoaded]);
    return <Fragment>
        {
            adLoaded?
                <ScrapedAdDetails ad={scrapedAd!} />
                :
                <LoadingComponent content={"loading details.."}/>
        }
    </Fragment>
};

export  default observer(DetailsWrapper);