import React, {useContext, useEffect, useState, Fragment} from "react";
import {IScrapedAdPreview} from "../../interfaces/IScrapedAdPreview";
import adStore from "../../stores/adStore";
import {IAdPic} from "../../interfaces/IAdPic";
import {Grid, Card, Button,  CardHeader} from "semantic-ui-react";
import LoadingComponent from "../LoadingComponent";
import {observer} from "mobx-react";
import agent from "../../api/agent";
import {Link} from "react-router-dom";
import authStore from "../../stores/authStore";
import {useMediaQuery} from "react-responsive";

const ScrapedAdPreview: React.FC<IScrapedAdPreview> = ({
                                                           rating,
                                                           commentsCount,
                                                           manufactureYear,
                                                           price,
                                                           mileage,
                                                           city,
                                                           uniqueId
                                                       }) => {
    const [loadingPics, setLoadingPics] = useState(true);
    const [Pics, setPics] = React.useState<IAdPic[] | null>(null);
    const AdStore = useContext(adStore);
    const _auth = useContext(authStore);
    const isDesktopOrMobile = useMediaQuery({
        query: "(min-device-width: 1224px)"
    });

    const extra = `Рейтинг - ${rating}/10. Год выпуска ${manufactureYear}, пробег ${mileage} км, цена ${price} рублей, ${commentsCount} комментариев`

    useEffect(() => {
        setLoadingPics(true);
        loadPictures(uniqueId).then(() =>
            setLoadingPics(false)
        );
    }, [AdStore, uniqueId]);

    const loadPictures = async (uniqueId: number) => {
        let pics: IAdPic[] = [];
        await agent.ScrapedAd.ImagesForUniqueAd(uniqueId).then(pp =>
            pp.forEach(z => pics.push(z))
        );
        setPics(pics);
        setLoadingPics(false);
    };
    let i = 0;

    return (
        <Fragment>
            {isDesktopOrMobile ?
            <Grid celled>
                <Grid.Row>
                    <Grid.Column width={3} textAlign={"left"}>
                        <Grid reversed={"computer vertically"}>
                            <Grid.Row>
                                <Grid.Column> Year: {manufactureYear} </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column> Mileage: {mileage} </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column> City: {city} </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column> Price: {price} </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column> Rating: {rating} </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column> Comments: {commentsCount} </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                    <Grid.Column width={13}>
                        {loadingPics ?
                            <LoadingComponent content="Loading pics.."/> :
                            <Card.Group itemsPerRow={4}>
                                {

                                    Pics?.map(pic => {

                                    return <Card color='black' image={pic.url} key={pic.url+i++}/>
                                })}
                            </Card.Group>
                        }
                    </Grid.Column>
                </Grid.Row>
                {_auth.isLoggedIn ?
                    <Button fluid={true} basic color={"green"}
                            as={Link} to={`/Ad/details/${uniqueId}`}
                    >Details</Button>
                    : ""}
            </Grid>
                :
                <Grid>
                    <Grid.Row>

                        {loadingPics ?
                            <LoadingComponent content="Loading pics.."/> :
                            <div>
                                <CardHeader as='h5' content={extra}>
                                </CardHeader>
                            <Card.Group itemsPerRow={3} description="5454">
                                {Pics?.map(pic => {

                                    return  <Card color='black' image={pic.url}  key={pic.url+i++}
                                    />
                                })}
                            </Card.Group>

                            </div>
                        }
                    </Grid.Row>
                    {_auth.isLoggedIn ?
                        <Button fluid={true} basic color={"green"}
                                as={Link} to={`/Ad/details/${uniqueId}`}
                        >Details</Button>
                        : ""}
                </Grid>
                }



        </Fragment>
    )

};

export default observer(ScrapedAdPreview);
