import React, { useContext, useEffect} from "react";
import {useMediaQuery} from "react-responsive";
import adStore from "../../stores/adStore";
import LoadingComponent from "../LoadingComponent";
import {Button, Container,  Grid, Item, Segment} from "semantic-ui-react";
import ScrapedAdPreview from "./ScrapedAdPreview";
import {observer} from "mobx-react";


const ScrapedAdList: React.FC = () => {
    const AdStore = useContext(adStore);
    const isDesktopOrLaptop = useMediaQuery({
        query: "(min-device-width: 1224px)"
    });

    useEffect(() => {
        AdStore.loadLatestAds();
    }, [AdStore]);

    if (AdStore.loadingInitial)
        return <LoadingComponent content="loading your ads" />;
    return (
        <Container fluid={true}>
            <Grid columns={3}>
                <Grid.Row>
                    <Grid.Column width={3}>
                        {" "}
                        <Button
                            disabled={AdStore.shift === 0}
                            onClick={() => {
                                AdStore.shift -= 4;
                                if (AdStore.shift < 0) AdStore.shift = 0;
                                AdStore.LatestAds = [];
                                AdStore.loadLatestAds(AdStore.shift);
                            }}
                        >
                            {" "}
                            Назад{" "}
                        </Button>{" "}
                    </Grid.Column>
                    <Grid.Column width={1}>  </Grid.Column>
                    <Grid.Column width={5}>

                        <h3> Объявления </h3>
                    </Grid.Column>
                    <Grid.Column width={1}>  </Grid.Column>
                    <Grid.Column width={3}>
                        {" "}
                        <Button
                            onClick={() => {
                                AdStore.shift += 4;
                                AdStore.LatestAds = [];
                                AdStore.loadLatestAds(AdStore.shift);
                            }}
                        >
                            Вперед
                        </Button>
                    </Grid.Column>
                </Grid.Row>

            </Grid>
            <br/>

            <Grid.Row>
                {isDesktopOrLaptop ? (
                    <Grid columns={2}>
                        {AdStore.LatestAds.map(ad => (
                            <Grid.Column key={ad.uniqueId}>
                                <div key={ad.uniqueId}>
                                   <ScrapedAdPreview city={ad.city} commentsCount={ad.commentsCount} manufactureYear={ad.manufactureYear}
                                   mileage={ad.mileage} price={ad.price} rating={ad.rating} uniqueId={ad.uniqueId}
                                   />
                                </div>
                            </Grid.Column>
                        ))}
                    </Grid>
                ) : (
                    <Item.Group divided>
                        {AdStore.LatestAds.map(ad => (
                            <div key={ad.uniqueId}>
                                <Segment key={ad.uniqueId}>
                                    <ScrapedAdPreview city={ad.city} commentsCount={ad.commentsCount} manufactureYear={ad.manufactureYear}
                                                      mileage={ad.mileage} price={ad.price} rating={ad.rating} uniqueId={ad.uniqueId}
                                    />
                                </Segment>
                                <br/>
                            </div>
                        ))}
                    </Item.Group>
                )}
            </Grid.Row>

        </Container>
    );
};

export default observer(ScrapedAdList);