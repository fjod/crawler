import React, {useContext, useEffect, useState, Fragment} from "react";
import adStore from "../../stores/adStore";
import {IAdPic} from "../../interfaces/IAdPic";
import {Card} from "semantic-ui-react";
import LoadingComponent from "../LoadingComponent";
import {observer} from "mobx-react";
import agent from "../../api/agent";
import AdPic from "./AdPic";

interface IProps {
    adUniqueId: number;
}

const ScrapedAdSmallPics: React.FC<IProps> = ({adUniqueId}) => {
    const [loadingPics, setLoadingPics] = useState(true);
    const [Pics, setPics] = React.useState<IAdPic[] | null>(null);
    const AdStore = useContext(adStore);
    useEffect(() => {
        setLoadingPics(true);
        loadPictures(adUniqueId).then(() =>
            setLoadingPics(false)
        );
    }, [AdStore, adUniqueId]);

    let i = 0;
    const loadPictures = async (uniqueId: number) => {
        let pics: IAdPic[] = [];
        await agent.ScrapedAd.ImagesForUniqueAd(uniqueId).then(pp =>
            pp.forEach(z => pics.push(z))
        );
        setPics(pics);
        setLoadingPics(false);
    };
    return (
        <Fragment>
            {loadingPics ?
                <LoadingComponent content="Loading pics.."/> :
                <Card.Group itemsPerRow={3}>
                    {Pics?.map(pic => {
                        return <AdPic pic={pic} key={pic.url + i++} showEditorButtons={false} buttonWasClicked={() => {
                        }}/>
                    })}
                </Card.Group>
            }
        </Fragment>
    )
};

export default observer(ScrapedAdSmallPics);