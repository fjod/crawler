import {IScrapedAd} from "../../interfaces/IScrapedAd";
import React, {Fragment, useContext, useEffect, useState} from "react";
import {observer} from "mobx-react";
import {ISetRating} from "../../interfaces/ISetRating";
import agent from "../../api/agent";
import {Segment} from "semantic-ui-react";
import ScrapedAdSmallPics from "./ScrapedAdSmallPics";
import ReactStars from "react-rating-stars-component";
import ListComments from "../Comments/List";
import adStore from "../../stores/adStore";
import LoadingComponent from "../LoadingComponent";
import Chart from "../Charts/Chart";

interface IProps {
    ad:IScrapedAd
}

const ScrapedAdDetailsMobile:React.FC<IProps>=({ad}) => {

    const [ratingWasLoaded,setRatingWasLoaded] = useState(false);
    const [rating,setRating] = React.useState<number|string>("");
    const [userSetRating,setUserSetRating] = useState(false);
    const [totalVotes,setTotalVotes] = useState(0);
    const AdStore = useContext(adStore);

    const sendNewRating = (value:number)=>{
        let foo : ISetRating = {advUniqueId : ad.uniqueId, value : value};
        agent.ScrapedAd.setRating(foo).then(()=>setUserSetRating(true));
    }

    useEffect(()=>{

        AdStore.loadTotalChartDataForAd(ad.uniqueId.toString());
        agent.ScrapedAd.getRating(ad.uniqueId).then(r => {
            if (r.totalVotes >0)
            {
                setRating(r.currentRating);
                setTotalVotes(r.totalVotes);
            }
            console.log(r.totalVotes + "got rating" + r.currentRating);
            setRatingWasLoaded(true);
        });
    },[ad.uniqueId,AdStore]);


return <Fragment>
    <Segment>
    <h5> Объявление создано {ad.createDate.toString().substring(0,10)}</h5>
    <h5> Цена {ad.price} рублей,  {ad.manufactureYear} год выпуска, пробег {ad.mileage}  </h5>
    <h5> Продает {ad.sellerName} из города {ad.city} </h5>

    <h5>Описание:</h5>
    <Segment style={{flexDirection:'row', flex: 1, flexWrap: 'wrap',flexShrink: 1, wordWrap:'break-word'}}>
        {ad.description}
    </Segment>

    <a href={ad.baseUrl}>Оригинальное объявление</a>
    </Segment>
    <ScrapedAdSmallPics adUniqueId={ad.uniqueId}/>
    <Segment>
        {
            ratingWasLoaded ?

                        <ReactStars
                            count={10}
                            size={24}
                            color2={'#ffd700'}
                            half={false}
                            value={rating}
                            onChange={(value:number) => sendNewRating(value)}
                        /> : " "

        }
        {
            ratingWasLoaded?
            <h5>Всего голосов : {totalVotes}</h5> : ""
        }
        {
            userSetRating ? <h6>Ваш голос учтен!</h6> :""
        }
    </Segment>
    <Segment>
        <ListComments AdId={ad.id}/>
    </Segment>
    <Segment>
        {
            AdStore.loadingChartData ?
                <LoadingComponent/> :  <Chart inputData={AdStore.ChartData}/>
        }
    </Segment>

</Fragment>
}

export default observer(ScrapedAdDetailsMobile);