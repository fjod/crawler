import {IScrapedAd} from "../../interfaces/IScrapedAd";
import React, {Fragment} from "react";
import {observer} from "mobx-react";
import {useMediaQuery} from "react-responsive";
import ScrapedAdDetailsMobile from "./ScrapedAdDetailsMobile";
import ScrapedAdDetailsPc from "./ScrapedAdDetailsPc";
import NavBar from "../NavBar/NavBar";


interface IProps {
    ad: IScrapedAd
}

const ScrapedAdDetails: React.FC<IProps> = ({ad}) => {


    const isDesktopOrLaptop = useMediaQuery({
        query: "(min-device-width: 1224px)"
    });

    return <Fragment>
        <NavBar/>
        {
            isDesktopOrLaptop ?
                <ScrapedAdDetailsPc ad={ad}/>
                : <ScrapedAdDetailsMobile ad={ad}/>
        }
    </Fragment>
};

export default observer(ScrapedAdDetails);