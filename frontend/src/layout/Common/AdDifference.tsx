import {IAdHistory} from "../../interfaces/IDifferences";
import React, {Fragment, useState} from "react";
import {observer} from "mobx-react";
import {Accordion, Card, Icon} from "semantic-ui-react";
import {AccordionTitleProps} from "semantic-ui-react/dist/commonjs/modules/Accordion/AccordionTitle";
import {IAdPic} from "../../interfaces/IAdPic";
import LoadingComponent from "../LoadingComponent";
import agent from "../../api/agent";

interface IProps {
    Diffs : IAdHistory
}


const TwoAdsHistory : React.FC<IProps> = ({Diffs}) =>{
    const [currentIndex, setCurrentIndex] =  useState(0); //accordion expander counter
    const[smallPicsDiff, setSmallPicsDiff] = React.useState<IAdPic[]>([]);
    const[picsWereLoaded, setPicsWereLoaded] = useState(false);

    let diffsCounter : number = -1;

    const handleClick = (e:React.MouseEvent<HTMLDivElement> , data: AccordionTitleProps)=>{
        const index = data.index;
        const newIndex = currentIndex === index ? -1 : index;
        setCurrentIndex(+newIndex!);
    };

    const handleImageAccordionClick = async (e: React.MouseEvent<HTMLDivElement>, data: AccordionTitleProps) => {
        const index = data.index;
        const newIndex = currentIndex === index ? -1 : index;
        setCurrentIndex(+newIndex!);

        let pics: IAdPic[] = [];
        await agent.ScrapedAd.picsDifference(Diffs.prevId, Diffs.nextId).then(pp =>
            pp.forEach(z => pics.push(z))
        );
        setSmallPicsDiff(pics);
        setPicsWereLoaded(true);
    };

    return <Fragment>
        <h5>First ad was scraped at {Diffs.prevScrapedAt}</h5>
        <br/>
        <h5>Next ad was scraped at {Diffs.nextScrapedAt} </h5>
        <Card>
        <Accordion>
            {

                Diffs.differences.length ===0 ?
                    <h5> No text differences... </h5>
                    :

                Diffs.differences.map(diff =>{
                    diffsCounter++;
                    return <div> <Accordion.Title
                        index={diffsCounter}
                        active={currentIndex === diffsCounter}
                        onClick={handleClick}
                    >
                        <Icon name={'dropdown'}/>
                        Check differences between {diff.fieldName}
                    </Accordion.Title>
                    <Accordion.Content active={currentIndex === diffsCounter}>
                        <h5> New: {diff.newFieldValue}</h5>
                        <h5> Old: {diff.prevFieldValue}</h5>
                    </Accordion.Content>
                    </div>
                })
            }

            <Accordion.Title
                index={diffsCounter}
                active={currentIndex === diffsCounter}
                onClick={handleImageAccordionClick}
            >
                <Icon name={'dropdown'}/>
                Image differences:
            </Accordion.Title>
            <Accordion.Content active={currentIndex === diffsCounter}>
                {
                    !picsWereLoaded  ? <LoadingComponent content={"loading pics diff.."}/>
                    : <div>

                            {
                                (smallPicsDiff.length === 0) ?
                                <h5> No difference in pics </h5>
                                :
                                <Card.Group itemsPerRow={2} >
                                {smallPicsDiff?.map(pic => {
                                    return <Card color='black' image={pic.url} key={pic.url}/>
                                })}
                                </Card.Group>
                            }
                    </div>
                }
            </Accordion.Content>
        </Accordion>
        </Card>
    </Fragment>
};

export  default observer(TwoAdsHistory);