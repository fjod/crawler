import {IScrapedAd} from "../../interfaces/IScrapedAd";
import React, {Fragment, useContext, useEffect, useState} from "react";
import {ISetRating} from "../../interfaces/ISetRating";
import agent from "../../api/agent";
import {Grid} from "semantic-ui-react";
import ScrapedAdMin from "./ScrapedAdMin";
import ReactStars from "react-rating-stars-component";

import ScrapedAdSmallPics from "./ScrapedAdSmallPics";
import ListComments from "../Comments/List";
import {observer} from "mobx-react";
import adStore from "../../stores/adStore";
import LoadingComponent from "../LoadingComponent";
import Chart from "../Charts/Chart";

interface IProps {
    ad:IScrapedAd
}

const ScrapedAdDetailsPc:React.FC<IProps>=({ad}) => {

    const [ratingWasLoaded,setRatingWasLoaded] = useState(false);
    const [rating,setRating] = React.useState<number>(0);
    const [userSetRating,setUserSetRating] = useState(false);
    const [totalVotes,setTotalVotes] = useState(0);
    const AdStore = useContext(adStore);

    const sendNewRating = (value:number)=>{
        let foo : ISetRating = {advUniqueId : ad.uniqueId, value : value};
        agent.ScrapedAd.setRating(foo).then(()=> setUserSetRating(true));
    }

    useEffect(()=>{
        AdStore.loadTotalChartDataForAd(ad.uniqueId.toString());
        agent.ScrapedAd.getRating(ad.uniqueId).then(r => {
            if (r.totalVotes >0)
            {
                setRating(r.currentRating);
                setTotalVotes(r.totalVotes);
            }
            console.log(r.totalVotes + "got rating" + r.currentRating);
            setRatingWasLoaded(true);

        });
    },[ad.uniqueId, AdStore]);

    return <Fragment>

        <div> <br/><br/><br/><br/><br/><br/> </div>


        <Grid columns={2} divided stretched>
            <Grid.Row>
                <Grid.Column>
                    <ScrapedAdMin uniqueId={ad.uniqueId} price={ad.price} mileage={ad.mileage} manufactureYear={ad.manufactureYear}
                                  city={ad.city} color={ad.color} baseUrl={ad.baseUrl} createDate={ad.createDate} description={ad.description}
                                  freshDate={ad.freshDate} id={ad.id} scrapedAt={ad.scrapedAt} sellerName={ad.sellerName} vin={ad.vin}
                    />
                    <br/>
                    <br/>
                    <br/>
                    {
                        ratingWasLoaded ?

                            <ReactStars
                                count={10}
                                size={24}
                                color2={'#ffd700'}
                                half={false}
                                value={rating}
                                onChange={(value:number) => sendNewRating(value)}
                            /> : " "

                    }
                    {
                        ratingWasLoaded?
                            <h5>Всего голосов : {totalVotes}</h5> : ""
                    }
                    {
                        userSetRating ? <h6>Ваш голос учтен!</h6> :""
                    }

                </Grid.Column>

                <Grid.Column>
                    <ScrapedAdSmallPics adUniqueId={ad.uniqueId}/>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row style={{ padding: '10rem' }}>
                <ListComments AdId={ad.id}/>
            </Grid.Row>
            <br/>

        </Grid>
        {
            AdStore.loadingChartData ?
                <LoadingComponent/> :  <Chart inputData={AdStore.ChartData}/>
        }

    </Fragment>
}

export default observer(ScrapedAdDetailsPc);