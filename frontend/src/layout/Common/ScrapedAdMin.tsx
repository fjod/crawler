
import React from "react";
import { Grid, Label } from "semantic-ui-react";
import { IScrapedAd } from "../../interfaces/IScrapedAd";
import {observer} from "mobx-react";

function hexToRgb(hex: string) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  if (result === null) return "rgb(0,0,0)";
  let r = parseInt(result[1], 16);
  let g = parseInt(result[2], 16);
  let b = parseInt(result[3], 16);

  return `rgb(${r},${g},${b})`;
}


const ScrapedAdMin: React.FC<IScrapedAd> = ({
  baseUrl,
  city,
  color,
  createDate,
  description,
  freshDate,
  id,
  manufactureYear,
  mileage,
  price,
  scrapedAt,
  sellerName,
  uniqueId,
  vin
}) => (

  <Grid columns={3}>

    <Grid.Row>
      <Grid.Column>
        <a href={baseUrl}>Ссылка на авто.ру</a>
      </Grid.Column>
      <Grid.Column>
        <Label size={"large"}>{city}</Label>
      </Grid.Column>
      <Grid.Column>
        <Label size={"large"}
          style={{ backgroundColor: hexToRgb(color), borderRadius: 0 }}
        >{" Указанный цвет  "}
        </Label>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column width={"5"}>Создан {createDate.toString().substring(0,10)} , <br/>
      Обновлен {freshDate.toString().substring(0,10)}</Grid.Column>
      <Grid.Column width={"11"}> <h4> {description} </h4></Grid.Column>

    </Grid.Row>
    <Grid.Row>

      <Grid.Column>
       <h3>  Год {manufactureYear}</h3>
      </Grid.Column>
      <Grid.Column>
      <h3> Пробег {mileage} </h3>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column> <h1> Цена {price} </h1></Grid.Column>
      <Grid.Column> <h5> Сохранено в БД: {scrapedAt.toString().substring(0,10) } </h5></Grid.Column>
      <Grid.Column> <h5> Продавец {sellerName} </h5></Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column>Вин {vin}</Grid.Column>
    </Grid.Row>
  </Grid>
);

export default observer(ScrapedAdMin);
