import React, { useContext, useState, Fragment } from "react";
import { Image } from "semantic-ui-react";
import EditorStore from "../../stores/editorStore";
import { IAdPic } from "../../interfaces/IAdPic";
import { observer } from "mobx-react";
import LoadingComponent from "../LoadingComponent";
import ReactModal from "react-modal";

interface IProps {
  image: IAdPic;
}

const ImageComponent: React.FC<IProps> = ({ image }) => {

    const customStyles = {
        content : {
            top                   : '50%',
            left                  : '50%',
            right                 : 'auto',
            bottom                : 'auto',
            marginRight           : '-50%',
            transform             : 'translate(-50%, -50%)'
        }
    };

  let BigImageWasLoaded: boolean = false;

  const [isSmallImage, setState] = useState(true);
  const handleShowDialog = () => {
    if (!BigImageWasLoaded) {
      editorStore.loadBigImage(image.id);
      BigImageWasLoaded = true;
    }
    setState(!isSmallImage);
  };
  const editorStore = useContext(EditorStore);

  return (
    <Fragment>
      {isSmallImage ? (
        <div>
          <Image
            className="medium"
            src={image.url}
            onClick={() => handleShowDialog()}
          />
        </div>
      ) : (
        <ReactModal isOpen={!isSmallImage} contentLabel="Big Picture" style={customStyles} >
          {editorStore.loadBigAd ? (
            <LoadingComponent content={"load big pic"}> </LoadingComponent>
          ) : (
            ""
          )}
          {editorStore.loadBigAd ? (
            ""
          ) : (
            <Image size="massive" src={editorStore.BigImage?.url} onClick={() => handleShowDialog()} />
          )}
        </ReactModal>
      )}
    </Fragment>
  );};

export default observer(ImageComponent);
