import React, { Fragment, useContext, useState} from "react";
import {Button, Form, TextArea, Grid} from "semantic-ui-react";
import {IAddComment} from "../../interfaces/IComments";
import CommentStore from "../../stores/commentStore";

interface IProps {
    AdId: number;
    ParentCommentId: number | null;
    CallBack: () => void;
}

const AddComment: React.FC<IProps> = ({AdId, ParentCommentId, CallBack}) => {
    const commentStore = useContext(CommentStore);
    const [commentText,SetCommentText] = useState("");

    function AddComment(values: any) {
        let foo: IAddComment = {AdId: AdId, ParentId: ParentCommentId, Text: commentText};
        commentStore.AddComment(foo).then();
        CallBack();
    }

    function handleChange(event : React.FormEvent<HTMLTextAreaElement> ) {
        SetCommentText( event.currentTarget.value);
    }
    return <Fragment>

        <Form onSubmit={AddComment}>
            <Grid>
                <Grid.Row>
                   <h3 style={{ padding: '1rem' }}> Добавьте комментарий.. </h3>
                </Grid.Row>
                <Grid.Row>


                    <TextArea placeholder={"Ваш комментарий"} name={"text"} onChange={handleChange}/>

                </Grid.Row>
                <Grid.Row>
                    <Button type="submit" color={"green"} fluid>
                        Отправить комментарий
                    </Button>
                </Grid.Row>
            </Grid>
        </Form>

    </Fragment>
}

export default AddComment;

