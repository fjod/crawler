import {IComment, IEditComment} from "../../interfaces/IComments";
import React, {Fragment, useContext, useState} from "react";
import {Button, ButtonGroup, Card, Confirm, Divider, Form, Image, TextArea} from "semantic-ui-react";
import AuthStore from "../../stores/authStore";
import CommentStore from "../../stores/commentStore";
import AddComment from "./AddComment";

interface IProps {
    Comment: IComment;
}

const SingleComment: React.FC<IProps> = ({Comment}) => {
    const authStore = useContext(AuthStore);
    const commentStore = useContext(CommentStore);
    const [editMode, setEditMode] = useState(false);

    const [deleteConfirm, setDeleteConfirm] = useState(false);
    const [addCommentForComment, setAddCommentForComment] = useState(false);
    const [commentText, SetCommentText] = useState(Comment.text);

    function checkUser() {
        if (authStore === null) return false;
        if (authStore.UserInfo === null) return false;
        return (Comment.vkUser.uid === authStore.UserInfo.user.uid);
    }

    function editButtonClick() {
        setEditMode(!editMode);
    }

    function EditComment(values: any) {
        let foo: IEditComment = {Id: Comment.id, Text: commentText};
        commentStore.EditComment(foo).then(() => {
            Comment.text = commentText;
            editButtonClick();
        });
    }

    function DeleteComment() {
        Comment.text = "";
        Comment.vkUser = {surname: "deleted", name: "deleted", ava1: "", uid: ""};
        commentStore.UserDeleteComment(Comment.id).then(() => {

        });
    }

    function handleChange(event: React.FormEvent<HTMLTextAreaElement>) {
        SetCommentText(event.currentTarget.value);
    }

    return <Fragment>
        {
            editMode ?

                <Form onSubmit={EditComment}>
                    <TextArea value={commentText} rows={3} onChange={handleChange}/>
                    <ButtonGroup>
                        <Button color={"green"} type="submit" basic>
                            OK
                        </Button>
                        <Button color={"blue"} basic onClick={() => editButtonClick()}>
                            Cancel
                        </Button>
                    </ButtonGroup>
                </Form>

                :

                <Card>
                    <Card.Content style={{flexDirection:'row', flex: 1, flexWrap: 'wrap',flexShrink: 1, wordWrap:'break-word'}}
                                  description={Comment.text}/>
                    <Card.Content extra>
                        <Image src={Comment.vkUser.ava1} size={"mini"} floated={"left"}/>  {Comment.vkUser.name}  {Comment.vkUser.surname}
                        <Divider/>
                        {
                            checkUser() ?
                                //it's his comment
                                <ButtonGroup fluid>
                                    <Button size={"small"} basic color={"blue"} onClick={() => editButtonClick()}>
                                        Редактировать
                                    </Button>
                                    <div>
                                        <Button size={"small"} basic color={"red"}
                                                onClick={() => setDeleteConfirm(true)}>
                                            Удалить
                                        </Button>
                                        <Confirm
                                            open={deleteConfirm}
                                            onCancel={() => setDeleteConfirm(false)}
                                            onConfirm={() => {
                                                setDeleteConfirm(false);
                                                DeleteComment();
                                            }}
                                        />
                                    </div>
                                </ButtonGroup>

                                :
                                //other person commented
                                <Button color={"blue"} basic
                                        onClick={() => setAddCommentForComment(!addCommentForComment)}>
                                    Ответить на этот коммент
                                </Button>
                        }
                        {
                            addCommentForComment
                                ?
                                <div>
                                    Добавить коммент на коммент
                                    <AddComment key={Comment.id} AdId={Comment.ScrapedAdId} ParentCommentId={Comment.id}
                                                CallBack={() => {
                                                    setAddCommentForComment(!addCommentForComment)
                                                }}
                                    />
                                </div>
                                : ""
                        }
                    </Card.Content>
                </Card>
        }
    </Fragment>
}

export default SingleComment;