import React, {Fragment, useContext, useEffect, useState} from "react";

import commentStore from "../../stores/commentStore";
import { observer } from "mobx-react";
import LoadingComponent from "../LoadingComponent";
import {Grid} from "semantic-ui-react";
import SingleComment from "./SIngleComment";
import {IComments} from "../../interfaces/IComments";
import AddComment from "./AddComment";

interface IProps {
    AdId : number;
}
const ListComments: React.FC<IProps> = ({AdId}) => {
    const CommentStore = useContext(commentStore);
    const [commentsLoaded,setCommentsLoaded] = useState(false);
    useEffect(() => {
        CommentStore.ListComments(AdId).then(()=>setCommentsLoaded(true));
    }, [CommentStore,AdId]);

    function PrintNestedComments(comments :IComments) {
            return <Grid.Row key={comments.comment.id} style={{marginLeft:10}}>
                {
                    <div>
                    <SingleComment Comment={comments.comment}/>
                        {
                            comments.nestedComments.map(c => PrintNestedComments(c))
                        }
                    </div>
                }
            </Grid.Row>
    }

    function ReloadComments() {
        setCommentsLoaded(false);
        CommentStore.ListComments(AdId).then(()=>setCommentsLoaded(true));
    }

    return <Fragment>
        {
            commentsLoaded ?
                <Grid>
                    {CommentStore.CommentsForCurrentAd.map(c =>
                    {
                        return <Grid.Row key={c.comment.id}>
                            <div>
                            <SingleComment Comment={c.comment} />
                                {
                                    c.nestedComments !== undefined ?
                                    c.nestedComments.map(z => PrintNestedComments(z)) : ""
                                }
                            </div>
                        </Grid.Row>
                    })
                    }

                    <AddComment AdId={AdId} ParentCommentId={null} CallBack={()=>ReloadComments()}/>


                </Grid>
                : <LoadingComponent content={"Loading comments..."}/>
        }
    </Fragment>
}

export default observer(ListComments);