import {Container} from "semantic-ui-react";
import React from "react";
import NavBar from "../NavBar/NavBar";
import ScrapedAdList from "../Common/ScrapedAdList";


const HomePage =() => {
    return (
        <Container>
            <NavBar/>
            <ScrapedAdList/>
        </Container>
    )
};

export default HomePage;

//<Button as={Link} to={"/editor"}> Goto Editor</Button>
