export interface IToken {
  nameid: string;
  typ: string;
  exp: number;
  iat: number;
  nbf: number;
}

export enum CurrentUser {
  User = 1,
  Admin,
  SuperAdmin
}
