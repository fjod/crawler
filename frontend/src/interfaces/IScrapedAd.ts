export interface IScrapedAd {
  id: number;
  baseUrl: string;
  scrapedAt: Date;
  uniqueId: number;
  description: string;
  freshDate: Date;
  createDate: Date;
  manufactureYear: number;
  vin: string;
  price: number;
  sellerName: string;
  mileage: number;
  color: string;
  city: string;
}

//public ICollection<AdPic> AdPics { get; set; }

export enum SortAds {
    ByDate = 0,
    ByRating,
    NotRated
}

