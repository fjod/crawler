export interface IUserWithCreds_DTO {
  uid: string;
  name: string;
  surname: string;
  ava1: string;
  type: string;
  bandatetime: Date | null;
}
