export interface IPrice {
  priceDate: string;
  value: number;
  amountOfPrices: number;
}

export interface IDTO_AdsPricesChange {
  prices: IPrice[];
  highestPrice : number;
  lowestPrice : number;
}
