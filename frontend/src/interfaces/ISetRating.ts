export interface ISetRating {
  advUniqueId: number;
  value: number;
}
