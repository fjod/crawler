import { IScrapedAd } from "./IScrapedAd";
import {IAdPic} from "./IAdPic";

export interface IEditorUniqueAd {
  ad: IScrapedAd;
  smallPics : IAdPic[];
}
