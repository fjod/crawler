import {IScrapedAd} from "./IScrapedAd";

export interface ILatestAd {
    current: IScrapedAd,
    unique : IScrapedAd,
    scrapedLocation : string,
    fileLocation : string
}
