export interface IBanUser {
  uid: string;
  days: number;
}

export interface IUserRole {
  uid: string;
  role: number;
}
