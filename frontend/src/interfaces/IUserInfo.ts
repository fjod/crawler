import { IVkUser } from "./IVkUser";

export interface IUserInfo {
  user: IVkUser;
  token: string;
}
