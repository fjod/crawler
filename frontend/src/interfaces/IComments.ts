import { IScrapedAd } from "./IScrapedAd";
import { IVkUser } from "./IVkUser";

export interface IComment {
  id: number;
  ScrapedAdId: number;
  ScrapedAd: IScrapedAd;
  VkUserId: number;
  vkUser: IVkUser;
  text: string;
  DateTime: Date;
  ParentId: number;
  Parent: IComment;
}

export interface IComments {
  nestedComments: IComments[];
  comment: IComment;
}

export interface IEditComment {
  Id: number;
  Text: string;
}

export interface IAddComment {
  Text: string;
  AdId: number;
  ParentId: number | null;
}
