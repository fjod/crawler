export interface IAdPic {
  id: number;
  imgHash: string;
  width: number;
  height: number;
  isBig: boolean;
  fileName: string;
  url: string;
}
