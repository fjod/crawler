export interface IRating {
  currentRating: number;
  totalVotes: number;
}
