export interface IScrapedAdPreview {
  rating: number;
  commentsCount: number;
  manufactureYear: number;
  price: number;
  mileage: number;
  city: string;
  uniqueId: number;
}
