export interface IFieldDifference {
  fieldName: string;
  prevFieldValue: string;
  newFieldValue: string;
}

export interface IAdHistory {
  prevId: number;
  nextId: number;
  differences: IFieldDifference[];
  prevScrapedAt: string;
  nextScrapedAt: string;
}
