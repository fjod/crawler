import React, {useContext, useEffect} from "react";
import {Route} from 'react-router-dom';

import EditorDashBoard from "./layout/Editor/EditorDashBoard";
import {Container} from "semantic-ui-react";
import HomePage from "./layout/Home/Home";
import UniqueDetails from "./layout/Editor/UniqueDetails";
import AuthStore from "../src/stores/authStore"
import DetailsWrapper from "./layout/Common/DetailsWrapper";
import UserList from "./layout/Administration/UserList";
import AllAdsChart from "./layout/Charts/AllAdsChart";


const App = () => {
    const authStore = useContext(AuthStore);
    const {setAppLoaded,token} = authStore;
    const {GetInfo} = authStore;
    useEffect( ()=>{
       if (token){
           GetInfo();
           setAppLoaded();
       }
       else{
           setAppLoaded();
       }
    },[GetInfo,setAppLoaded,token]);


  return (

       <Container fluid={true} >
           <Route path='/' component={HomePage} exact   />
           <Route path='/editor' component={EditorDashBoard} exact />
           <Route path='/adminPanel' component={UserList} exact />
           <Route path='/editor/details/:id' component={UniqueDetails}  />
           <Route path='/ad/details/:id' component={DetailsWrapper}  />
           <Route path='/chart' component={AllAdsChart} exact />
       </Container>
  );
};

export default App;

