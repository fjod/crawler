import { observable, action } from "mobx";
import { ILatestAd } from "../interfaces/ILatestAd";
import agent from "../api/agent";
import { createContext } from "react";
import { IAdPic } from "../interfaces/IAdPic";
import { IEditorUniqueAd } from "../interfaces/IEditorUniqueAd";

class EditorStore {
  @observable LatestAds: ILatestAd[] = [];
  @observable loadingInitial = false;
  shift: number = 0;

  @action loadLatestAds = (shift: number = 0) => {
    this.loadingInitial = true;
    agent.LatestAd.list(10, shift)
      .then(receivedAds => {
        receivedAds.forEach(la => {
          this.LatestAds.push(la);
        });
      })
      .finally(() => (this.loadingInitial = false));
  };

  @observable BigImage: IAdPic | null = null;
  @observable loadBigAd: boolean = false;
  @action loadBigImage = (id: number) => {
    this.loadBigAd = true;
    agent.BigPic.get(id)
      .then(value => {
        this.BigImage = value;
        console.log(this.BigImage);
      })
      .finally(() => (this.loadBigAd = false));
  };

  DetailsAdId: number = 0;
  @observable DetailsAd: IEditorUniqueAd | null = null;
  @action loadDetails = (id: number) => {
    this.loadingInitial = true;
    agent.UniqueDetails.get(id)
      .then(det => (this.DetailsAd = det))
      .finally(() => (this.loadingInitial = false));
  };
}

export default createContext(new EditorStore());
