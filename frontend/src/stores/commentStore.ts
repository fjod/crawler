import agent from "../api/agent";
import { action, observable } from "mobx";
import { IAddComment, IComments, IEditComment } from "../interfaces/IComments";
import { createContext } from "react";

class CommentStore {
  @observable CommentsForCurrentAd: IComments[] = [];

  @action EditComment = async (input: IEditComment) => {
    agent.Comments.edit(input).then();
  };

  @action ListComments = async (adId: number) => {
    agent.Comments.list(adId).then(comments => {
      this.CommentsForCurrentAd = [];
      comments.forEach(c => this.CommentsForCurrentAd.push(c));
    });
  };

  @action UserDeleteComment = async (id: number) => {
    agent.Comments.userDelete(id).then();
  };

  @action AddComment = async (input: IAddComment) => {
    agent.Comments.addComment(input).then(comment => {
      let parentComment = this.CommentsForCurrentAd.find(
        c => c.comment.id === input.ParentId
      );
      let emptyNestedComments: IComments[] = [];
      let newComment: IComments = {
        comment: comment,
        nestedComments: emptyNestedComments
      };
      if (parentComment) {
        if (parentComment!.nestedComments)
          parentComment!.nestedComments.push(newComment);
        else {
          parentComment!.nestedComments = emptyNestedComments;
          parentComment!.nestedComments.push(newComment);
        }
      } else this.CommentsForCurrentAd.push(newComment); //user added parent comment
    });
  };
}

export default createContext(new CommentStore());
