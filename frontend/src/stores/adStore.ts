import agent from "../api/agent";
import { action, observable } from "mobx";
import { IAdPic } from "../interfaces/IAdPic";
import { createContext } from "react";
import { IScrapedAdPreview } from "../interfaces/IScrapedAdPreview";
import { SortAds } from "../interfaces/IScrapedAd";
import { IPrice } from "../interfaces/IPrice";

class AdStore {
  @action loadPictures = async (uniqueId: number) => {
    let pics: IAdPic[] = [];
    await agent.ScrapedAd.ImagesForUniqueAd(uniqueId).then(pp =>
      pp.forEach(z => pics.push(z))
    );
  };

  @observable CurrentAdSorting: SortAds = SortAds.ByDate;
  @action changeSorting = (newSortOrder: SortAds) => {
    if (newSortOrder === this.CurrentAdSorting) return;
    this.CurrentAdSorting = newSortOrder;
    this.loadLatestAds(this.shift);
  };

  @observable LatestAds: IScrapedAdPreview[] = [];
  @observable loadingInitial = false;
  shift: number = 0;

  @action loadLatestAds = (shift: number = 0) => {
    this.loadingInitial = true;

    agent.ScrapedAd.list(4, shift, this.CurrentAdSorting)
      .then(receivedAds => {
        receivedAds.forEach(la => {
          this.LatestAds.push(la);
        });
      })
      .finally(() => (this.loadingInitial = false));
  };

  @observable loadingChartData = true;
  @observable ChartData: IPrice[] = [];
  chartHighestPrice: number = 0;
  chartLowestPrice: number = 0;
  @action loadTotalChartData = () => {
    this.loadingChartData = true;
    agent.ScrapedAd.getTotalPrices()
      .then(receivedPrices => {
        this.ChartData = receivedPrices.prices;
        this.chartHighestPrice = receivedPrices.highestPrice;
        this.chartLowestPrice = receivedPrices.lowestPrice;
      })
      .finally(() => (this.loadingChartData = false));
  };

  @action loadTotalChartDataForAd = (uid: string) => {
    this.loadingChartData = true;
    agent.ScrapedAd.getPriceChangeForAd(uid)
      .then(receivedPrices => {
        this.ChartData = receivedPrices.prices;
        this.chartHighestPrice = receivedPrices.highestPrice;
        this.chartLowestPrice = receivedPrices.lowestPrice;
      })
      .finally(() => (this.loadingChartData = false));
  };
}

export default createContext(new AdStore());
