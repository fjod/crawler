import { action, observable, reaction } from "mobx";
import { IUserInfo } from "../interfaces/IUserInfo";
import agent from "../api/agent";
import { createContext } from "react";
import { IUserRegister } from "../interfaces/IUserRegister";

import JwtDecode from "jwt-decode";
import { CurrentUser, IToken } from "../interfaces/IToken";
import { IUserWithCreds_DTO } from "../interfaces/IUserWithCreds_DTO";
import { IBanUser, IUserRole } from "../interfaces/IBanUser";

class AuthStore {
  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          //console.log("got token! " + token + "authStore.ctor.reaction");
          window.localStorage.setItem("jwt", token);
        } else {
          // console.log("removing token " + token + "authStore.ctor.reaction");
          window.localStorage.removeItem("jwt");
        }
      }
    );
  }

  @observable isLoggedIn: boolean = false;
  @observable UserInfo: IUserInfo | null = null;
  @observable token: string | null = window.localStorage.getItem("jwt");
  @observable appLoaded = false;

  @action Register = (guid: string) => {
    //этот метод работает по авторизации с вк

    const temp: IUserRegister = { id: guid };
    agent.Auth.register(temp).then(userInfo => {
      this.UserInfo = userInfo;

      if (Object.keys(userInfo).length > 0) {
        this.isLoggedIn = true;
        this.setToken(userInfo.token);
      }
    });
  };

  @action GetInfo = () => {
    //этот метод должен работать по jwt токену
    this.isLoggedIn = false;
    //скачивает имя и аватарку по кукисам
    agent.Auth.getInfo().then(value => {
      if (Object.keys(value).length > 0) {
        this.UserInfo = value;
        this.isLoggedIn = true;
        this.setToken(value.token);
      }
    });
  };

  @action logout = () => {
    this.setToken(null);
    this.UserInfo = null;
    this.isLoggedIn = false;
  };

  @observable CurrentUserType: CurrentUser | null = null;

  @action setToken = (token: string | null) => {
    this.token = token;
    if (token !== null) {
      const decodedTokenPayload = JwtDecode<IToken>(token);
      const userFromToken = decodedTokenPayload.typ;

      if (userFromToken === "User") {
        this.CurrentUserType = CurrentUser.User;
      }
      if (userFromToken === "Admin") {
        this.CurrentUserType = CurrentUser.Admin;
      }
      if (userFromToken === "SuperAdmin") {
        this.CurrentUserType = CurrentUser.SuperAdmin;
      }
    }
  };

  @action setAppLoaded = () => {
    this.appLoaded = true;
  };

  @action AssignRoleToUser = async (_Uid: string, currentUser: CurrentUser) => {
    let roleNumber = 2;
    switch (currentUser) {
      case CurrentUser.Admin:
        roleNumber = 1;
        break;
      case CurrentUser.SuperAdmin:
        roleNumber = 1;
        break;
      case CurrentUser.User:
        roleNumber = 2;
        break;
    }
    let newRole: IUserRole = {
      role: roleNumber,
      uid: _Uid
    };

    agent.Auth.assignRole(newRole).then();
    let roleUser = this.UserList.find(c => c.uid === _Uid);
    if (roleUser !== null) {
      roleUser!.type = currentUser.toString();
    }
  };

  @observable UserList: IUserWithCreds_DTO[] = [];
  @action LoadUserList = async () => {
    agent.Auth.listUsers().then(users => {
      this.UserList = [];
      users.forEach(c => this.UserList.push(c));
    });
  };

  // days = 0 -> unban
  @action banUser = async (_Uid: string, _days: number) => {
    let newBan: IBanUser = {
      days: _days,
      uid: _Uid
    };
    agent.Auth.banUser(newBan).then(
      //find this user in list and assign banTime to him
      () => {
        let bannedUser = this.UserList.find(c => c.uid === _Uid);
        let dateTime = new Date();
        if (bannedUser !== null)
          bannedUser!.bandatetime = this.addDays(dateTime, _days);
      }
    );
  };

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }
}

export default createContext(new AuthStore());
