import axios, { AxiosResponse } from "axios";
import { ILatestAd } from "../interfaces/ILatestAd";
import { IAdPic } from "../interfaces/IAdPic";
import { IEditorUniqueAd } from "../interfaces/IEditorUniqueAd";
import { IUserInfo } from "../interfaces/IUserInfo";
import { IUserRegister } from "../interfaces/IUserRegister";
import { IScrapedAdPreview } from "../interfaces/IScrapedAdPreview";
import { IAdHistory } from "../interfaces/IDifferences";
import { IScrapedAd } from "../interfaces/IScrapedAd";
import { ISetRating } from "../interfaces/ISetRating";
import {
  IAddComment,
  IComment,
  IComments,
  IEditComment
} from "../interfaces/IComments";
import { IUserWithCreds_DTO } from "../interfaces/IUserWithCreds_DTO";
import { IBanUser, IUserRole } from "../interfaces/IBanUser";
import { IRating } from "../interfaces/IRating";
import { IDTO_AdsPricesChange } from "../interfaces/IPrice";

//axios.defaults.baseURL = "http://localhost:5000/api";
axios.defaults.baseURL = process.env.REACT_APP_API_URL;

axios.interceptors.request.use(
  config => {
    const token = window.localStorage.getItem("jwt");
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
  get: (url: string) => axios.get(url).then(responseBody),
  post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
  put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
  del: (url: string) => axios.delete(url).then(responseBody)
};

const LatestAd = {
  list: (take: number = 10, skip: number = 0): Promise<ILatestAd[]> =>
    requests.get(`/editor/list?take=${take}&skip=${skip}`)
};

const AdPageName = "Page";
const ScrapedAd = {
  ImagesForUniqueAd: (id: number): Promise<IAdPic[]> =>
    requests.get(`/${AdPageName}/images?id=${id}`),
  list: (
    take: number = 10,
    skip: number = 0,
    sort: number
  ): Promise<IScrapedAdPreview[]> =>
    requests.get(`/${AdPageName}/list?take=${take}&skip=${skip}&sort=${sort}`),
  picsDifference: (prevId: number, nextId: number): Promise<IAdPic[]> =>
    requests.get(
      `/${AdPageName}/picsDifference?prevId=${prevId}&nextId=${nextId}`
    ),
  history: (uniqueId: number): Promise<IAdHistory[]> =>
    requests.get(`/${AdPageName}/history?id=${uniqueId}`),
  details: (id: string): Promise<IScrapedAd> =>
    requests.get(`${AdPageName}/${id}`),
  setRating: (value: ISetRating): Promise<number> =>
    requests.post(`${AdPageName}/rating`, value),
  getRating: (uniqueId: number): Promise<IRating> =>
    requests.get(`${AdPageName}/rating?uniqueId=${uniqueId}`),
  getTotalPrices: (): Promise<IDTO_AdsPricesChange> =>
    requests.get(`/${AdPageName}/totalPriceChange`),
  getPriceChangeForAd: (id: string): Promise<IDTO_AdsPricesChange> =>
    requests.get(`${AdPageName}/priceChange/${id}`)
};

const BigPic = {
  get: (id: number): Promise<IAdPic> =>
    requests.get(`/${AdPageName}/images?id=${id}`)
};

const UniqueDetails = {
  get: (id: number): Promise<IEditorUniqueAd> =>
    requests.get(`/editor/details/${id}`)
};

const Auth = {
  register: (register: IUserRegister): Promise<IUserInfo> =>
    requests.post("/user/register", register),
  getInfo: (): Promise<IUserInfo> => requests.get("/user/info"),
  listUsers: (): Promise<IUserWithCreds_DTO[]> => requests.get("/user/list"),
  banUser: (input: IBanUser): Promise<void> =>
    requests.post("user/assignBan", input),
  assignRole: (input: IUserRole): Promise<void> =>
    requests.post("user/assignRole", input)
};

const Comments = {
  edit: (input: IEditComment): Promise<void> =>
    requests.post(`/comment/${input.Id}`, input),
  userDelete: (id: number): Promise<void> => requests.del(`/comment/${id}`),
  addComment: (input: IAddComment): Promise<IComment> =>
    requests.post("/comment", input),
  list: (adId: number): Promise<IComments[]> => requests.get(`/comment/${adId}`)
};

export default {
  LatestAd,
  BigPic,
  UniqueDetails,
  Auth,
  ScrapedAd,
  Comments
};
