﻿using System;
using System.Text;

namespace Application.Misc
{
    public static  class DecodeExtension
    {
        public static string Decode(this string input)
        {
                Encoding one = Encoding.GetEncoding("UTF-8"); 
                Encoding two = Encoding.GetEncoding("iso-8859-1");
                byte[] utfBytes = one.GetBytes(input);
                byte[] isoBytes = Encoding.Convert(one, two, utfBytes);
                return (one.GetString(isoBytes));
        }
        
        public static DateTime ToDateTime(this string input)
        {
            var dateString = input.Substring(0, input.Length - 3);
            var createDate = new DateTime(1970,1,1,0,0,0,0,DateTimeKind.Utc);
            return createDate.AddSeconds( double.Parse(dateString) ).ToLocalTime();
        }
    }
}