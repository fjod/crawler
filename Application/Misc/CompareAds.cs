﻿using System;
using Application.Interfaces;
using Domain;

namespace Application.Misc
{
    public static class CompareAds
    {
        
        public static bool IsEqual(this ScrapedAd ad, ScrapedAd other, ICompareString stringy)
        {
            
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(ad, other)) return true;
            
            //if they have same uri 
            if (Equals(ad.BaseUrl, other.BaseUrl)) return true;
            if (Equals(ad.CreateDate, other.CreateDate)) return true;
            
            double sameAd = 0;
            
            if ( stringy.CrawlerCompare(ad.Description,other.Description) < 0.5) sameAd += 0.3;
            if (ad.ManufactureYear == other.ManufactureYear)sameAd += 0.1;
            if (ad.Vin == other.Vin && 
                !String.IsNullOrEmpty(ad.Vin)&&
                !String.IsNullOrEmpty(other.Vin))
                sameAd += 0.3;
            if (ad.SellerName == other.SellerName)sameAd += 0.2;
            if (ad.Color == other.Color)sameAd += 0.1;
            if (ad.City == other.City)sameAd += 0.1;

            return sameAd >= 0.7;
        }
    }
}