﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using DataBase;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.Misc
{
    public enum SortAds
    {
        ByDate = 0,
        ByRating = 1,
        NotRated = 2
    }

    public class SortFactory
    {
        private readonly DataContext _context;

        public SortFactory(DataContext context)
        {
            _context = context;
        }
        public IAdSorter GetSorter(SortAds type)
        {
            return type switch
            {
                SortAds.ByDate => new SortByDate(_context),
                SortAds.ByRating => new SortByRating(_context),
                SortAds.NotRated => new SortByNotRated(_context),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, "Weird SortAds type in sorter factory")
            };
        }
    }

    class SortByDate : IAdSorter
    {
        private readonly DataContext _context;

        public SortByDate(DataContext context)
        {
            _context = context;
        }
        public  Task<List<ScrapedAd>> Sort(int take, int skip)
        {
           return  _context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId)
               .OrderByDescending(ad => ad.ScrapedAt).Skip(skip).Take(take).
               Include(ad => ad.Ratings).
               Include(ad => ad.Comments).ToListAsync();
        }
    }
    class SortByRating : IAdSorter
    {
        private readonly DataContext _context;

        public SortByRating(DataContext context)
        {
            _context = context;
        }
        public async Task<List<ScrapedAd>> Sort(int take, int skip)
        {
            var adsWithRating = await _context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId).
                Include(ad => ad.Ratings).
                Include(ad => ad.Comments).
                ToListAsync();
                        
            var sortedByRating =
                adsWithRating.OrderByDescending(ad => ad.Ratings.Average(r => r.Rating))
                    .Skip(skip).Take(take);
                        
           var newerAds = await
                _context.ScrapedAds.Where(ad => 
                        sortedByRating.Contains(ad)).
                    Include(ad => ad.Ratings).ToListAsync();
           
          return newerAds.OrderByDescending(ad => ad.Ratings.Average(r => r.Rating)).ToList();
        }
    }
    class SortByNotRated : IAdSorter
    {
        private readonly DataContext _context;

        public SortByNotRated(DataContext context)
        {
            _context = context;
        }
        public async Task<List<ScrapedAd>> Sort(int take, int skip)
        {
            var adsWithRating = await _context.ScrapedAds.Where(ad => ad.ID == ad.UniqueId).
                Include(ad => ad.Ratings).
                Include(ad => ad.Comments).
                ToListAsync();
                        
            var sortedByRating =
                adsWithRating.OrderBy(ad => ad.Ratings.Average(r => r.Rating))
                    .Skip(skip).Take(take);
                        
          var  newerAds = await
                _context.ScrapedAds.Where(ad => sortedByRating.Contains(ad)).
                    Include(ad => ad.Ratings).
                    ToListAsync();
          
            return newerAds.OrderBy(ad => ad.Ratings.Average(r => r.Rating)).ToList();
        }
    }
}