﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.DTO;

namespace Application.Misc
{
    public class GroupedMedianPrice
    {
        public static DTO_AdsPricesChange Calc(List<Price> prices)
        {
            var groupedPrices = prices.GroupBy(p => p.PriceDate.Year);
            var ret = new List<Price>();
            foreach (var priceByYear in groupedPrices)
            {
                var priceByMonth = priceByYear.GroupBy(p => p.PriceDate.Month);

                //calculate median price
                foreach (var pricesInMonth in priceByMonth)
                {
                    var priceByDay = pricesInMonth.GroupBy(p => p.PriceDate.Day);

                    foreach (var pricesInDay in priceByDay)
                    {
                        var currentMonthAndYearAndDay = new DateTime(priceByYear.Key, pricesInMonth.Key, pricesInDay.Key,1,1,1);
                        var avgPriceInDay = (int) pricesInDay.Average(q => q.Value);
                        var p = new Price(currentMonthAndYearAndDay, avgPriceInDay,pricesInDay.Count());
                        ret.Add(p);
                    }
                }
            }

            ret = ret.OrderBy(p => p.PriceDate).ToList();
            return new DTO_AdsPricesChange {Prices = ret, HighestPrice = ret.Max(p=> p.Value), LowestPrice = ret.Min(p=> p.Value)};
        }
    }
}