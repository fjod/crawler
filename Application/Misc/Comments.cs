using System.Collections.Generic;
using Domain;

namespace Application.Misc
{
    public class Comments
    {
        public List<Comments> NestedComments { get; set; }
        public Comment Comment { get; set; }
    }
}