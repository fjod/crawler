﻿using AutoMapper;
using Domain;

namespace Application.DTO
{
    public class DTO_VkUser
    {
        public string  Uid { get; set; }
        public string  Name { get; set; }
        public string  Surname { get; set; }
        public string  Ava1 { get; set; }
    }
    
    public class VkUserMapping : Profile
    {
        public VkUserMapping()
        {
            CreateMap<VkUser, DTO_VkUser>(); 
        }
    }
}