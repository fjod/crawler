﻿using System;
using System.Collections.Generic;

namespace Application.DTO
{
    public class DTO_AdsPricesChange
    {
        public List<Price> Prices { get; set; } = new List<Price>();
        public int HighestPrice { get; set; }
        public int LowestPrice { get; set; }
    }

    public class Price
    {
       
        public Price(DateTime priceDate, int value)
        {
            PriceDate = priceDate;
            Value = value;
        }
        public Price(DateTime priceDate, int value, int amountOfPrices)
        {
            PriceDate = priceDate;
            Value = value;
            AmountOfPrices = amountOfPrices;
        }
        public DateTime PriceDate { get;  }
        public int Value { get;  }
        public int AmountOfPrices { get;  }
        public override string ToString()
        {
            return $"{PriceDate.Year} {PriceDate.Month}  {Value}";
        }
    }
}