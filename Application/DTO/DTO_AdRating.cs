﻿using System.Linq;

namespace Application.DTO
{
    public class DTO_AdRating
    {
        public int CurrentRating { get; }
        public int TotalVotes { get; }

        public DTO_AdRating(IQueryable<int> ratings)
        {
            TotalVotes = ratings.Count();
            if (TotalVotes > 0)
                CurrentRating = ratings.Sum() / TotalVotes;
            else CurrentRating = 0;
        }
    }
}