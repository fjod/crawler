﻿using System;
using System.Collections.Generic;
using Application.User;

namespace Application.DTO
{
    public class DTO_UserWithCreds
    {
        /// <summary>
        /// from vk user to display on index page
        /// </summary>
        public string Name { get; set; }

        public string Surname { get; set; }
        public string Ava1 { get; set; }

        /// <summary>
        /// admin or not
        /// </summary>
        public UserType Type { get; set; }

        /// <summary>
        /// from AppUser
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// is banned until this dateTime
        /// </summary>
        public DateTime? BanDateTime { get; set; }
        
        static readonly Dictionary<string, UserType> Dict = new Dictionary<string, UserType> {
            { "SuperAdmin", UserType.SuperAdmin },
            { "Admin", UserType.Admin },
            { "User", UserType.User },
            { "Undefined", UserType.Undefined }
        };

        public static UserType FromString(string userString)
        {
            return String.IsNullOrEmpty(userString) ? UserType.Undefined : Dict[userString];
        }
        
    }

    
}