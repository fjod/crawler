using System;
using System.Linq;
using AutoMapper;
using Domain;
using MoreLinq;

namespace Application.DTO
{
    public class ScrapedAdMapping : Profile
    {
        public ScrapedAdMapping()
        {
            CreateMap<ScrapedAd, DTO_ScrapedAdPreview>()
                .ForMember(dto => dto.Rating, 
                    source => 
                        source.MapFrom(ad =>
                            
                            ad.Ratings.Count > 0 ? ad.Ratings.Sum(r => r.Rating)/ad.Ratings.Count : 0  

                            ))
                .ForMember(dto => dto.CommentsCount, 
                    source=> 
                        source.MapFrom(ad => ad.Comments.Count))
                .ForMember(dto=> dto.UniqueId, 
                    source => 
                    source.MapFrom(ad => ad.UniqueId ?? 0)); 
        }
    }
    public class DTO_ScrapedAdPreview : IEquatable<DTO_ScrapedAdPreview>
    {
        public int Rating;
        public int CommentsCount;
        public int ManufactureYear;
        public int Price;
        public int Mileage;
        public string City;
        public int UniqueId;

        public bool Equals(DTO_ScrapedAdPreview other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Rating == other.Rating && 
                   CommentsCount == other.CommentsCount &&
                   ManufactureYear == other.ManufactureYear &&
                   Price == other.Price &&
                   Mileage == other.Mileage && 
                   City == other.City && 
                   UniqueId == other.UniqueId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DTO_ScrapedAdPreview) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Rating, CommentsCount, ManufactureYear, Price, Mileage, City, UniqueId);
        }
    }
}