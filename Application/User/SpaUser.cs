using Application.DTO;
using Domain;

namespace Application.User
{
    public class SpaUser
    {
        public DTO_VkUser user { get; set; }
        public string token { get; set; }
    }
}