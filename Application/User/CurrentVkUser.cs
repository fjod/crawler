using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Misc;
using DataBase;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.User
{
    public static class CurrentVkUser
    {
        public static async Task<VkUser> Get(DataContext context, IUserAccessor accessor, CancellationToken token)
        {
            var name = accessor.GetCurrentUserName();
            if (string.IsNullOrEmpty(name))
                throw new RestException(HttpStatusCode.BadRequest, new {Error = "User is not authenticated"});

            var vkUser = await context.VkUsers.FirstOrDefaultAsync(c => c.Uid == name,
                token);

            return vkUser;
        }
    }
}