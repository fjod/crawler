﻿namespace Application.User
{
    public enum UserType
    {
        SuperAdmin = 0,
        Admin = 1,
        User = 2,
        Undefined = 3
    }
}