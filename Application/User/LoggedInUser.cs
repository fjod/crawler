

namespace Application.User
{
    public class LoggedInUser
    {
        public const string uid = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        public const string givenname = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname";
        public const string surname = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname";
        public const string ava1 = "urn:vkontakte:photo:link";
        public const string ava2 = "urn:vkontakte:photo_thumb:link";
    }
}