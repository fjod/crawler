using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using Domain.Frontend;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads.Editor
{
    /// <summary>
    /// handle duplicate images manually
    /// </summary>
    public class UniqueDetails
    {
        public class Query : IRequest<EditorUniqueAd>
        {
            /// <summary>
            /// Id of unique ad
            /// </summary>
            public int ID;
        }

        public class Handler : IRequestHandler<Query,EditorUniqueAd>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<EditorUniqueAd> Handle(Query request, CancellationToken cancellationToken)
            {
                var ads = await (from ad in _context.ScrapedAds
                    where ad.UniqueId == request.ID
                    select ad).ToListAsync(cancellationToken); //many ads for this unique ID


                List<AdPic> pics2 = new List<AdPic>();
                ads.ForEach(ad =>
                {
                    var imagesForThisAd = from picLink
                            in _context.AdImageLink
                        where picLink.Ad.ID == ad.ID
                        join adPic in _context.AdPics on picLink.Image.ID equals adPic.ID
                        select adPic;
                    pics2.AddRange(imagesForThisAd);
                });
                    
                 
                    List<AdPic> retPics = new List<AdPic>();
                    pics2.ForEach(p =>
                    {
                        if (!p.IsBig)
                        {
                            var gotSameHash = retPics.Any(rp => rp.ImgHash == p.ImgHash);
                            if (!gotSameHash) retPics.Add(p);
                        }
                    });

                    var ret =  from ad in ads
                        where ad.UniqueId == ad.ID
                        select ad //should be only one anyway
                        into unique
                        select new EditorUniqueAd
                        {
                            Ad = unique,
                            SmallPics = retPics
                        };

                return ret.First();
            }
        }
    }
}