using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain.Frontend;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads.Editor
{
    public class List
    {
        public class Query : IRequest<List<LatestAd>>
        {
            public int Take { get; set; } = 10;
            public int Skip { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<LatestAd>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<LatestAd>> Handle(Query request, CancellationToken cancellationToken)
            {
                var adsAndImageId =
                    (from ad in _context.ScrapedAds.OrderByDescending(localAd => localAd.ScrapedAt).Skip(request.Skip).Take(request.Take)
                        join uniqueAd in _context.ScrapedAds
                            on ad.UniqueId equals uniqueAd.ID
                        join table in _context.AdImageLink
                            on ad.ID equals table.Ad.ID
                        select new {_ad = ad, _uniqueAd = uniqueAd, table.Image.ID}
                    ); //all images for this ad, because I dont know which are good


                var imagesWithAds = from pic in _context.AdPics.Where(p => !p.IsBig)
                    join ad in adsAndImageId
                        on pic.ID equals ad.ID //many small images here for given ad
                    select new LatestAd
                    {
                        Current = ad._ad,
                        Unique = ad._uniqueAd,
                        FileLocation = pic.FileName,
                        ScrapedLocation = pic.Url
                    };

                var groupedImagesWithAds = 
                    from la in await imagesWithAds.ToListAsync(cancellationToken)
                    group la by la.Current
                    into g
                    select new LatestAd
                    {
                        Current = g.Key,
                        FileLocation = g.First().FileLocation,
                        ScrapedLocation = g.First().ScrapedLocation,
                        Unique = g.First().Unique
                    }; //group by ad so I can have only one image for each ad. Grouping fails if included in prev request

                return groupedImagesWithAds.ToList();
            }
        }
    }
}