﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.DTO;
using Application.Misc;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads
{
    //Medium price change for bikes per month
    public class GetTotalPriceChange
    {
        public class Request : IRequest<DTO_AdsPricesChange>
        {
        }

        public class Handler : IRequestHandler<Request, DTO_AdsPricesChange>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<DTO_AdsPricesChange> Handle(Request request, CancellationToken cancellationToken)
            {
                var req = from ad in _context.ScrapedAds select new Price(ad.ScrapedAt, ad.Price);
                var prices = await req.ToListAsync(cancellationToken: cancellationToken);
                return GroupedMedianPrice.Calc(prices);
            }
        }
    }
}