using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.DTO;
using Application.Misc;
using AutoMapper;
using DataBase;
using Domain;
using Domain.Frontend;
using MediatR;

namespace Application.MediatR.Ads
{
    
    public class List
    {
        public class Query : IRequest<List<DTO_ScrapedAdPreview>>
        {
            public int Take { get; set; } = 10;
            public int Skip { get; set; }
            public SortAds Sort { get; set; } = SortAds.ByDate;
        }


        public class Handler : IRequestHandler<Query, List<DTO_ScrapedAdPreview>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<DTO_ScrapedAdPreview>> Handle(Query request, CancellationToken cancellationToken)
            {
                var newerAds = await new SortFactory(_context).
                    GetSorter(request.Sort).Sort(request.Take,request.Skip);

                return _mapper.Map<List<ScrapedAd>, List<DTO_ScrapedAdPreview>>(newerAds);
            }
        }
    }
}