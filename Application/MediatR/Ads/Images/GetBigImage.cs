using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads.Images
{
    public class GetBigImage
    {
        public class Query : IRequest<AdPic>
        {
            /// <summary>
            /// id of small pic
            /// </summary>
            public int Id;
        }

        public class Handler : IRequestHandler<Query, AdPic>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public async Task<AdPic> Handle(Query request, CancellationToken cancellationToken)
            {
                var ret = from picLink in _context.AdPicsLinks
                    where picLink.SmallImage.ID == request.Id
                    join pic in _context.AdPics on picLink.BigImage.ID equals pic.ID
                    select pic;
                
                 var retPic =  await ret.FirstOrDefaultAsync(cancellationToken);
                 retPic.FixUrlToUseHttps();
                 return retPic;
            }
        }
    }
}