using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads.Images
{
    public class PicsForAd
    {
        public static  Task<List<AdPic>> GetSmall(int id, DataContext context, CancellationToken cancellationToken)
        {
            return  (from ad in context.ScrapedAds
                where ad.ID == id
                join picLink in context.AdImageLink on ad equals picLink.Ad
                join pic in context.AdPics.Where(p => p.IsBig == false) on picLink.Image equals pic
                select pic).ToListAsync(cancellationToken);
        }
    }
}