using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;

namespace Application.MediatR.Ads.Images
{
    public class PicsDifference
    {
        public class Query : IRequest<List<AdPic>>
        {
            public int PrevId { get; set; }
            public int NextId { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<AdPic>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            public async Task<List<AdPic>> Handle(Query request, CancellationToken cancellationToken)
            {
                var picsForCurrentAd = await PicsForAd.GetSmall(request.PrevId, _context, cancellationToken);
                var picsForNextAd = await PicsForAd.GetSmall(request.NextId, _context, cancellationToken);

                var diffInPics = picsForCurrentAd.SequenceEqual(picsForNextAd);
                if (!diffInPics)
                {
                    return picsForCurrentAd.FindAll(currentPic =>
                        picsForNextAd.Any(nextPic => nextPic.Equals(currentPic)) == false).
                        ConvertAll(input =>
                        {
                            var retPic = input;
                            retPic.ImgHash = ""; // no need to send hashes to frontend
                            retPic.FixUrlToUseHttps();
                            return retPic;
                        });
                }

                return new List<AdPic>();
            }
        }
    }
}