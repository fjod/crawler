using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;
using MoreLinq;

namespace Application.MediatR.Ads.Images
{
    public class SmallForAd
    {
        public class Query : IRequest<List<AdPic>>
        {
            public int UniqueId { get; set; }
        }
        
         public class Handler : IRequestHandler<Query, List<AdPic>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<AdPic>> Handle(Query request, CancellationToken cancellationToken)
            {
                //get all pics for unique
                var uniqueTotalPics =
                    await PicsForAd.GetSmall(request.UniqueId, _context, cancellationToken);

                var ret = uniqueTotalPics.DistinctBy(pic => pic.ImgHash).ToList();
                foreach (var adPic in ret)
                {
                    adPic.CleanUp();
                }

                return ret;
            }
        }
    }
}