using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads
{

    public class Details
    {
        public class Query : IRequest<ScrapedAd>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, ScrapedAd>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<ScrapedAd> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.ScrapedAds.Where(ad => ad.ID == request.Id)
                    .FirstOrDefaultAsync(cancellationToken);
            }
        }
    }
}