using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Misc;
using Application.User;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads
{
    public class SetRating
    {
        public class Command : IRequest
        {
            public int value { get; set; }
            public int advUniqueId { get; set; }
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userManager;

            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _context = context;
                _userManager = userAccessor;
            }
            protected override async Task Handle(Command request, CancellationToken cancellationToken)
            {
                //first we need to get current user
                var vkUser = await CurrentVkUser.Get(_context, _userManager, cancellationToken);

                var adForRating = await _context.ScrapedAds.Where(ad =>
                    (ad.UniqueId == request.advUniqueId) && (ad.ID == ad.UniqueId)).ToListAsync(cancellationToken);
                if (!adForRating.Any())
                {
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "Wrong unique id"});
                }


                var oldRating =  _context.Ratings.Where(r =>
                        r.ScrapedAd.UniqueId == request.advUniqueId && r.VkUser == vkUser);
                    
                if (oldRating.Any())
                {
                    //just change the value then
                    var rating = oldRating.First();
                    rating.Rating = request.value;
                }
                else
                {
                    //user set new rating
                    var newRating = new AdRating
                    {
                        Rating = request.value, VkUser = vkUser, ScrapedAd = adForRating.First()
                    };

                    await _context.Ratings.AddAsync(newRating, cancellationToken);
                }
                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}