using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.DTO;
using Application.Interfaces;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Ads
{
    public class GetRating
    {
        public class Request : IRequest<DTO_AdRating>
        {
            public int AdUniqueId { get; set; }
        }

        public class Handler : IRequestHandler<Request, DTO_AdRating>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public Task<DTO_AdRating> Handle(Request request, CancellationToken cancellationToken)
            {
                var r1 = from rate in _context.Ratings
                    where rate.ScrapedAdId == request.AdUniqueId
                    select rate.Rating;
                
                DTO_AdRating ret = new DTO_AdRating(r1);
              
                return Task.FromResult(ret);
            }
        }
    }
}