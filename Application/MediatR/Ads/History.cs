using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Misc;
using DataBase;
using Domain.Frontend;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace Application.MediatR.Ads
{
    public class History
    {
        public class Query : IRequest<List<AdHistory>>
        {
            public int UniqueId { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<AdHistory>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<AdHistory>> Handle(Query request, CancellationToken cancellationToken)
            {
                var allAdsWithUnique = await _context.ScrapedAds.Where(ad => ad.UniqueId == request.UniqueId)
                    .OrderByDescending(ad => ad.ScrapedAt).ToListAsync(cancellationToken);

                if (!allAdsWithUnique.Any())
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "wrong unique id"});

                var ret = new List<AdHistory>();
                for (var i = 0; i < allAdsWithUnique.Count - 1; i++)
                {
                    var history = new AdHistory();

                    var currentAd = allAdsWithUnique[i];
                    var nextAd = allAdsWithUnique[i + 1];

                    var fieldDiff = currentAd.FindDifferences(nextAd);
                    history.Differences = fieldDiff;

                    history.NextId = nextAd.ID;
                    history.PrevId = currentAd.ID;
                    history.PrevScrapedAt = currentAd.ScrapedAt.ToShortDateString();
                    history.NextScrapedAt = nextAd.ScrapedAt.ToShortDateString();
                    ret.Add(history);
                }

                return ret;
            }
        }
    }
}