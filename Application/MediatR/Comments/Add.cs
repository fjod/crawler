using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Misc;
using Application.User;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Comments
{
    public class Add
    {
        public class Command : IRequest<Comment>
        {
            public string Text { get; set; }
            public int AdId { get; set; }
            public int? ParentId { get; set; }
        }

        public class Handler : IRequestHandler<Command,Comment>
        {
            private DataContext _context;
            private IUserAccessor _userManager;

            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _context = context;
                _userManager = userAccessor;
            }

            public async Task<Comment> Handle(Command request, CancellationToken cancellationToken)
            {
                if (request.Text == null)
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "provide some text"});
                
                var vkUser = await CurrentVkUser.Get(_context, _userManager, cancellationToken);
                if (vkUser == null)
                {
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "cant find this user"});
                }

                var appUserRequest = from user in _context.Users
                    where user.Uid == vkUser.Uid
                    select new
                    {
                        user.LockoutEnabled,
                        user.LockoutEnd
                    };
                var appUser = await appUserRequest.FirstOrDefaultAsync(cancellationToken: cancellationToken);
                if (appUser.LockoutEnabled && appUser.LockoutEnd.HasValue)
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "user is banned from adding comments"});
                
                var newComment = new Comment();
                if (request.ParentId != null)
                {
                    var parentComment = _context.Comments.Where(p => p.ID == request.ParentId);
                    if (!parentComment.Any())
                    {
                        throw  new RestException(HttpStatusCode.BadRequest, new {Error = "cant find parent comment"});
                    }
                    newComment.Parent = parentComment.First();
                }

                var ad = await _context.ScrapedAds.Where(adv => adv.ID == request.AdId).Include(a => a.Ratings)
                    .FirstAsync();
                if (ad == null)
                {
                    throw  new RestException(HttpStatusCode.BadRequest, new {Error = "cant find ad for this comment"});
                }
                
                newComment.VkUser = vkUser;
                newComment.Text = request.Text;
               // newComment.ScrapedAd = ad;
                ad.Comments.Add(newComment);
              //  _context.Comments.Add(newComment);
                
                await _context.SaveChangesAsync(cancellationToken);
                return newComment;
            }
        }
    }
}