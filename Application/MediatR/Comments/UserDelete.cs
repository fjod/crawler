using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Misc;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Comments
{
    public class UserDelete
    {
        public class Request : IRequest
        {
            public int Id { get; set; }
        }

        public class Handler : AsyncRequestHandler<Request>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            protected override async Task Handle(Request request, CancellationToken cancellationToken)
            {
                var comment = await _context.Comments.FirstOrDefaultAsync(c => c.ID == request.Id,
                    cancellationToken: cancellationToken);
                if (comment == null)
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "bad id for comment"});

                comment.Text = "";
                comment.VkUser = null;
                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}