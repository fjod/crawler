using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Misc;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Comments
{
    public class Delete
    {
        public class Request : IRequest
        {
            public int Id { get; set; }
        }

        public class Handler : AsyncRequestHandler<Request>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            protected override async Task Handle(Request request, CancellationToken cancellationToken)
            {
                var comment = await _context.Comments.FirstOrDefaultAsync(c => c.ID == request.Id,
                    cancellationToken: cancellationToken);
                if (comment == null)
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "bad id for comment"});
                var children = _context.Comments.Where(c => c.Parent == comment);
                foreach (var child in children)
                {
                    child.Parent = null;
                }
                _context.Comments.Remove(comment);
                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}