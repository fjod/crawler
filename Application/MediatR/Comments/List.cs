using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.Comments
{
    public class List
    {
        public class Query : IRequest<List<Misc.Comments>>
        {
            public int Id { get; set; }   
        }

        public class Handler : IRequestHandler<Query, List<Misc.Comments>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            public async Task<List<Misc.Comments>> Handle(Query request, CancellationToken cancellationToken)
            {
                var allComments = await _context.ScrapedAds.
                    Where(ad => ad.ID == request.Id)
                    .Include(ad => ad.Comments).ThenInclude(c=> c.VkUser)
                    .FirstOrDefaultAsync();

                var ret = new List<Misc.Comments>();
                
                foreach (var comment in allComments.Comments.Where(c => c.Parent == null))
                {
                    //top level - all comments are without parents
                   var nested =  GetNested(allComments.Comments, comment);
                   var c = new Misc.Comments {Comment = comment, NestedComments = nested};
                   ret.Add(c);
                }

                return ret;
            }
            
            /// <summary>
            /// looks for nested comments recursively
            /// </summary>
            /// <param name="list">list with all comments</param>
            /// <param name="comment">comment to find nested comments</param>
            /// <returns>list of all nested comments in tree-like structure for given parent</returns>
            private static List<Misc.Comments> GetNested(IEnumerable<Comment> list, Comment comment)
            {
                var enumerable = list as Comment[] ?? list.ToArray();
                var nested = enumerable.Where(c => c.Parent == comment);
                var ret = new List<Misc.Comments>();
                foreach (var nestedComment in nested)
                {
                    var c = new Misc.Comments
                    {
                        Comment = nestedComment, NestedComments = GetNested(enumerable, nestedComment)
                    };
                    ret.Add(c);
                }

                return ret;
            }
        }

        
    }
    
    
}