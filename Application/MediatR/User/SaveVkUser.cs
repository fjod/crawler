using System;
using System.Threading.Tasks;
using DataBase;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.User
{
    public class SaveVkUser
    {
        public class Command : IRequest
        {
            public Guid TempId { get; } = Guid.NewGuid();
            public VkUser UserInfo { get; set; }
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            protected override async Task Handle(Command request,
                System.Threading.CancellationToken cancellationToken)
            {
                var userId = request.UserInfo.Uid;

                var vkUser = await _context.VkUsers.FirstOrDefaultAsync(user => user.Uid == userId,
                     cancellationToken);
                if (vkUser != null)
                {
                    vkUser.Guid = request.TempId.ToString();
                    await _context.SaveChangesAsync(cancellationToken);
                    return;
                }

                await _context.VkUsers.AddAsync(request.UserInfo, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}