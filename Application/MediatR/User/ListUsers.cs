﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Application.DTO;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.User
{
    public class ListUsers
    {
        public class Query : IRequest<List<DTO_UserWithCreds>>
        {
            
        }

        public class Handler : IRequestHandler<Query, List<DTO_UserWithCreds>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            public async Task<List<DTO_UserWithCreds>> Handle(Query request, CancellationToken cancellationToken)
            {
                
                var users = from vkuser in _context.VkUsers
                    join contextUser in _context.Users on vkuser.Uid equals contextUser.Uid
                    join userClaim in _context.UserClaims 
                        on contextUser.Id equals userClaim.UserId
                        where userClaim.ClaimType == ClaimTypes.Role
                    let userBanOffset = contextUser.LockoutEnd
                    let userBan = userBanOffset.HasValue ? userBanOffset.Value.Date : (DateTime?)null
                    //https://stackoverflow.com/questions/62895794/c-sharp-lambda-null-conditional/62895994#62895994
                    select new DTO_UserWithCreds
                    { 
                        Name = vkuser.Name,
                        Surname = vkuser.Surname,
                        Ava1 = vkuser.Ava1,
                        Uid = vkuser.Uid,
                        Type =  DTO_UserWithCreds.FromString(userClaim.ClaimValue),
                        BanDateTime = userBan
                        // related https://stackoverflow.com/questions/44681362/an-expression-tree-lambda-may-not-contain-a-null-propagating-operator
                    };
               
                var usersDTO= await users.ToListAsync(cancellationToken);
                
                // var bannedUsers = from user in _context.Users where user.LockoutEnabled select new
                // {
                //     Id = user.Uid,
                //     BanDateTime = user.LockoutEnd
                // };
                // var bannedUsersList = await bannedUsers.ToListAsync(cancellationToken);
                //
                // foreach (var userIdAndBan in bannedUsersList)
                // {
                //     var sameUser = usersDTO.Find(c => c.Uid == userIdAndBan.Id);
                //     if (sameUser != null)
                //     {
                //         sameUser.BanDateTime = userIdAndBan.BanDateTime?.DateTime;
                //     }
                // }

                return usersDTO;
            }
        }
    }
}