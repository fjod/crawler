using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.DTO;
using Application.Interfaces;
using Application.Misc;
using Application.User;
using AutoMapper;
using DataBase;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.User
{
    public class Register
    {
        public class Command : IRequest<SpaUser>
        {
            public string Id { get; set; }
        }

        public class Handler : IRequestHandler<Command, SpaUser>
        {
            private readonly DataContext _context;
            private readonly IJWTGenerator _jwtGen;
            private readonly UserManager<AppUser> _userManager;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _accessor;

            public Handler(DataContext context, IJWTGenerator jwtGen,
                UserManager<AppUser> userManager, IMapper mapper, IUserAccessor accessor)
            {
                _context = context;
                _jwtGen = jwtGen;
                _userManager = userManager;
                _mapper = mapper;
                _accessor = accessor;
            }

            public async Task<SpaUser> Handle(Command request,
                System.Threading.CancellationToken cancellationToken)
            {
                
                var currentUser = await _context.VkUsers.FirstOrDefaultAsync(c => c.Guid == request.Id,
                     cancellationToken);
                
                if (currentUser == null)
                    throw new RestException(HttpStatusCode.BadRequest, new {Error = "no user for that input"});
                

                var appUser = await _context.Users.FirstOrDefaultAsync(user => user.Uid == currentUser.Uid,
                     cancellationToken);

                if (appUser != null)
                {
                    return new SpaUser
                    {
                        user = _mapper.Map<DTO_VkUser>(currentUser),
                        token = _jwtGen.CreateToken(currentUser, _accessor.GetCurrentUserType(appUser.Id))
                    };
                }
                appUser = new AppUser
                {
                    UserName = Guid.NewGuid().ToString(), //cant save cyrillic name here
                    Uid = currentUser.Uid, 
                    Ava1 = currentUser.Ava1
                };
                var result = await _userManager.CreateAsync(appUser);
                
                await _userManager.AddClaimAsync(appUser, new Claim(ClaimTypes.Name,appUser.UserName));
                await _userManager.AddClaimAsync(appUser, new Claim(ClaimTypes.Role,UserType.User.ToString()));
                
                if (!result.Succeeded) throw new SystemException("cant create new user");

                return new SpaUser
                {
                    user = _mapper.Map<DTO_VkUser>(currentUser),
                    token = _jwtGen.CreateToken(currentUser, UserType.User) //users are created with basic role only
                };
            }
        }
    }
}