using System;
using System.Threading;
using System.Threading.Tasks;
using Application.DTO;
using Application.Interfaces;
using Application.User;
using AutoMapper;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Application.MediatR.User
{
    public class CurrentUser
    {
        public class Query : IRequest<SpaUser>
        {
        }

        public class Handler : IRequestHandler<Query, SpaUser>
        {
            private readonly IJWTGenerator _jWtGenerator;
            private readonly IUserAccessor _userAccessor;
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(IJWTGenerator jWtGenerator, IUserAccessor userAccessor, DataContext context,IMapper mapper)
            {
                _jWtGenerator = jWtGenerator;
                _userAccessor = userAccessor;
                _context = context;
                _mapper = mapper;
            }

            public async Task<SpaUser> Handle(Query request, CancellationToken cancellationToken)
            {
                var name = _userAccessor.GetCurrentUserName();
                if (string.IsNullOrEmpty(name)) return null;

                
                
                var vkUser = await _context.VkUsers.FirstOrDefaultAsync(c => c.Uid == name,
                    cancellationToken);
                if (vkUser == null) return null;

                var appUser = await _context.Users.FirstOrDefaultAsync(u => u.Uid == vkUser.Uid, cancellationToken: cancellationToken);
                if (appUser.LockoutEnabled)
                {
                    if (appUser.LockoutEnd?.DateTime < DateTime.Now)
                    {
                        //clear user ban!
                        appUser.LockoutEnd = null;
                        appUser.LockoutEnabled = false;
                    }
                }
                
                return new SpaUser
                {
                    user = _mapper.Map<DTO_VkUser>(vkUser),
                    token = _jWtGenerator.CreateToken(vkUser, _userAccessor.GetCurrentUserType(appUser.Id))
                };
            }
        }
    }
}