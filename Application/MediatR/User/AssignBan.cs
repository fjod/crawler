﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DataBase;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.User
{
    public class AssignBan
    {
        public class Request : IRequest
        {
            public int Days { get; set; }
            public string Uid { get; set; }
        }

        public class Handler : AsyncRequestHandler<Request>
        {
            protected override async Task Handle(Request request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FirstOrDefaultAsync(u => u.Uid == request.Uid, cancellationToken);
                if (request.Days > 0)
                {
                    user.LockoutEnd = new DateTimeOffset(DateTime.Now + TimeSpan.FromDays(request.Days));
                    user.LockoutEnabled = true;
                }
                else
                {
                    user.LockoutEnd = null;
                    user.LockoutEnabled = false;
                }

                await _context.SaveChangesAsync(cancellationToken);
            }
            
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
        }
    }
}