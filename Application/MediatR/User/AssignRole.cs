﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Application.User;
using DataBase;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Application.MediatR.User
{
    public class AssignRole
    {
        public class Request : IRequest
        {
            public string Uid { get; set; }
            public int Role { get; set; }

            public UserType GetRole()
            {
                return Role switch
                {
                    0 => UserType.SuperAdmin,
                    1 => UserType.Admin,
                    2 => UserType.User,
                    _ => throw new ArgumentException("Bad role ID")
                };
            }
        }

        public class Handler : AsyncRequestHandler<Request>
        {
            private readonly DataContext _context;
            private readonly UserManager<AppUser> _userManager;

            public Handler(DataContext context,UserManager<AppUser> userManager)
            {
                _context = context;
                _userManager = userManager;
            }
            protected override async Task Handle(Request request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FirstOrDefaultAsync(u => u.Uid == request.Uid, cancellationToken);
                var oldClaims = await _userManager.GetClaimsAsync(user);
                var oldClaim = oldClaims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
                
                if (oldClaim != null) //edit user role
                    await _userManager.RemoveClaimAsync(user, oldClaim);
                
                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, request.GetRole().ToString()));
            }
        }
    }
}