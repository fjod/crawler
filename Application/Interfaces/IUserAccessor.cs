using Application.User;

namespace Application.Interfaces
{
    public interface IUserAccessor
    {
        string GetCurrentUserName();

        UserType GetCurrentUserType(string id = null);
    }
}