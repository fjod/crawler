﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BrowseSharp.Common;
using Domain;
using RestSharp;

namespace Application.Interfaces
{
    public interface IWebAccess
    {
        Task<AdPic> GetAdPic(string uri,bool isBigPic);
        void Dispose();
        Task<IDocument> Navigate(string url);

        void RemoveAllCookiesExceptGdprRelated();
        IList<RestResponseCookie> PrevCookies { get; }
    }
}