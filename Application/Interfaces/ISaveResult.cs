﻿using System.Collections.Generic;
using Domain;

namespace Application.Interfaces
{
    /// <summary>
    /// interface for consumers to save downloaded things somewhere
    /// </summary>
    public interface ISaveResult
    {
        void Save(ScrapedAd ad,List<AdPic> images, ISpiderLog log);
    }
}