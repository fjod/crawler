﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICaptchaSolver
    {
        double? GetBalance();
        string SolveImageCaptcha(string filepath);
    }

    public interface IGetImageForCaptchaUrl
    {
        Task<string> Start(string url, HttpClient client);
    }

    
}