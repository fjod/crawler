﻿using Domain;

namespace Application.Interfaces
{
    public interface ISpiderLog
    {
        void LogAd(ScrapedAd ad);
        void LogMessage(string msg);
    }
}