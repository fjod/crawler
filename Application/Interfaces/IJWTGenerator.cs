using Application.User;
using Domain;

namespace Application.Interfaces
{
    public interface IJWTGenerator
    {
        string CreateToken(VkUser user, UserType type);
    }
}