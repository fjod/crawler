﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;

namespace Application.Interfaces
{
    public interface IAdSorter
    {
        Task<List<ScrapedAd>> Sort(int take, int skip);
    }
}