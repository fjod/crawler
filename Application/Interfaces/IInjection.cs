﻿namespace Application.Interfaces
{
    public interface IInjection
    {
        T GetType<T>();
    }
}