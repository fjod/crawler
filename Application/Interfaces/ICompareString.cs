﻿using System;

namespace Application.Interfaces
{
    public interface ICompareString
    {
        double CrawlerCompare(String input, String compareTo);
    }
}