using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Application.Interfaces;
using Application.User;
using Domain;
using Microsoft.IdentityModel.Tokens;

namespace InfraStructure
{
    public class JwtGenerator : IJWTGenerator
    {
        SymmetricSecurityKey _key;

        public JwtGenerator()
        {
            var tempSetting = new Settings();
            _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tempSetting.Get("TokenKey")));
        }

        public string CreateToken(VkUser user, UserType type)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.Uid),
                new Claim(JwtRegisteredClaimNames.Typ, type.ToString())
            };
            
            
            var creds = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDesc = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDesc);
            return tokenHandler.WriteToken(token);
        }
    }
}