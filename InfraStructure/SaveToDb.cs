﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Application.Interfaces;
using DataBase;
using Domain;
using Domain.ImageComparison;

namespace InfraStructure
{
    public class SaveToDb : ISaveResult
    {
        private readonly DataContext _context;
        
        public SaveToDb(DataContext context)
        {
            _context = context;
        
        }

        public void Save(ScrapedAd ad, List<AdPic> _images, ISpiderLog log)
        {
            var images = (from im in _images where im!=null select im).ToList();
            
            _context.ScrapedAds.Add(ad);
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    _context.SaveChanges(); //until I save it I wont have it's Id
                    break;
                }
                catch (InvalidOperationException e)
                {
                    log.LogMessage(e.Message);
                    log.LogMessage(e.StackTrace);
                    Console.WriteLine("Cant save images in db");
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
                catch (Exception e)
                {
                    log.LogMessage(e.Message);
                    log.LogMessage(e.StackTrace);
                    Console.WriteLine("Cant save images in db - bad exception");
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
            }


            //dont need to save same pics to db
            //here ad does not have links to it's own images
            //so I need to scan db for similar pics and assign them to the joining table
            var smaller = (from z in _context.AdPics select new {hash = z.ImgHash, id = z.ID}).ToList();

            var picsThatAlreadyInDb = (from pic in smaller
                join image in images on pic.hash equals image.ImgHash
                select new {DbPicId=pic.id, newImage = image}).ToList();
            
            
            //for these pics I must save their url pic.id (Db) == AdPic
            picsThatAlreadyInDb.ForEach(pic =>
            {
                var correctAdPic = _context.AdPics.Find(pic.DbPicId);
                if (correctAdPic.AdditionalUrls == null) 
                    correctAdPic.AdditionalUrls = new List<AdPicUrl>();
                correctAdPic.AdditionalUrls.Add(new AdPicUrl {AdPic = correctAdPic, Url = pic.newImage.Url});
            });
            
            
            //now select all images that not in db and save them to have ID assigned
            List<AdPic> left = new List<AdPic>();
            foreach (var adPic in images)
            {
                var found = picsThatAlreadyInDb.Find(im => im.newImage == adPic);
                if (found == null) left.Add(adPic);
            }
            //pics are stored in temp folder at the moment
            //create subfolder for this ad if necessary
            var pathToAdOnDisk = TempFileName.GetFolder(TempFileName.AdsFolderName);
            var currentAdDir = Path.Combine(pathToAdOnDisk, ad.ID.ToString());
            if (!Directory.Exists(currentAdDir))
                Directory.CreateDirectory(currentAdDir);
            
            GC.Collect(); //hopefully it will help with bitmap disposes
            GC.WaitForPendingFinalizers();
            
            //move pics there
            foreach (var adPic in left)
            {
                var destFileName = Path.Combine(currentAdDir, Path.GetFileName(adPic.FileName));
                try
                {
                    if (File.Exists(adPic.FileName)) //we dont do it in tests
                    {
                        File.Move(adPic.FileName, destFileName);
                        adPic.FileName = destFileName;
                    }
                }
                catch (IOException)
                {
                    //delete dest file if we for some reason already got it
                    try
                    {
                        File.Delete(destFileName);
                        File.Move(adPic.FileName, destFileName);
                        adPic.FileName = destFileName;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("--------cant save pic on disk-----------");
                        Console.WriteLine(e);
                        Console.WriteLine(e.Message);
                    }
                }
                finally
                {
                    try
                    {
                        File.Delete(adPic.FileName);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("--------cant delete temp img on disk-----------");
                        Console.WriteLine(e);
                        Console.WriteLine(e.Message);
                    }
                }
            }
            
            _context.AdPics.AddRange(left);
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    _context.SaveChanges();
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    Console.WriteLine($"Cant save pics in db, attempt # {i}");
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    i = 10;
                }
            }

            //in the end save them to joining table
            List<AdsAndImages> adsAndImages = new List<AdsAndImages>();
            foreach (var x1 in picsThatAlreadyInDb)
            {
                AdsAndImages temp = new AdsAndImages
                {
                    Ad = ad, Image = _context.AdPics.FirstOrDefault(pic => pic.ID == x1.DbPicId)
                };
                adsAndImages.Add(temp);
            }
            List<AdPicsLinks> adPicsLinks = new List<AdPicsLinks>();
            foreach (var image in left)
            {
                AdsAndImages temp = new AdsAndImages
                {
                    Ad = ad, Image = _context.AdPics.FirstOrDefault(pic => pic.ID == image.ID)
                };
                adsAndImages.Add(temp);
                
                if (image.IsBig)
                    adPicsLinks.Add(new AdPicsLinks
                    {
                       BigImage = image, SmallImage = image.Sibling
                    });
                else
                {
                    adPicsLinks.Add(new AdPicsLinks
                    {
                        BigImage = image.Sibling, SmallImage = image
                    });
                }
            }
            _context.AdImageLink.AddRange(adsAndImages);
            _context.AdPicsLinks.AddRange(adPicsLinks);
            
            ad.AdPics = new List<AdPic>();
            adsAndImages.ForEach(im =>
            {
                ad.AdPics.Add(im.Image);
            });
            
            _context.SaveChanges();
        }

       
    }
}