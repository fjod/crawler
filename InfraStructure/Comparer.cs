﻿using Application.Interfaces;

namespace InfraStructure
{
    /*Levenshtein calculates the shortest possible distance between two strings.
     Producing a count of the number of insertions, deletions and substitutions to make one string into another.*/
    public class CrawlerStringComparer : ICompareString
    {
        /// <summary>
        /// Compare two strings to check how different they are
        /// </summary>
        /// <param name="input"></param>
        /// <param name="compareTo"></param>
        /// <returns>percentage of difference. 1 = totally different, 0 = same </returns>
        double ICompareString.CrawlerCompare(string input, string compareTo)
        {
            if (input == null && compareTo == null) return 0;
            if (input == null || compareTo == null) return 1;
            
            var biggerLen = input.Length > compareTo.Length ? input.Length : compareTo.Length;
            double distance = Fastenshtein.Levenshtein.Distance(input, compareTo);
            return distance / biggerLen;
        }
    }
}