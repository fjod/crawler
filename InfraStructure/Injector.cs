﻿using Application.Interfaces;
using Crawler.Browser;
using DataBase;
using Domain;
using InfraStructure.Captcha;
using InfraStructure.Logs;
using Ninject;
using Ninject.Modules;
#pragma warning disable 618

namespace InfraStructure
{
    public class Injector : IInjection
    {
        readonly StandardKernel _kernel;
        public Injector()
        {
            _kernel = new StandardKernel(new LoadModule());
        }

        class LoadModule : NinjectModule
        {
            public override void Load()
            {
                Bind<IHostingDbEnv>().To<HostingEnv>();
                Bind<ISpiderLog>().To<LogToFile>();
                Bind<DataContext>().To<DataContext>().InSingletonScope();
                Bind<ISaveResult>().To<SaveToDb>();
                Bind<ICompareString>().To<CrawlerStringComparer>();
                Bind<ICaptchaSolver>().To<CaptchaSolver>();
                Bind<IGetImageForCaptchaUrl>().To<GetCaptchaUrlFromAutoRuUrl>();
                Bind<Settings>().To<Settings>().InSingletonScope();
                Bind<IWebAccess>().To<WebAccess>().InSingletonScope();
            }
        }

        public T GetType<T>()
        {
            return _kernel.Get<T>();
        }
    }
}