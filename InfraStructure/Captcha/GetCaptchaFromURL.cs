﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Interfaces;

namespace InfraStructure.Captcha
{
    public class GetCaptchaUrlFromAutoRuUrl : IGetImageForCaptchaUrl
    {
        //get image from url, save on disk and return
        public async Task<string> Start(string url, HttpClient client)
        {
            return await GetFileAndSaveIt(url, client);
        }

        public async Task<String> GetFileAndSaveIt(string location, HttpClient client)
        {
            var originalFileName = Path.GetTempFileName();
            File.Delete(originalFileName);
            string tempFileName =  originalFileName+ ".jpg";
            HttpResponseMessage image = await client.GetAsync(location);
            using (Stream responseStream = await image.Content.ReadAsStreamAsync())
            {
                using (FileStream fs = new FileStream(tempFileName, FileMode.OpenOrCreate))
                {
                  await responseStream.CopyToAsync(fs);
                }
            }
            return tempFileName;
        }
    }
}