﻿using System;
using Application.Interfaces;
using Domain;

namespace InfraStructure.Captcha
{
    public class CaptchaSolver : ICaptchaSolver
    {
        private readonly string _clientId;
        private readonly ISpiderLog _log;

        public CaptchaSolver(Settings settings, ISpiderLog log)
        {
            _clientId = settings.Get("CaptchaClientID");
            _log = log;
        }

        public double? GetBalance()
        {
            var api = new ImageToText
            {
                ClientKey = _clientId
            };

            var balance = api.GetBalance();

            if (balance == null)
                _log.LogMessage("get balance for captcha failed " + api.ErrorMessage);

            return balance;
        }


        public string SolveImageCaptcha(string filepath)
        {
            var api = new ImageToText
            {
                ClientKey = _clientId,
                FilePath = filepath
            };

            if (!api.CreateTask())
                _log.LogMessage("Captcha : API v2 send failed. " + api.ErrorMessage);
            else if (!api.WaitForResult())
                _log.LogMessage("Captcha: Could not solve the captcha.");
            else
            {
                var result = api.GetTaskSolution().Text;
                _log.LogMessage("Captcha result: " + result);
                return result;
            }

            return String.Empty;
        }
    }
}