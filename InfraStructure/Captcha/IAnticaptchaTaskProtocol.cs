﻿using Newtonsoft.Json.Linq;

namespace InfraStructure.Captcha
{
    internal interface IAnticaptchaTaskProtocol
    {
        JObject GetPostData();
        TaskResultResponse.SolutionData GetTaskSolution();
    }
}