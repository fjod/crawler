using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Application.DTO;
using Application.Interfaces;
using Application.User;
using DataBase;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace InfraStructure
{
    public class UserAccessor : IUserAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DataContext _context;

        public UserAccessor(IHttpContextAccessor httpContextAccessor, DataContext context)
        {
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }

        public string GetCurrentUserName()
        {
            return _httpContextAccessor.HttpContext.User?.Claims
                ?.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.NameId)?.Value;
        }

        public UserType GetCurrentUserType(string appUserId = null)
        {
            //might now work if there is no Policy on method
            var currentRole = _httpContextAccessor.HttpContext.User?.Claims
                ?.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Typ)?.Value;

            if (String.IsNullOrEmpty(currentRole) && appUserId != null)
            {
                var roleClaim = _context.UserClaims.FirstOrDefault(claim => claim.ClaimType == ClaimTypes.Role);
                currentRole = roleClaim?.ClaimValue;
            }
            
            return DTO_UserWithCreds.FromString(currentRole);
        }
    }
}