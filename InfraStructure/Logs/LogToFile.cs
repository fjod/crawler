﻿using System.IO;
using Application.Interfaces;
using Domain;
using Domain.ImageComparison;
using Serilog;
using Serilog.Events;

namespace InfraStructure.Logs
{
    public class LogToFile : ISpiderLog
    {
        static LogToFile()
        {
            var logFolder = TempFileName.GetFolder("Logs");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(Path.Combine(logFolder,"log.txt"), rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        public void LogAd(ScrapedAd ad)
        {
            Log.Information(ad.ToString());
        }

        public void LogMessage(string msg)
        {
            Log.Information(msg);
        }
    }
}