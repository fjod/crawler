﻿using System;
using System.Threading.Tasks;
using Application.Interfaces;
using Crawler;
using InfraStructure;

namespace scraperTest
{
    // ReSharper disable once ClassNeverInstantiated.Global
    class Program
    {
        // ReSharper disable once UnusedParameter.Local
        static async Task Main(string[] args)
        {
            var injection = new Injector();
            var log = injection.GetType<ISpiderLog>();
            AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => { log.LogMessage("scraper app exit"); };
            try
            {
                CuteSpider spider = new CuteSpider(injection);
                await spider.Start();
            }
            catch (Exception e)
            {
                log.LogMessage("exception in scraper app");
                log.LogMessage(e.Message);
                log.LogMessage(e.StackTrace);
                
            }
            
        }
        
        
    }
}

