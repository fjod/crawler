using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class VkUser
    {
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }

        /// <summary>
        /// vk user id
        /// </summary>
        public string  Uid { get; set; }
        public string  Name { get; set; }
        public string  Surname { get; set; }
        public string  Ava1 { get; set; }
        public string  Ava2 { get; set; }

        /// <summary>
        /// guid for registration
        /// </summary>
        public string Guid { get; set; }
        
        public List<AdRating> Ratings { get; set; }

        public VkUser CleanUp()
        {
            VkUser temp = MemberwiseClone() as VkUser;
            temp.ID = 0;
            temp.Guid = "";
            return temp;
        }

        public static readonly string SampleAdder = "SAMPLE";
        public static VkUser CreateSampleUser( Random random)
        {
            return new VkUser
            {
                Name = SampleAdder+RandomString.Get(10),
                Surname = SampleAdder+RandomString.Get(10),
                Guid = System.Guid.NewGuid().ToString(),
                Ava1 = "https://picsum.photos/100/100.jpg",
                Ava2 = "https://picsum.photos/100/100.jpg",
                Uid =  random.Next(1_000_000, 9_000_000).ToString()
            };

        }
    }
}