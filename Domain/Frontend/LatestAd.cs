using System;


namespace Domain.Frontend
{
    public class LatestAd : IEquatable<LatestAd>
    {
        public ScrapedAd Current;
        public ScrapedAd Unique;
        
        public string ScrapedLocation;
        public string FileLocation;

        public bool Equals(LatestAd other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Current.ID == other.Current.ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LatestAd) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Unique, ScrapedLocation, FileLocation);
        }
    }
}