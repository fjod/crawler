using System.Collections.Generic;

namespace Domain.Frontend
{
    public class EditorUniqueAd
    {
        public ScrapedAd Ad;
        public List<AdPic> SmallPics = new List<AdPic>();
    }
}