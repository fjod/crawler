namespace Domain.Frontend
{
    public class ClaimsInfo
    {
        public string NameIdentifier { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Ava { get; set; }
    }
}