using System.Collections.Generic;

namespace Domain.Frontend
{
    /// <summary>
    /// difference between two ads
    /// </summary>
    public class AdHistory
    {
        public int PrevId { get; set; }
        public int NextId { get; set; }
        public List<FieldDifference> Differences { get; set; } = new List<FieldDifference>();
        
        public string PrevScrapedAt  { get; set; }
        public string NextScrapedAt  { get; set; }
    }

    public class FieldDifference
    {
        public string FieldName { get; set; }
        public string PrevFieldValue { get; set; }
        public string NewFieldValue { get; set; }
    }
}