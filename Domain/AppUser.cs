﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class AppUser : IdentityUser
    {
        public string  Uid { get; set; }
        public string  Ava1 { get; set; }

        public static AppUser CreateSampleUser(VkUser source, Guid id)
        {
            return new AppUser
            {
                Id = id.ToString(),
                Uid = source.Uid,
                Ava1 =  source.Ava1,
                UserName = Guid.NewGuid().ToString()
            };
        }

        public static IdentityUserClaim<string> CreateSampleClaim(AppUser user, string claimType, string userType)
        {
            return new IdentityUserClaim<string>
            {
                ClaimType = claimType, ClaimValue = userType, UserId = user.Id
            };
        }
    }
}