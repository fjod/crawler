﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Domain
{
    public class CuteRequest
    {
        public static async Task<HttpResponseMessage> SendPost(string url, HttpClient client,
             HttpContent content,
            List<(string name, string value)> headers = null)
        {
            //AddHeadersAndCookies(client, cookies);
            headers?.ForEach(h=>
            {
                if (client.DefaultRequestHeaders.Contains(h.name))
                {
                    client.DefaultRequestHeaders.Remove(h.name);
                }
                client.DefaultRequestHeaders.Add(h.name, h.value);
            });
            var ret = await client.PostAsync(url, content);
            headers?.ForEach(h=>
            {
                if (client.DefaultRequestHeaders.Contains(h.name))
                {
                    client.DefaultRequestHeaders.Remove(h.name);
                }
            });
            return ret;
        }
        
        public static async Task<HttpResponseMessage> SendGet(string url, HttpClient client)
        {
            //AddHeadersAndCookies(client, cookies);
            return await client.GetAsync(url);
        }
    }
}