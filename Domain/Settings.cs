﻿using System;
using System.IO;
using System.Net;
using Microsoft.Extensions.Configuration;

namespace Domain
{
    public class Settings
    {
        private readonly IConfigurationRoot _root;

        public Settings()
        {
            
            IConfigurationBuilder builder = new ConfigurationBuilder();
            var settingsPath = Path.Combine(Directory.GetCurrentDirectory(), "settings.json"); //default for laptop
            void AddSettingsFile()
            {
                if (File.Exists(settingsPath)) builder.AddJsonFile(settingsPath);
            }
            
            AddSettingsFile();


            if (!File.Exists(settingsPath)) //for ef core cli
            {
                var crawlerPos = settingsPath.IndexOf("tortest2", StringComparison.Ordinal);
                if (crawlerPos > 0)
                {
                    settingsPath = settingsPath.Substring(0, crawlerPos);
                    settingsPath = Path.Combine(settingsPath, "Domain");
                    settingsPath = Path.Combine(settingsPath, "settings.json");
                    AddSettingsFile();
                }
                else //for API
                {
                    crawlerPos = settingsPath.IndexOf("API", StringComparison.Ordinal);
                    if (crawlerPos > 0)
                    {
                        settingsPath = settingsPath.Substring(0, crawlerPos);
                        settingsPath = Path.Combine(settingsPath, "Domain");
                        settingsPath = Path.Combine(settingsPath, "settings.json");
                        AddSettingsFile();
                    }
                }
            }

            if (!File.Exists(settingsPath)) //for vps
            {
                settingsPath = Path.Combine("/home/fjod/crawler", "settings.json");
                AddSettingsFile();
            }
            
            if (!File.Exists(settingsPath)) //for docker
            {
                settingsPath = Path.Combine("/home/fjod/crawler/crawler", "settings.json");
                AddSettingsFile();
            }

            var cont = File.OpenText(settingsPath).ReadToEnd();
            if (cont.Length <= 0)
                throw new Exception("was not able to locate settings.json!");
            
            _root = builder.Build();
        }

        public string Get(string key)
        {
            try
            {
                var ret = _root.GetValue<string>(key);
                return ret;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public (string ClientId, string ClientSecret) GetVkCreds(bool isProd)
        {
            string request = isProd ? "VkProd" : "VkDebug";
            string id = _root.GetValue<string>($"{request}:ClientId");
            string secret = _root.GetValue<string>($"{request}:ClientSecret");
            return (id, secret);
        }
        public WebProxy GetProxy()
        {
            try
            {
                string ip = _root.GetValue<string>("Proxy:IP");
                string port = _root.GetValue<string>("Proxy:Port");
                string login = _root.GetValue<string>("Proxy:Login");
                string pass = _root.GetValue<string>("Proxy:Password");

                ICredentials credentials = new NetworkCredential(login, pass);

                var builder = new UriBuilder(ip) {Port = int.Parse(port)};

                return new WebProxy(builder.Uri, true, null, credentials);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}