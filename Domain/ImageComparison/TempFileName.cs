using System;
using System.IO;
using System.Linq;

namespace Domain.ImageComparison
{
    public static class TempFileName
    {
        public static string TempFolderName => "temp";
        public static string AdsFolderName => "Ads"+DateTime.Now.Year; //linux can have up to 64k subfolders
        public static string DefaultExtension = ".jpg";
        private static string _volumeName = "/crawlervolume";
        public static string Get()
        {
            return Get(TempFolderName);
        }

        public static void CleanTempFolder()
        {
            var folder = GetFolder(TempFolderName);
            DirectoryInfo di = new DirectoryInfo(folder);
            foreach (var enumerateFile in di.EnumerateFiles())
            {
                File.Delete(enumerateFile.FullName);
            }
        }

        public static string GetFolder(string subfolder)
        {
            if (subfolder == AdsFolderName) //someone is looking for place to store ads pictures
            {
                //docker folder path for prod
                //simply check if folder exists
                var isProd = Directory.GetDirectories(Path.DirectorySeparatorChar.ToString()).Any(folder => folder == _volumeName);
                if (isProd)
                    return Path.Combine(Path.DirectorySeparatorChar.ToString(), _volumeName);
            }
            var currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (String.IsNullOrEmpty(subfolder) || String.IsNullOrWhiteSpace(subfolder)) return currentDir;
            currentDir = Path.Combine(currentDir, subfolder);
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);

            return currentDir;
        }
        
        public static string Get(string folder)
        {
            var currentDir = GetFolder(folder);

            var newFileName = Path.Combine(currentDir,Guid.NewGuid().ToString()) + DefaultExtension;

            File.Create(newFileName);

            return newFileName;
        }
    }
}