using System.Drawing;
using System.Text;

namespace Domain.ImageComparison
{
    public static class Converter
    {
        static Bitmap ToBitmap(this Image im)
        {
            return new Bitmap(im);
        }

        public static AdPic ToAdPic(this Image im, bool isBig, string uri)
        {
            if (im == null) return null;
            
            var adPic = new AdPic();
           
            var pic = im.ToBitmap();
            adPic.Height = pic.Height;
            adPic.Width = pic.Width;
            adPic.IsBig = isBig;
            var hash = new ImgHash();
            hash.GenerateFromImage(pic);
            StringBuilder hashBuilder = new StringBuilder();
            for (int i = 0; i < hash.HashData.Length; i++)
            {
                hashBuilder.Append(hash.HashData[i] ? "1" : "0");
            }
            adPic.ImgHash = hashBuilder.ToString();

            var temp = TempFileName.Get();
            im.Save(temp);
            adPic.FileName = temp; 
            adPic.Url = uri;

            return adPic;
        }
    }
}