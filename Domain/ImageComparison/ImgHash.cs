﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Domain.ImageComparison
{
   
    public class ImgHash
    {
        private readonly int _hashSide;

        private bool[] _hashData;
        public bool[] HashData => _hashData;

        public ImgHash(int hashSideSize = 32)
        {
            _hashSide = hashSideSize;

            _hashData = new bool[hashSideSize * hashSideSize];
        }
        
        public ImgHash(Bitmap bmp,int hashSideSize = 32)
        {
            _hashSide = hashSideSize;
            _hashData = new bool[_hashSide * _hashSide];
            GenerateFromImage(bmp);
        }
        
        public ImgHash(String sequence)
        {
            _hashSide = 32;
            _hashData = new bool[_hashSide * _hashSide];
            if (sequence.Length != 32*32) throw new Exception("Unknown hash format");
            for (int i = 0; i < _hashSide*_hashSide; i++)
            {
                 _hashData[i] = sequence[i] == '1';
            }
        }

        /// <summary>
        /// Method to compare 2 image hashes
        /// </summary>
        /// <returns>% of similarity</returns>
        public double CompareWith(ImgHash compareWith)
        {
            if (HashData.Length != compareWith.HashData.Length)
            {
                throw new Exception("Cannot compare hashes with different sizes");
            }

            int differenceCounter = 0;

            for (int i = 0; i < HashData.Length; i++)
            {
                if (HashData[i] != compareWith.HashData[i])
                {
                    differenceCounter++;
                }
            }

            double percent = (double)differenceCounter / HashData.Length;
            return percent;
        }
    

        public void GenerateFromImage(Bitmap img)
        {
            List<bool> lResult = new List<bool>();

            //resize img to 16x16px (by default) or with configured size 
            Bitmap bmpMin = new Bitmap(img, new Size(_hashSide, _hashSide));
            
            for (int j = 0; j < bmpMin.Height; j++)
            {
                for (int i = 0; i < bmpMin.Width; i++)
                {
                    //reduce colors to true and false
                    lResult.Add(bmpMin.GetPixel(i, j).GetBrightness() < 0.5f);
                }
            }

            _hashData = lResult.ToArray();

            bmpMin.Dispose();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _hashSide*_hashSide; i++)
            {
                sb.Append(_hashData[i] ? "1" : "0");
            }

            return sb.ToString();
        }

        public static string GenerateMock()
        {
            Random r = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 16*16; i++)
            {
                var randInt = r.Next(0, 100);
                sb.Append(randInt>50 ? "1" : "0");
            }

            return sb.ToString();
        }
    }
}