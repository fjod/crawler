using System.Collections.Generic;
using System.Linq;

namespace Domain.ImageComparison
{
    public static class FIlterUniques
    {
        public static List<AdPic> Filter (this IEnumerable<AdPic> input)
        {
            return input.GroupBy(pic => pic.ImgHash).Select(group => group.First()).ToList();
        }
    }
}