﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    /// <summary>
    /// link between ad and adpic
    /// </summary>
    public class AdsAndImages
    {
        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        public ScrapedAd Ad { get; set; }
        public AdPic Image { get; set; }
    }
}