﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class AdPic : IEquatable<AdPic>
    {   
        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        public string ImgHash { get; set; }
        //TODO: add hashCode for ImgHash so distinct will work faster 
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsBig { get; set; }
        public string FileName { get; set; }
        
        public string Url { get; set; }
        
        public ICollection<AdPicUrl> AdditionalUrls { get; set; }
        
        public ScrapedAd Ad { get; set; }

        public AdPic Sibling;

        public static AdPic CreateMock(bool isBig)
        {
            AdPic ap = new AdPic();
            ap.IsBig = isBig;
            ap.FileName = RandomString.Get(10);
            int imageSize = isBig ? 1000 : 100;
            ap.Url = $"https://picsum.photos/{imageSize}/{imageSize}.jpg";
            //most important thing here is hash
            ap.ImgHash = ImageComparison.ImgHash.GenerateMock();
            return ap;
        }

        public void FixUrlToUseHttps()
        {
            if (Url.StartsWith("https")) return;
            if (Url.StartsWith("http"))
                Url = Url.Replace("http", "https");
        }

        public bool Equals(AdPic other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ImgHash == other.ImgHash && IsBig == other.IsBig && Url == other.Url;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AdPic) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ImgHash, IsBig, Url);
        }

        public void CleanUp()
        {
            ImgHash = String.Empty;
            AdditionalUrls = null;
            Sibling = null;
            Ad = null;
            FixUrlToUseHttps();
        }
    }
}