using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class AdRating
    {
        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }

        public int ScrapedAdId { get; set; }
        public ScrapedAd ScrapedAd { get; set; }

        public int VkUserId { get; set; }
        public VkUser VkUser { get; set; }
        
        public int Rating { get; set; }

        public static AdRating GetSampleRating(VkUser user, ScrapedAd ad, Random random)
        {
            return new AdRating
            {
                Rating = random.Next(0,10),
                ScrapedAd = ad,
                VkUser = user
            };
        }
    }
}