using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Comment
    {
        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        
        public int ScrapedAdId { get; set; }
        public ScrapedAd ScrapedAd { get; set; }

        public int VkUserId { get; set; }
        public VkUser VkUser { get; set; }

        public string Text { get; set; }

        public DateTime DateTime { get; set; }

        public int? ParentId { get; set; }
        public Comment Parent { get; set; }

        public override string ToString()
        {
            return ScrapedAd.ID.ToString();
        }

        public static Comment GetSampleComment(ScrapedAd ad, VkUser user)
        {
            return new Comment
            {
                Text = RandomString.Get(50),
                DateTime = DateTime.Now,
             //   ScrapedAd = ad,
                VkUser = user
            };
        }
    }
}