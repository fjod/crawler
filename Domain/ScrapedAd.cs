﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Frontend;


namespace Domain
{
    public class ScrapedAd : IEquatable<ScrapedAd>
    {
        #region my fields

        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        public Uri BaseUrl { get; set; }
        public DateTime ScrapedAt { get; set; }

        public int? UniqueId { get; set; }
        
        public ICollection<AdPic> AdPics { get; set; }

        public override string ToString()
        {
           // return $"URL {BaseUrl?.AbsoluteUri} was scraped {ScrapedAt}";
           return ID.ToString();
        }
        
        public List<AdRating> Ratings { get; set; } = new List<AdRating>();

        public List<Comment> Comments { get; set; } = new List<Comment>();
        #endregion

        #region  scrapedFields

        public string Description { get; set; }
        public DateTime FreshDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int ManufactureYear { get; set; }
        public string Vin { get; set; }
        public int Price { get; set; }
        public string SellerName { get; set; }
        public int Mileage { get; set; }
        public string Color { get; set; }
        public string City { get; set; }

        #endregion

        public ScrapedAd Copy()
        {
            ScrapedAd other = (ScrapedAd) this.MemberwiseClone();
            other.BaseUrl = BaseUrl;
            other.Comments = new List<Comment>();
            other.Ratings = new List<AdRating>();
            return other;
        }

        public static ScrapedAd CreateMockAd(DateTime scrapedWhen)
        {
            ScrapedAd ret = new ScrapedAd();

            int RandomNumber(int min, int max)
            {
                Random random = new Random();
                return random.Next(min, max);
            }

            ret.City = VkUser.SampleAdder+RandomString.Get(15);
            ret.Color = RandomString.Get(5);
            ret.Description = RandomString.Get(100);
            ret.Mileage = RandomNumber(10000, 50000);
            ret.Price = RandomNumber(150000, 300000);
            ret.Vin = RandomString.Get(10);
            ret.BaseUrl = new Uri($"https://www.{RandomString.Get(10)}.ru/{RandomString.Get(5)}");
            ret.CreateDate = DateTime.Now - TimeSpan.FromHours(RandomNumber(0, 100));
            ret.FreshDate = DateTime.Now;
            ret.ManufactureYear = 2004 + RandomNumber(0, 5);
            ret.ScrapedAt = scrapedWhen;
            ret.SellerName = RandomString.Get(10);
            return ret;
        }

        /// <summary>
        /// Get diffs
        /// </summary>
        /// <param name="other">newer ad</param>
        /// <returns></returns>
        public List<FieldDifference> FindDifferences(ScrapedAd other)
        {
            var ret = new List<FieldDifference>();
            if (Description != other.Description)
                ret.Add(new FieldDifference
                {
                    NewFieldValue =  other.Description,
                    PrevFieldValue = Description,
                    FieldName = "Description"
                });
            
            if (Mileage!= other.Mileage)
                ret.Add(new FieldDifference
                {
                    NewFieldValue =  other.Mileage.ToString(),
                    PrevFieldValue = Mileage.ToString(),
                    FieldName = "Mileage"
                });
            
            if (Price!= other.Price)
                ret.Add(new FieldDifference
                {
                    NewFieldValue =  other.Price.ToString(),
                    PrevFieldValue = Price.ToString(),
                    FieldName = "Price"
                });
            
            if (ManufactureYear!= other.ManufactureYear)
                ret.Add(new FieldDifference
                {
                    NewFieldValue =  other.ManufactureYear.ToString(),
                    PrevFieldValue = ManufactureYear.ToString(),
                    FieldName = "ManufactureYear"
                });

            if (SellerName!= other.SellerName)
                ret.Add(new FieldDifference
                {
                    NewFieldValue =  other.SellerName,
                    PrevFieldValue = SellerName,
                    FieldName = "SellerName"
                });

            return ret;
        }

        public bool Equals(ScrapedAd other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ID == other.ID && Equals(BaseUrl, other.BaseUrl) && ScrapedAt.Equals(other.ScrapedAt) && UniqueId == other.UniqueId && Description == other.Description && FreshDate.Equals(other.FreshDate) && CreateDate.Equals(other.CreateDate) && ManufactureYear == other.ManufactureYear && Vin == other.Vin && Price == other.Price && SellerName == other.SellerName && Mileage == other.Mileage && Color == other.Color && City == other.City;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ScrapedAd) obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(ID);
            hashCode.Add(BaseUrl);
            hashCode.Add(ScrapedAt);
            hashCode.Add(UniqueId);
            hashCode.Add(Description);
            hashCode.Add(FreshDate);
            hashCode.Add(CreateDate);
            hashCode.Add(ManufactureYear);
            hashCode.Add(Vin);
            hashCode.Add(Price);
            hashCode.Add(SellerName);
            hashCode.Add(Mileage);
            hashCode.Add(Color);
            hashCode.Add(City);
            return hashCode.ToHashCode();
        }
    }
}