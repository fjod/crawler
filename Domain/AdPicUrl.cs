﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class AdPicUrl
    {
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        public string Url { get; set; }
        public AdPic AdPic { get; set; }
    }
}