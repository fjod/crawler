using System.ComponentModel.DataAnnotations;

namespace Domain
{
    /// <summary>
    /// link between small and big image
    /// </summary>
    public class AdPicsLinks
    {
        [MaxLength(255)]
        [Key]
        // ReSharper disable once InconsistentNaming
        public int ID { get; set; }
        public AdPic BigImage { get; set; }
        public AdPic SmallImage { get; set; }
    }
}