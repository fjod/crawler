using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CrawlerWorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        private int workerVersion = 10_02;
        private int _counter;
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
            
               // _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
               // _logger.LogInformation($"ver = {workerVersion}, counter = {_counter++}");
               // await Task.Delay(2000, stoppingToken);
                
                _logger.LogInformation("Spider started: {time}", DateTimeOffset.Now);
                _logger.LogInformation($"Launch counter: {_counter}");
                _logger.LogInformation($"Spider ver : {workerVersion}");
                string filename = "/usr/bin/dotnet";
                string args = "/home/fjod/crawler/crawler/ScraperApp.dll";
                _logger.LogInformation($"filename and args = {filename} {args}");
                
                Process process = new Process();
                //process.StartInfo.FileName="/usr/bin/dotnet";
               // process.StartInfo.Arguments = "/home/fjod/crawler/ScraperApp.dll";  //without docker
               //for docker we use 2 containers, so 2 publish commands
               //so actual crawler console app will be not in slightly different location
             
               process.StartInfo.FileName = filename;
               process.StartInfo.Arguments = args;
              
                
               // process.StartInfo.UseShellExecute = false;
                process.Start();
                await Task.Delay(TimeSpan.FromDays(1), stoppingToken);
                _counter++;
            }
        }
    }
}